﻿#include<vector>;
#include<string>;
#include<iostream>
#include<string_view>
#include<format>

using namespace std;

string extractExtension(string_view filename)
{
	return string{ filename.substr(filename.find_last_of('.')) };
}


int main()
{
	// part 1
	// string literal as const char*
	const char* str = "Hello, World!";
	cout << str << endl;

	//char* str2 = "Hello, World!";
	//str2[0] = 'h'; // error: assignment of read-only location '*(str + 0)'
	//cout << str2 << endl;

	// but you can initialize a char array with a string literal
	char str3[] = "Hello, World!";
	str3[0] = 'h';
	cout << str3 << endl;

	// part 2
	// raw string
	const string raw = R"(C:\Program Files\Microsoft)";
	cout << raw << endl;

	string raw2 = R"-(Embedded "quotes" in raw string)-";
	cout << raw2 << endl;

	// part 3
	// string class

	string a{ "12" };
	string b{ "34" };
	string c{ a + b };

	auto result{ a <=> b }; // spaceship operator
	if (is_gt(result)) {
		cout << a << " > " << b << endl;
	}
	else if (is_lt(result)) {
		cout << a << " < " << b << endl;
	}
	else {
		cout << a << " == " << b << endl;
	}

	// part 4
	// operation on string
	string s{ "Hello, World!" };
	cout << s << endl;
	auto ss {s.substr(0, 5)}; // Hello
	cout << ss << endl;

	auto pos = s.find("World");
	if (pos != string::npos) {
		cout << "Found at " << pos << endl;
	}
	else {
		cout << "Not found" << endl;
	}

	auto replaceS {s.replace(7, 5, "C++")}; // Hello, C++!
	cout << replaceS << endl;

	auto insertS {s.insert(7, "C++ ")}; // Hello, C++ World!
	cout << insertS << endl;
	cout << insertS.starts_with("Hello") << endl;
	cout << insertS.ends_with("World!") << endl;

	// std::string literals
	auto string3{"Hello, World!"s }; // s postfix
	cout << string3 << endl;

	// CTAD when creating a vector of string
	vector v{ "Hello"s, "World"s, "C++"s }; // vector<string>
	for (const auto& s : v) {
		cout << s << endl;
	}

	vector v2{ "Hello", "World", "C++" }; // vector<const char*>
	for (const auto& s : v2) {
		cout << s << endl;
	}

	// part 5
	// string_view
	string_view sv{ "Hello, World!"sv };  // sv postfix
	cout << sv << endl;
	cout << extractExtension("file.txt") << endl;

	// part 6
	// formatting & printing
	auto x{ 1 };
	auto y{ 2 };
	cout << format("x = {}, y = {}", x, y) << endl;
	return 0;
}


