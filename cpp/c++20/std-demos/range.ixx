export module range;

import std;

using namespace std;


template <typename IteratorType>
void iteratorTraitsTest(IteratorType it) {
	using ValueType = typename std::iterator_traits<IteratorType>::value_type;
	using DifferenceType = typename std::iterator_traits<IteratorType>::difference_type;
	using ReferenceType = typename std::iterator_traits<IteratorType>::reference;
	using PointerType = typename std::iterator_traits<IteratorType>::pointer;
	using IteratorCategory = typename std::iterator_traits<IteratorType>::iterator_category;

	std::cout << "ValueType: " << typeid(ValueType).name() << std::endl;
	std::cout << "DifferenceType: " << typeid(DifferenceType).name() << std::endl;
	std::cout << "ReferenceType: " << typeid(ReferenceType).name() << std::endl;
	std::cout << "PointerType: " << typeid(PointerType).name() << std::endl;
	std::cout << "IteratorCategory: " << typeid(IteratorCategory).name() << std::endl;

	ValueType temp;
	temp = *it;
	std::cout << "temp: " << temp << std::endl;
}

export void demo1() {
	std::cout << "\n\nrange_demos() called from " << __FILE__ << std::endl << std::endl;

	std::vector<int> v{ 1,2,3,4,5 };
	/*iteratorTraitsTest(v.begin());
	iteratorTraitsTest(v.cbegin());
	iteratorTraitsTest(v.rbegin());
	iteratorTraitsTest(v.crbegin());
	iteratorTraitsTest(v.end());
	iteratorTraitsTest(v.cend());
	iteratorTraitsTest(v.rend());
	iteratorTraitsTest(v.crend());*/
	iteratorTraitsTest(std::cbegin(v));
}

export void demo2() {
	std::vector<int> vec = { 1, 2, 3 };
	std::vector<int> new_elements = { 4, 5, 6 };

	// 使用back_inserter适配器向vec末尾插入新元素
	std::copy(new_elements.begin(), new_elements.end(), std::back_inserter(vec));

	cout << "vec: ";
	for (int n : vec) {
		std::cout << n << ' ';
	}
	cout << endl;
	// 输出：vec: 1 2 3 4 5 6

	std::vector<int> numbers = { 1, 2, 3, 4, 5 };
	cout << "numbers: ";
	// 使用ostream_iterator将vector内容输出到cout
	std::copy(numbers.begin(), numbers.end(), std::ostream_iterator<int>(std::cout, " "));
}
