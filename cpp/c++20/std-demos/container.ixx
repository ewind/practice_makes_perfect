export module container;


import std;

using namespace std;

template <typename T>
void display(const vector<T>& v) {
	for (int i = 0; i < v.size(); i++) {
		cout << v[i] << " ";
	}
	cout << endl;
}

export void container_demos() {
	print("container_demos() called from {__FILE__}\n\n");


	// part1: vector initialization
	vector<int> v1(10); // 10 elements, each initialized to 0
	vector<int> v2(10, 1); // 10 elements, each initialized to 1

	// part2: vector assignment
	v2.assign(5, 2); // 5 elements, each initialized to 2
	v2.assign({ 1,2,3,4,5 }); // 5 elements, each initialized to 2
	v1.swap(v2);
	display(v1); // output: 1 2 3 4 5

	// part3: vector cmp
	vector<int> v3 = { 1,2,3,4,5 };
	vector<int> v4 = { 1,2,3,4,5 };
	cout << (v3 == v4) << endl; // output: 1

	// part4: vector iterator
	vector<int> v5 = { 1,2,3,4,5 };
	for (vector<int>::iterator it{ begin(v5) }; it != end(v5); ++it) {
		cout << *it << " ";
	}
	cout << endl;

	// part5: vector access, insert
	vector<string> strVector{ "hello", "world" };
	for (auto it{ begin(strVector) }; it != end(strVector); ++it) {
		it->append("!");
	}
	// append ?
	for (auto& str : strVector) {
		str.append("?");
	}
	display(strVector);


	// store reference
	string str1{ "hello" };
	string str2{ "world" };
	vector<reference_wrapper<string>> vec{ ref(str1) };
	vec.push_back(ref(str2));
	vec[1].get() += "!";
	cout << str1 << " " << str2 << endl;


	// ************************* list *************************
	list<string> dictionay{ "aardvark", "ambulance" };
	list<string> bWords{ "ball", "bat", "bat", "bat", "bat" };
	dictionay.push_back("canticle");
	dictionay.push_front("apple");
	if (!bWords.empty()) {
		auto iterLastB{ --(cend(bWords)) };
		auto it{ cbegin(dictionay) };
		for (; it != cend(dictionay); ++it) {
			if (*it > *iterLastB) {
				break;
			}
		}
		// add in the b words.  
		dictionay.splice(it, bWords);
	}

	// ************************* array *************************
	array<int, 3> arr{ 1, 2, 3 };
	for (int i = 0; i < arr.size(); i++) {
		cout << arr[i] << " ";
	}
	for (const auto& elem : arr) {
		cout << elem << " ";
	}
	cout << endl;

	// ************************* queue *************************
	queue<int> q;
	q.push(1);
	q.push(2);
	q.push(3);
	for (int i = 0; i < 3; i++) {
		cout << q.front() << " ";
		q.pop();
	}
	cout << "\nq.size() = " << q.size() << endl;

	// ************************* stack *************************
	stack<int> s;
	s.push(1);
	s.push(2);
	s.push(3);
	for (int i = 0; i < 3; i++) {
		cout << s.top() << " ";
		s.pop();
	}
	cout << "\ns.size() = " << s.size() << endl;

	// ************************* priority_queue *************************
	priority_queue<int> pq;
	pq.push(3);
	pq.push(1);
	pq.push(2);
	for (int i = 0; i < 3; i++) {
		cout << pq.top() << " ";
		pq.pop();
	}
	cout << "\npq.size() = " << pq.size() << endl;

	// С���ѣ� greater<int>
	priority_queue<int, vector<int>, greater<int>> pq2;
	pq2.push(3);
	pq2.push(1);
	pq2.push(2);
	for (int i = 0; i < 3; i++) {
		cout << pq2.top() << " ";
		pq2.pop();
	}
	cout << "\npq2.size() = " << pq2.size() << endl;

	// ************************* pair *************************
	pair<int, string> p1{ 1, "hello" };
	cout << p1.first << " " << p1.second << endl;
	pair<string, int> p2;
	p2.first = "world";
	p2.second = 2;
	cout << p2.first << " " << p2.second << endl;

	// ************************* map *************************
	map<string, int> m;
	m["hello"] = 1;
	m["world"] = 2;
	for (const auto& elem : m) {
		cout << elem.first << " " << elem.second << endl;
	}

	map m2{
		pair<string, int>("hello", 1),
		pair<string, int>("world", 2)
	};
	m2.insert({ "hello", 11 }); // will not update
	m2.insert(make_pair("world", 22)); // will not update
	cout << "m2 insert" << endl;
	for (const auto& elem : m2) {
		cout << elem.first << " " << elem.second << endl;
	}
	m2.insert_or_assign("hello", 111); // will update
	m2.insert_or_assign("world", 222); // will update
	cout << "m2 insert_or_assign" << endl;
	for (const auto& elem : m2) {
		cout << elem.first << " " << elem.second << endl;
	}
	// using []
	m2["name"] = 1111;
	m2["age"] = 2222;
	cout << "m2 []" << endl;
	for (const auto& elem : m2) {
		cout << elem.first << " " << elem.second << endl;
	}
	// using [key, value] to iterate
	cout << "m2 [key, value]" << endl;
	for (const auto& [key, value] : m2) {
		cout << key << "->" << value << endl;
	}

	cout << "contained key: " << m2.contains("hello") << endl;

	// unordered_map
	unordered_map<string, int> um;
	um["hello"] = 1;
	um["world"] = 2;
	cout << "um" << endl;
	for (const auto& elem : um) {
		cout << elem.first << " " << elem.second << endl;
	}

	// ************************* set *************************
	set<int> s1{ 1,2,3,4,5 };
	set<int> s2{ 4,5,6,7,8 };
	set<int> s3;
	set_union(s1.begin(), s1.end(), s2.begin(), s2.end(), inserter(s3, s3.begin()));
	cout << "s3" << endl;
	for (const auto& elem : s3) {
		cout << elem << " ";
	}

	// unordered_set
	unordered_set<int> us{ 1,2,3,4,5 };
	cout << "us" << endl;
	for (const auto& elem : us) {
		cout << elem << " ";
	}

	// ************************* multimap *************************
	multimap<string, int> mm;
	mm.insert({ "hello", 20 });
	mm.insert({ "hello", 21 }); // allow duplicate key
	mm.insert({ "hello", 22 }); // allow duplicate key
	mm.insert({ "world", 30 });
	mm.insert({ "world", 30 });
	cout << "mm" << endl;
	for (const auto& elem : mm) {
		cout << elem.first << " " << elem.second << endl;
	}

	// upper_bound, lower_bound
	auto it1 = mm.lower_bound("hello");
	auto it2 = mm.upper_bound("hello");
	cout << "mm [hello]" << endl;
	for (auto it = it1; it != it2; ++it) {
		cout << it->first << " " << it->second << endl;
	}

	// ************************* bitset *************************
	bitset<10> bs;
	bs.set(1);
	bs.set(3);
	bs.set(5);
	cout << "bs ==> " << bs << endl;
	if (bs.test(1)) {
		cout << "bit 1 is set" << endl;
	}

	// initialize bitset with string
	{
		auto str1 = "1010101010";
		auto str2 = "0101010101";
		bitset<10> bs1{ str1 };
		bitset<10> bs2(str2);
		auto bs3 = bs1 & bs2;
		cout << "bs1 & bs2 ==> " << bs3 << endl;
	}
}