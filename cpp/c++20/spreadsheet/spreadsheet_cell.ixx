export module spreadsheet_cell;

import std;

export class SpreadsheetCell {
public:
	void setValue(double value);
	double getValue() const;

	void setString(std::string_view str);
	std::string getString() const;
private:
	std::string doubleToString(double value) const;
	double stringToDouble(std::string_view str) const;

	double value{ 0.0 };
};
