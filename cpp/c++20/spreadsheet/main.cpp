import std;
import spreadsheet_cell;
import spreadsheet;

using namespace std;

Spreadsheet createObject() {
	return Spreadsheet{ 5, 5 };
}

auto main() -> int{
	vector<Spreadsheet> sheets;
	for (size_t i = 0; i < 2; i++) {
		println("iteration {}", i);	
		sheets.push_back(Spreadsheet{100, 100});
		println("");
	}

	println("test 1");
	Spreadsheet sheet{ 5, 5 };
	sheet = createObject();
	println("");

	println("test 2");
	Spreadsheet sheet2{ 5, 5 };
	sheet2 = sheet;
	return 0;
}

//int main() {
//	SpreadsheetCell myCell;
//	myCell.setValue(3.7);
//	cout << myCell.getValue() << endl;
//
//	myCell.setString("5.9");
//	cout << myCell.getString() << endl;
//
//	// object on the heap
//	auto myCellPtr{ make_unique<SpreadsheetCell>() };
//	myCellPtr->setValue(9);
//	cout << myCellPtr->getValue() << endl;
//
//	// spreadsheet
//	Spreadsheet sheet{ 5, 5 };
//
//	sheet.setCellAt(3, 4, myCell);
//	print("sheet[3,4] = {}\n", sheet.getCellAt(3, 4).getValue());
//
//	Spreadsheet sheet2{ std::move(sheet) };
//	print("sheet2[3,4] = {}\n", sheet2.getCellAt(3, 4).getValue());
//
//	// what if we try to access the cell in the original sheet?
//	// this will cause a runtime error
//	// print("sheet[3,4] = {}\n", sheet.getCellAt(3, 4).getValue());
//
//
//	return 0;
//}