export module spreadsheet;

import std;
import spreadsheet_cell;


export class Spreadsheet {
public:
	Spreadsheet(size_t width, size_t height);
	Spreadsheet(const Spreadsheet& src); // copy constructor
	Spreadsheet(Spreadsheet&& src) noexcept; // move constructor

	Spreadsheet& operator=(const Spreadsheet& rhs);
	Spreadsheet& operator=(Spreadsheet&& rhs) noexcept; // move assignment operator
	// delete copy constructor so that we can't copy the object
	//Spreadsheet(const Spreadsheet& src) = delete;
	//Spreadsheet& operator=(const Spreadsheet& rhs) = delete;
	~Spreadsheet();

	void setCellAt(size_t x, size_t y, const SpreadsheetCell& cell);
	SpreadsheetCell& getCellAt(int x, int y);

	void swap(Spreadsheet& other) noexcept;

private:

	bool inRange(int val, int upper) const;
	void verifyCoordinate(int x, int y) const;

	size_t m_width{ 0 };
	size_t m_height{ 0 };
	SpreadsheetCell** m_cells{ nullptr };
};

export void swap(Spreadsheet& lhs, Spreadsheet& rhs) noexcept {
	lhs.swap(rhs);
}
