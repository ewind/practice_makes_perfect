module spreadsheet;

import std;
import spreadsheet_cell;

Spreadsheet::Spreadsheet(size_t width, size_t height) : m_width{ width }, m_height{ height } {
	std::println("normal constructor");
	m_cells = new SpreadsheetCell*[m_width];
	for (size_t i = 0; i < m_width; i++) {
		m_cells[i] = new SpreadsheetCell[m_height];
	}
}

// delegating constructor
// 委托构造函数 (delegating constructor) 是指在一个构造函数中调用另一个构造函数的行为。
Spreadsheet::Spreadsheet(const Spreadsheet& src) : Spreadsheet{ src.m_width, src.m_height } {
	std::println("copy constructor");
	for (size_t i = 0; i < m_width; i++) {
		for (size_t j = 0; j < m_height; j++) {
			m_cells[i][j] = src.m_cells[i][j];
		}
	}
}

//Spreadsheet::Spreadsheet(const Spreadsheet& src) : m_width{ src.m_width }, m_height{ src.m_height } {
//	m_cells = new SpreadsheetCell*[m_width];
//	for (size_t i = 0; i < m_width; i++) {
//		m_cells[i] = new SpreadsheetCell[m_height];
//		for (size_t j = 0; j < m_height; j++) {
//			m_cells[i][j] = src.m_cells[i][j];
//		}
//	}
//}



// good implementation
// copy and swap idiom
void Spreadsheet::swap(Spreadsheet& other) noexcept {
	std::swap(m_width, other.m_width);
	std::swap(m_height, other.m_height);
	std::swap(m_cells, other.m_cells);
}

// swap idiom
Spreadsheet& Spreadsheet::operator=(const Spreadsheet& rhs) {
	std::println("copy assignment operator");
	// self-assignment check
	if (this == &rhs) {
		return *this;
	}

	Spreadsheet temp{ rhs };
	// temp object will be destroyed after this function
	// and the original object will be destroyed
	this->swap(temp);
	return *this;
}

// not good implementation
//Spreadsheet& Spreadsheet::operator=(const Spreadsheet& rhs) {
//	// self-assignment check
//	if (this == &rhs) {
//		return *this;
//	}
//
//	// free the old memory
//	for (size_t i = 0; i < m_width; i++) {
//		delete[] m_cells[i];
//	}
//	delete[] m_cells;
//
//	m_width = rhs.m_width;
//	m_height = rhs.m_height;
//
//	m_cells = new SpreadsheetCell*[m_width];
//	for (size_t i = 0; i < m_width; i++) {
//		m_cells[i] = new SpreadsheetCell[m_height];
//		for (size_t j = 0; j < m_height; j++) {
//			m_cells[i][j] = rhs.m_cells[i][j];
//		}
//	}
//
//	return *this;
//}

Spreadsheet::Spreadsheet(Spreadsheet&& src) noexcept {
	std::println("move constructor");
	this->swap(src);
}

Spreadsheet& Spreadsheet::operator=(Spreadsheet&& rhs) noexcept {
	std::println("move assignment operator");
	auto moved{ std::move(rhs) };
	this->swap(moved);
	return *this;
}

Spreadsheet::~Spreadsheet() {
	for (size_t i = 0; i < m_width; i++) {
		delete[] m_cells[i];
	}
	delete[] m_cells;
}

bool Spreadsheet::inRange(int val, int upper) const {
	return val < upper;
}

void Spreadsheet::setCellAt(size_t x, size_t y, const SpreadsheetCell& cell) {
	verifyCoordinate(x, y);
	m_cells[x][y] = cell;
}

SpreadsheetCell& Spreadsheet::getCellAt(int x, int y) {
	verifyCoordinate(x, y);
	return m_cells[x][y];
}

void Spreadsheet::verifyCoordinate(int x, int y) const {
	if (!inRange(x, m_width)) {
		throw std::out_of_range{std::format("x {} must be less than {}", x, m_width)};
	}

	if (!inRange(y, m_height)) {
		throw std::out_of_range{std::format("y {} must be less than {}", y, m_height)};
	}
}


