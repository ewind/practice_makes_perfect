module spreadsheet_cell;

import std;
using namespace std;

void SpreadsheetCell::setValue(double value) {
	this->value = value;
}

double SpreadsheetCell::getValue() const {
	return value;
}

void SpreadsheetCell::setString(string_view str) {
	value = stringToDouble(str);
}

string SpreadsheetCell::getString() const {
	return doubleToString(value);
}

string SpreadsheetCell::doubleToString(double value) const {
	return to_string(value);
}

double SpreadsheetCell::stringToDouble(string_view str) const {
	return stod(string(str));
}

