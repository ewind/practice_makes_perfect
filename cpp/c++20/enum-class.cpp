//
// Created by cat on 23-11-21.
//
#include "iostream"
#include "limits"

enum class PieceType {
  King,
  Queen,
  Rook,
};

int main() {
  std::cout << std::numeric_limits<int>::max() << std::endl;

  auto a = PieceType::King;
  if (a == PieceType::King) {
    std::cout << "king" << std::endl;
  }

  using enum PieceType;
  if (a == King) {
    std::cout << "king" << std::endl;
  }
}