//
// Created by hshgeek on 2023/11/19.
//
#include "iostream"
#include "string"

using std::string;

struct Item {
  int id;
  string name;
};

int main() {
  int a = 5;
  int b = 4;

  // 打印 a， b 的值和地址
  std::cout << "a: " << a << " b: " << b << std::endl;
  std::cout << "&a: " << &a << " &b: " << &b << std::endl;

  std::swap(a, b);
  std::cout << "after swap" << std::endl;
  // 重新打印 a， b 的值和地址
  std::cout << "a: " << a << " b: " << b << std::endl;
  std::cout << "&a: " << &a << " &b: " << &b << std::endl;

  // struct a, struct b 的版本
  std::cout << "struct version" << std::endl;
  Item item1{1, "item1"};
  Item item2{2, "item2"};
  std::cout << "item1: " << item1.id << " " << item1.name << std::endl;
  std::cout << "item2: " << item2.id << " " << item2.name << std::endl;
  std::cout << "&item1: " << &item1 << " &item2: " << &item2 << std::endl;
  std::swap(item1, item2);
  std::cout << "after swap" << std::endl;
  std::cout << "item1: " << item1.id << " " << item1.name << std::endl;
  std::cout << "item2: " << item2.id << " " << item2.name << std::endl;
  std::cout << "&item1: " << &item1 << " &item2: " << &item2 << std::endl;

  // 指针的版本
  std::cout << "pointer version" << std::endl;
  int *p1 = &a;
  int *p2 = &b;
  std::cout << "p1: " << p1 << " p2: " << p2 << std::endl;
  std::cout << "&p1: " << &p1 << " &p2: " << &p2 << std::endl;
  std::cout << "*p1: " << *p1 << " *p2: " << *p2 << std::endl;
  std::swap(p1, p2);
  std::cout << "after swap" << std::endl;
  std::cout << "p1: " << p1 << " p2: " << p2 << std::endl;
  std::cout << "&p1: " << &p1 << " &p2: " << &p2 << std::endl;
  std::cout << "*p1: " << *p1 << " *p2: " << *p2 << std::endl;

  return 0;
}
