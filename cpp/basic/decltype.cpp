#include "iostream"
#include "type_traits"

int main() {
  // decltype 规则
  std::cout << "decltype 规则" << std::endl;
  int a[3]{};
  std::cout << std::is_same_v<decltype(a), int[3]> << std::endl;

  int i = 42, *p = &i, &r = i;
  decltype(r + 0) i1 = 0; // int
  std::cout << std::is_same_v<decltype(r + 0), int> << std::endl;
  //  decltype(*p) i2;   // int&, but not initialized, compile error
  decltype(*p) i2 = i; // int&
  std::cout << std::is_same_v<decltype(*p), int &> << std::endl;

  decltype(p) i3; // int*
  std::cout << std::is_same_v<decltype(p), int *> << std::endl;
}