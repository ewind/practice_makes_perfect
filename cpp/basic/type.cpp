#include "iostream"
#include "type_traits"

int main() {
  int a[3]{};
  int b;
  std::cout << std::is_same_v<decltype(a), int[3]> << std::endl;
  return 0;
}