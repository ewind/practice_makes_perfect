#include "iostream"
#include "string"
#include "type_traits"

using namespace std;

int main() {
  auto arr = "hello"; // const char *
  auto a = "hello"s;
  string b = "world";
  cout << b << endl;
  cout << std::is_same_v<decltype(arr), const char *> << endl;
  string c = std::move(b); // b will be set to ""
  cout << std::is_same_v<decltype(c), string> << endl;
  cout << "b = " << b << endl;
  cout << "c = " << c << endl;
}