#include "iostream"
#include "type_traits"
#include "vector"
using std::vector;

int main() {
  std::cout << "auto 规则" << std::endl;
  int i = 100;
  int &j = i;
  auto k = j;
  std::cout << std::is_same_v<decltype(k), int> << std::endl;
  // 0: 引用被忽略
  std::cout << std::is_same_v<decltype(k), int &> << std::endl;

  // 指针不能被忽略
  auto p = &j;
  std::cout << std::is_same_v<decltype(p), int *> << std::endl;

  const int ic = 100;
  auto jc = ic;
  // 0: const 被忽略
  std::cout << std::is_same_v<decltype(jc), const int> << std::endl;
  std::cout << std::is_same_v<decltype(jc), int> << std::endl;

  auto &j3 = i;
  // auto & 保留了引用
  std::cout << std::is_same_v<decltype(j3), int> << std::endl;
  std::cout << std::is_same_v<decltype(j3), int &> << std::endl;

  int b = 10;
  int &refb = b;
  auto ab = refb;
  std::cout << std::is_same_v<decltype(refb), int &> << std::endl;
  std::cout << std::is_same_v<decltype(ab), int> << std::endl;
}