#include "iostream"
#include "vector"
using std::vector;

int main() {
  vector<int> v{1, 2, 3, 4, 5};
  for (auto i : v) {
    std::cout << i << " ";
  }
  std::cout << std::endl;

  // const reference
  for (const auto &i : v) {
    std::cout << i << " ";
  }
  std::cout << std::endl;

  for (auto &i : v) {
    i *= i;
  }

  // const reference
  for (const auto &i : v) {
    std::cout << i << " ";
  }
  std::cout << std::endl;
}
