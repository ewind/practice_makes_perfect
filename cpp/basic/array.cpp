//
// Created by hshgeek on 2023/11/19.
//

#include <iostream>

class Simple {
private:
  int i;

public:
  Simple(int i = 0) {
    std::cout << "Simple constructor: " << i << " called!" << std::endl;
  } // default constructor
  ~Simple() { std::cout << "Simple destructor called!" << std::endl; }
};

int main() {
  // a pointer to array of Simple objects
  Simple *pSimple = new Simple[4];
  delete[] pSimple;

  std::cout << "========" << std::endl;
  // array of pointers to Simple objects
  // Simply*[4] is an array of 4 pointers to Simple objects
  Simple **pSimpleArray = new Simple *[4];
  for (int i = 0; i < 4; ++i) {
    pSimpleArray[i] = new Simple(i);
    //    pSimpleArray[i] = new Simple{i};
  }
  for (int i = 0; i < 4; ++i) {
    delete pSimpleArray[i];
  }
  delete[] pSimpleArray;
  return 0;
}