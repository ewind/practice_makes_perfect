#include "map"
#include "iostream"
#include "string"
#include "unordered_map"
#include "utility"

using std::map;
using std::multimap;
using std::pair;
using std::string;
using std::unordered_map;
using std::unordered_multimap;

void print_map(map<string, int> &m) {
  for (auto &p : m) {
    std::cout << p.first << " " << p.second << std::endl;
  }
}

int main() {
  map<string, int> m1;
  m1["hello"] = 1;
  m1.insert({"world", 2});
  m1.insert(pair<string, int>("!", 3));
  print_map(m1);

  if (m1.find(string("hello")) != m1.end()) {
    std::cout << "found" << std::endl;
  } else {
    std::cout << "not found" << std::endl;
  }

  auto cnt = m1.count(string("hello"));
  std::cout << "cnt = " << cnt << std::endl;

  // mutil map
  multimap<string, int> mm1;
  mm1.insert({"hello", 1});
  mm1.insert({"hello", 2});
  mm1.insert({"hello", 3});
  mm1.insert({"world", 4});
  for (auto &p : mm1) {
    std::cout << p.first << " " << p.second << std::endl;
  }

  // unorder map
  unordered_map<string, int> um1;
  um1.insert({"hello", 1});
  um1.insert({"world", 2});
  um1.insert({"!", 3});
  for (auto &p : um1) {
    std::cout << p.first << " " << p.second << std::endl;
  }
}