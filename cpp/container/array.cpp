#include <algorithm>
#include <array>
#include <iostream>

using std::cin, std::cout, std::endl;

int main() {
  // 1. 创建空array，长度为3; 常数复杂度
  std::array<int, 3> v0{};
  v0.fill(1); // 填充数组

  // 2. 用指定常数创建array; 常数复杂度
  std::array<int, 3> v1{1, 2, 3};
  cout << v1[0] << endl;
  cout << v1.at(1) << endl;
  cout << v1.front() << endl;

  auto p = v1.data();
  cout << *p << endl;

  // 访问数组
  // v0
  cout << "v0 ==>";
  for (int i : v0) {
    cout << i << " ";
  }
  cout << endl;
  // v1
  cout << "v1 ==>";
  for (int ele : v1) {
    cout << ele << " ";
  }
  cout << endl;

  // 交换数组
  v0.swap(v1);
  cout << "v1 ==> ";
  for (int e : v1) {
    cout << e << " ";
  }
  cout << endl;

  // 交换数组
  std::swap(v0, v1);
  cout << "v1 ==> ";
  for (int e : v1) {
    cout << e << " ";
  }
  cout << endl;
}
