#include "set"
#include "iostream"
#include "unordered_set"

using std::cin, std::cout, std::endl;
using std::multiset;
using std::set;
using std::unordered_multiset;
using std::unordered_set;

void print(set<int> &s);

int main() {
  // set example
  set<int> s1{1, 2, 3, 4, 5}; // 列表初始化
  print(s1);

  auto i = s1.find(3);
  if (i == s1.end()) {
    cout << "not found" << endl;
  } else {
    cout << "found and erase ";
    s1.erase(i);
  }

  s1.insert(6);
  print(s1);

  auto lb = s1.lower_bound(5); // <= 5
  cout << "lower_bound(5) = " << *lb << endl;
  auto ub = s1.upper_bound(2); // > 3
  cout << "upper_bound(2) = " << *ub << endl;

  // mutil set
  multiset<int> ms1{1, 2, 3, 4, 5, 5, 5, 5};
  cout << "ms1 = ";
  for (auto cb = ms1.cbegin(); cb != ms1.cend(); ++cb) {
    cout << *cb << " ";
  }
  cout << endl;

  // unorder set
  unordered_set<int> s2{1, 2, 3, 4, 5};
  cout << "s2 = ";
  for (int cb : s2) {
    cout << cb << " ";
  }
  cout << endl;

  // check existence
  if (s2.find(3) != s2.end()) {
    cout << "found and erase s2" << endl;
    s2.erase(3);
    for (auto cb = s2.cbegin(); cb != s2.cend(); ++cb) {
      cout << *cb << " ";
    }
    cout << endl;
  } else {
    cout << "not found" << endl;
  }
}

void print(set<int> &s) {
  for (int i : s) {
    cout << i << " ";
  }
  cout << endl;
}