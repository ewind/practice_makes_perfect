#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::iota;
using std::string;
using std::vector;

void print_vector(vector<int> &v);

int main() {
  // 1. 创建空vector; 常数复杂度
  vector<int> v0;
  // 1+. 这句代码可以使得向vector中插入前3个元素时，保证常数时间复杂度
  v0.reserve(3);
  cout << "v0 ==> ";
  print_vector(v0);

  // 2. 创建一个初始空间为3的vector，其元素的默认值是0; 线性复杂度
  vector<int> v1(3);
  cout << "v1 ==> ";
  print_vector(v1);

  // 3. 创建一个初始空间为3的vector，其元素的默认值是2; 线性复杂度
  vector<int> v2(3, 2);
  print_vector(v2);

  // 4. 创建一个初始空间为3的vector，其元素的默认值是1，
  // 并且使用v2的空间配置器; 线性复杂度
  vector<int> v3(3, 1, v2.get_allocator());
  print_vector(v3);

  // 5. 创建一个v2的拷贝vector v4， 其内容元素和v2一样; 线性复杂度
  vector<int> v4(v2);
  print_vector(v4);

  // 6. 创建一个v4的拷贝vector v5，其内容是{v4[1], v4[2]}; 线性复杂度
  vector<int> v5(v4.begin() + 1, v4.begin() + 3);
  print_vector(v5);

  // 7. 移动v2到新创建的vector v6，不发生拷贝; 常数复杂度; 需要 C++11
  vector<int> v6(std::move(v2)); // 或者 v6 = std::move(v2);
  print_vector(v6);

  // iota
  vector<int> pa(10);
  iota(pa.begin(), pa.end(), 0);
  cout << "vector iota function: " << endl;
  print_vector(pa);

  // vector<string>
  vector<string> vs1{"hello", "world"};
  for (const auto &s : vs1) {
    std::cout << s << " ";
  }
  std::cout << std::endl;
  vector<string> vs2{10}; // fucking wield
  std::cout << "vs2.size() = " << vs2.size() << std::endl;
  vector<string> vs3(10);
  std::cout << "vs3.size() = " << vs3.size() << std::endl;

  return 0;
}

void print_vector(vector<int> &v) {
  for (auto i : v) {
    cout << i << " ";
  }
  cout << endl;
}
