#include "deque"
#include "algorithm"
#include "iostream"

using std::cin, std::cout, std::endl;
using std::deque;

void print_deque(const deque<int> &d);

int main() {
  deque<int> d{1, 2, 3, 4, 5};
  cout << "d ==> ";
  print_deque(d);

  d.push_front(0);
  d.push_back(6);

  cout << "after push_front/back: ";
  print_deque(d);

  auto top = d.front();
  cout << "front element = " << top << endl;

  d.pop_front();
  d.pop_back();
  cout << "after pop_front: ";
  print_deque(d);
}

void print_deque(const deque<int> &d) {
  for (int i : d) {
    cout << i << " ";
  }
  cout << endl;
}