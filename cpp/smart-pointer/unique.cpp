//
// Created by hshgeek on 2023/11/19.
//

#include "iostream"
#include "memory"

class Simple {
private:
  int i;

public:
  Simple(int i = 0) {
    this->i = i;
    std::cout << "Simple constructor: " << this->i << " called!" << std::endl;
  } // default constructor
  ~Simple() {
    std::cout << "Simple destructor: " << this->i << " called!" << std::endl;
  }

  int get_data() const { return this->i; }

  void go() { std::cout << "go" << std::endl; }
};

void print_simple(const Simple *pSimple) {
  std::cout << "print_simple: " << pSimple->get_data() << std::endl;
}

int main() {
  auto mySimple = std::make_unique<Simple>(1);
  mySimple->go();

  // 智能指针传参给函数
  // get 方法可以获取 mySimple 的底层指针
  print_simple(mySimple.get());

  // reset 重置底层指针
  mySimple.reset(new Simple(2));
  mySimple->go();

  // release 释放底层指针
  auto *pSimple = mySimple.release();
  std::cout << "pSimple: " << pSimple->get_data() << std::endl;
  delete pSimple;

  return 0;
}
