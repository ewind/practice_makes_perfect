//
// Created by hshgeek on 2023/11/19.
//
#include "iostream"
#include "memory"
#include "string"
#include "vector"

using std::make_shared;
using std::shared_ptr;
using std::string;
using std::vector;

int main() {
  auto p = make_shared<int>(42);
  std::cout << *p << std::endl;

  auto ps = make_shared<string>(10, '9');
  std::cout << *ps << std::endl;

  auto pv = make_shared<vector<string>>();
  pv->emplace_back("hello");
  pv->emplace_back("world");
  for (const auto &s : *pv) {
    std::cout << s << " ";
  }
  std::cout << std::endl;

  // share point 复制
  auto pv2 = pv;
  pv->emplace_back("again");
  for (const auto &s : *pv2) {
    std::cout << s << " ";
  }
  std::cout << std::endl;

  // share point 引用计数
  std::cout << pv.use_count() << std::endl;

  if (!pv2.unique()) {
    pv2.reset(new vector<string>{*pv2});
  }
  pv2->emplace_back("new");

  // 重新打印 pv1, pv2
  std::cout << "pv2: " << std::endl;
  for (const auto &s : *pv2) {
    std::cout << s << " ";
  }
  std::cout << std::endl;

  std::cout << "pv: " << std::endl;
  for (const auto &s : *pv) {
    std::cout << s << " ";
  }
  std::cout << std::endl;
}