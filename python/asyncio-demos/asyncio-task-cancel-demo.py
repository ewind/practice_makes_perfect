import asyncio
from asyncio import CancelledError

async def delay(n):
    print("begin delay")
    await asyncio.sleep(n)
    print("end delay")


async def main():
    # put the coro to the event loop and schedule it to run
    long_task = asyncio.create_task(delay(10))

    seconds_elapsed = 0

    while not long_task.done():
        print(f'task not finished, checking again in a second')
        await asyncio.sleep(1)
        seconds_elapsed += 1
        if seconds_elapsed == 5:
            long_task.cancel()
            print("is done？", long_task.done())
            print("is cancelled？", long_task.cancelled())
            print("long_task cancelled")
    
    try:
        await long_task
    except CancelledError:
        print("long_task was cancelled")

asyncio.run(main())

