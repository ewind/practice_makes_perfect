# coding:utf-8

import asyncio

# 定义一个协程， 直接调用不会执行， 而是生成一个协程对象
# class 'coroutine'
# my_coro() 返回的是一个 coroutine object
async def my_coro():
    print('Hello, World!')

def add_one(x: int) -> int:
    return x + 1

async def coroutine_add_one(x: int) -> int:
    print("coroutine_add_one begin")
    await asyncio.sleep(1)
    print("coroutine_add_one end")
    return x + 1

async def hello_world_message() -> str:
    print("hello_world_message begin")
    await asyncio.sleep(1)
    print("hello_world_message end")
    return 'Hello, World!'


async def main():
    # await 挂起当前协程，等待 my_coro() 执行完毕
    await my_coro()
    function_result = add_one(1)
    coroutine_result =  coroutine_add_one(1)

    print(f'function_result: {function_result} and the type is {type(function_result)}')
    print(f'coroutine_result: {coroutine_result} and the type is {type(coroutine_result)}')
    # function_result: 2 and the type is <class 'int'>
    # coroutine_result: <coroutine object coroutine_add_one at 0x000001C5139C7A00> and the type is <class 'coroutine'>

    print("="*10)
    print("="*10)
    print("="*10)
    print("="*10)

    result = await coroutine_result
    print("coro_add_one", result)

    message = await hello_world_message()
    print("hello world", message)

# 如何运行 coroutine object
asyncio.run(main())