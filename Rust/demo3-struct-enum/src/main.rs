struct Point {
    x: i32,
    y: i32,
}

struct Foo {
    a: i32,
    b: i32,
    c: i32,
    d: i32,
    e: i32,
}

fn main() {
    let origin = Point { x: 0, y: 0 };
    println!("The origin is at ({}, {})", origin.x, origin.y);

    // 初始化
    let mut x = Foo {
        a: 1,
        b: 1,
        c: 2,
        d: 2,
        e: 3,
    };
    x.a = 2;
    println!("{} {} {} {} {}", x.a, x.b, x.c, x.d, x.e);
    let x2 = Foo { e: 4, ..x }; // from x
    println!("{} {} {} {} {}", x2.a, x2.b, x2.c, x2.d, x2.e);

    // tuple struct
    struct Color(i32, i32, i32);
    let mut c = Color(0, 255, 255);
    c.0 = 255;
    match c {
        Color(r, g, b) => println!("({}, {}, {})", r, g, b), // (255, 255, 255)
    }

    // enum
    enum Resultish {
        Ok,
        Warning { code: i32, message: String },
        Err(String),
    }

    // 模式匹配使用场景
    // 模式匹配语句 match
    // 变量绑定，包括 let、if let、while let
    // for 循环
    // 函数和闭包的参数
}
