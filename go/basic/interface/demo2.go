package main

import (
	"fmt"
	"reflect"
)

type Struct1 struct {
	Name string
	Age  int
}

func main() {
	var s = Struct1{
		Name: "test",
		Age:  1,
	}

	sRV := reflect.ValueOf(s)
	sRT := reflect.TypeOf(s)
	fmt.Printf("sRV: %v, sRT: %v\n", sRV, sRT)

	var i1 any = s
	i1V := reflect.ValueOf(i1)
	i1T := reflect.TypeOf(i1)
	fmt.Printf("i1V: %v, i1T: %v\n", i1V, i1T)

	if i1V == sRV {
		fmt.Println("i1V == sRV")
	} else {
		fmt.Println("i1V != sRV")
	}
	if i1T == sRT {
		fmt.Println("i1T == sRT")
	} else {
		fmt.Println("i1T != sRT")
	}

	var i2 any = s
	i2V := reflect.ValueOf(i2)
	i2T := reflect.TypeOf(i2)
	fmt.Printf("i2V: %v, i2T: %v\n", i2V, i2T)

	if i2V == sRV {
		fmt.Println("i2V == sRV")
	} else {
		fmt.Println("i2V != sRV")
	}

	if i2T == sRT {
		fmt.Println("i2T == sRT")
	} else {
		fmt.Println("i2T != sRT")
	}

	fmt.Println("end")
}

/*
sRV: {test 1}, sRT: main.Struct1
i1V: {test 1}, i1T: main.Struct1
i1V != sRV
i1T == sRT
i2V: {test 1}, i2T: main.Struct1
i2V != sRV
i2T == sRT
*/
