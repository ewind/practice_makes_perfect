package main

type Duck interface {
	Quack()
}

type Cat struct {
	Name string
}

//go:noinline
func (c *Cat) Quack() {
	println(c.Name + " meow")
}

func Say(a Duck) {
	a.Quack()
}

func main() {
	var c Duck = &Cat{Name: "draven"}
	Say(c)
}
