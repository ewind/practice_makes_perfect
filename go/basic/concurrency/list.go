package main

import "time"

type User struct {
	Id int
}

var succList = make(chan *User, 10)

func writechan() {
	for i := 0; i < 10; i++ {
		succList <- &User{Id: i}
	}
	close(succList)
}

func readChan() {
	for user := range succList {
		println(user.Id)
	}
}

func main() {
	go writechan()
	readChan()

	time.Sleep(1 * time.Second)
}
