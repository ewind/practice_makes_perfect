package main

import (
	"fmt"
	"reflect"
)

type Person struct {
	Name   string `json:"name"`
	Age    int    `json:"age"`
	Gender string `json:"gender"`
}

func (p *Person) SayHello() {
	fmt.Printf("hello, I'm %s\n\n", p.Name)
}

func (p *Person) SetName(name string) {
	p.Name = name
}

func main() {
	p := Person{
		Name:   "Allen.Wu",
		Age:    25,
		Gender: "male",
	}

	p.SayHello()

	// 此处必须是指针， 否则会panic
	// 因为receiver 是指针类型
	getValue := reflect.ValueOf(&p)

	methodName := getValue.MethodByName("SetName")
	args := []reflect.Value{reflect.ValueOf("He Shuhao")}
	methodName.Call(args)
	p.SayHello()
}
