package main

import (
	"fmt"
	"reflect"
)

func main() {

	var num float64 = 1.2345

	// 反射第一定律：反射可以将“接口类型变量”转换为“反射类型对象”。
	pointer := reflect.ValueOf(&num)
	value := reflect.ValueOf(num)

	// Interface() 方法和 reflect.ValueOf() 恰恰相反
	// Interface() 方法是将 reflect.Value 类型转换为 interface{} 类型
	convertPointer := pointer.Interface().(*float64)
	convertValue := value.Interface().(float64)

	fmt.Println(convertPointer)
	fmt.Println(convertValue)

	e := pointer.Elem()
	fmt.Printf("type of e: %T, value of e: %v\n", e, e)

	//value.SetFloat(7.1)
	if value.CanAddr() {
		value.SetFloat(7.1)
	} else {
		fmt.Println("can not set value")
	}
}
