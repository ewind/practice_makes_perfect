package main

type Person struct {
	Name   string `json:"name" bson:"name"`
	Age    int    `json:"age" bson:"age"`
	Gender string `json:"gender" bson:"gender"`
}

func main() {
	// 解析结构体的bson tag
	// 1. 获取结构体的类型信息
	// 2. 遍历结构体的字段
	// 3. 获取每个字段的tag
	// 4. 解析tag
	// 5. 生成bson.M

	p := &Person{
		Name:   "Allen.Wu",
		Age:    25,
		Gender: "male",
	}
	pp := &p
	ppp := &pp

}
