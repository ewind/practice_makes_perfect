package main

import (
	"fmt"
	"net/http"
	"testing"
)

func HttpDemo2(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hi")
}

func TestHttpDemo2(t *testing.T) {
	// 更多http.Server的字段可以根据情况初始化
	server := http.Server{
		Addr:         ":8080",
		ReadTimeout:  0,
		WriteTimeout: 0,
	}
	http.HandleFunc("/", HttpDemo2)
	server.ListenAndServe()
}
