package main

import (
	"fmt"
	"net/http"
	"testing"
)

func HttpDemo3(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hi")
}

func TestHttpDemo3(t *testing.T) {
	server := http.Server{
		Addr:         ":8080",
		ReadTimeout:  0,
		WriteTimeout: 0,
	}
	mux := http.NewServeMux()
	server.Handler = mux

	mux.HandleFunc("/", HttpDemo3)
	server.ListenAndServe()
}
