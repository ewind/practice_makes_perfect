package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	//ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(3*time.Second))
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go func(ctx context.Context) {
		i := 0
		ticker := time.NewTicker(1 * time.Second)
		defer ticker.Stop()
		for {
			select {
			case <-ctx.Done():
				fmt.Println("exit...")
				return
			case <-ticker.C:
				i++
				fmt.Printf("%ds\n", i)
			}
		}
	}(ctx)

	time.Sleep(1 * time.Minute)
}
