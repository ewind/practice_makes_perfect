package main

import (
	"fmt"
	"net/http"
	"testing"
)

func HttpDemo1(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hi")
}

func TestHttpDemo1(t *testing.T) {
	mux := http.NewServeMux()

	mux.HandleFunc("/", HttpDemo1)
	http.ListenAndServe(":8080", mux)
}
