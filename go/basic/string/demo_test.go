package main

import "testing"

func TestStrToBytes(t *testing.T) {
	str := "hello"
	bytes := StrToBytes(str)
	t.Logf("bytes: %v", bytes)
}

func TestBytesToStr(t *testing.T) {
	bytes := []byte{104, 101, 108, 108, 111}
	str := BytesToStr(bytes)
	t.Logf("str: %v", str)
}
