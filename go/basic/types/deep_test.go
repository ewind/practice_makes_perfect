package main

import (
	"fmt"
	"reflect"
	"testing"
)

func TestDeepEq(t *testing.T) {
	var a, b []string = nil, []string{}
	fmt.Println(reflect.DeepEqual(a, b)) // "false"

	var c, d map[string]int = nil, make(map[string]int)
	fmt.Println(reflect.DeepEqual(c, d)) // "false"
}
