package main

import (
	"fmt"
	"testing"
	"unsafe"
)

func TestMap(t *testing.T) {
	var d = map[string]int{}
	fmt.Println(unsafe.Sizeof(d))

	d["hello"] = 1
	fmt.Println(unsafe.Sizeof(d))

	var s = []string{}
	fmt.Println(unsafe.Sizeof(s))
	s = append(s, "hello")
	fmt.Println(unsafe.Sizeof(s))
}
