package main

import (
	"fmt"
	"testing"
	"unsafe"
)

func TestPointer(t *testing.T) {
	var x struct {
		a bool
		b int16
		c []int
	}

	// 和 pb := &x.b 等价
	// unsafe.Pointer <==> uintptr
	pb := (*int16)(unsafe.Pointer(
		uintptr(unsafe.Pointer(&x)) + unsafe.Offsetof(x.b)))
	*pb = 42
	fmt.Println(x.b) // "42"
}
