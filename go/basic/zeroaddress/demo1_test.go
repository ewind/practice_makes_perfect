package main

import (
	"fmt"
	"testing"
)

func TestDemo1(t *testing.T) {
	var (
		a struct{}
		b [0]int
		c [100]struct{}
		d = make([]struct{}, 100)
	)

	/*	输出结果：固定的地址，
		go version go1.21.4 linux/amd64
		a: 0x64e860
		b: 0x64e860
		c: 0x64e860
		d: 0x64e860
	*/
	fmt.Printf("a: %p\n", &a)
	fmt.Printf("b: %p\n", &b)
	fmt.Printf("c: %p\n", &c[50])
	fmt.Printf("d: %p\n", &d[50])
}
