package main

import (
	"fmt"
	"net/http"
	"time"
)

// http 对 127.0.0.1:8080 发起 http 请求
func main() {
	client := &http.Client{
		Timeout: 10 * time.Second,
	}
	resp, err := client.Get("http://127.0.0.1:8080/hello")
	fmt.Println(err)
	fmt.Println(resp.Body)
}
