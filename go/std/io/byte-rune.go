package io

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

func Utf8Index(str, substr string) int {
	index := strings.Index(str, substr)
	fmt.Println("string-index=", index)
	runeIndex := utf8.RuneCountInString(str[:index])
	fmt.Println("rune-index=", runeIndex)
	return 0
}
