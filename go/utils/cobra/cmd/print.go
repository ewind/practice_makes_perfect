package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var printFlag string

var printCmd = &cobra.Command{
	Use: "print [OPTIONS] [COMMANDS]",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("run print...")
		fmt.Printf("printFlag: %v\n", printFlag)
		fmt.Printf("Source: %v\n", source)
	},
}

func init() {
	rootCmd.AddCommand(printCmd)

	// 本地标志
	printCmd.Flags().StringVarP(&printFlag, "flag", "f", "", "print flag for local")
}
