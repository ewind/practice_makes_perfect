package main

import (
	"gitee.com/ewind/practice_makes_perfect/go/utils/cobra/cmd"
)

func main() {
	cmd.Execute()
}
