package main

import "go.uber.org/zap"

func main() {
	logger, _ := zap.NewProduction()
	defer logger.Sync()

	logger.Info("demo3:",
		zap.String("app", "start ok"),
		zap.Int("major version", 3),
	)
	logger.Error("demo3:",
		zap.String("app", "crash"),
		zap.Int("reason", -1),
	)
}
