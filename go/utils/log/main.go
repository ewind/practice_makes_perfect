package main

func main() {
	var tops = []TeeOption{
		{
			Filename: "access.log",
			Ropt: RotateOptions{
				MaxSize:    1,
				MaxAge:     1,
				MaxBackups: 3,
				Compress:   true,
			},
			Lef: func(lvl Level) bool {
				return lvl <= InfoLevel
			},
		},
		{
			Filename: "error.log",
			Ropt: RotateOptions{
				MaxSize:    1,
				MaxAge:     1,
				MaxBackups: 3,
				Compress:   true,
			},
			Lef: func(lvl Level) bool {
				return lvl > InfoLevel
			},
		},
	}

	logger := NewTeeWithRotate(tops)
	ResetDefault(logger)

	for i := 0; i < 20000; i++ {
		Info("demo3:", String("app", "start ok"),
			Int("major version", 3))
		Error("demo3:", String("app", "crash"),
			Int("reason", -1))
	}

}
