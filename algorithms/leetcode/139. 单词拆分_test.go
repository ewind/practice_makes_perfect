package leetcode

// TODO(heshuhao): Timeout
//func wordBreak(s string, wordDict []string) bool {
//	sort.Slice(wordDict, func(i, j int) bool {
//		return len(wordDict[i]) > len(wordDict[j])
//	})
//
//	memo := make(map[string]bool)
//	var dfs func(s string, wordDict []string) bool
//	dfs = func(s string, wordDict []string) bool {
//		if len(s) == 0 {
//			return true
//		}
//
//		if v, ok := memo[s]; ok {
//			return v
//		}
//
//		var flag bool
//		for _, word := range wordDict {
//			n := len(word)
//			if len(s) >= n && s[:n] == word {
//				flag = wordBreak(s[n:], wordDict)
//				if flag {
//					break
//				} else {
//					memo[s[n:]] = false
//				}
//			}
//		}
//		memo[s] = flag
//		return flag
//	}
//
//	return dfs(s, wordDict)
//}

func wordBreak(s string, wordDict []string) bool {
	wordDictSet := make(map[string]bool)
	for _, w := range wordDict {
		wordDictSet[w] = true
	}
	dp := make([]bool, len(s)+1)
	dp[0] = true
	for i := 1; i <= len(s); i++ {
		for j := 0; j < i; j++ {
			if dp[j] && wordDictSet[s[j:i]] {
				dp[i] = true
				break
			}
		}
	}
	return dp[len(s)]
}
