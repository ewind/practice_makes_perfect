package leetcode

import (
	"fmt"
	"testing"
)

func permute(nums []int) [][]int {
	var ans [][]int
	var path []int
	var n = len(nums)
	var used = make(map[int]bool)

	var dfs func()
	dfs = func() {
		if len(path) == n {
			temp := make([]int, n)
			copy(temp, path)
			ans = append(ans, temp)
			return
		}

		for _, e := range nums {
			if used[e] {
				continue
			}

			path = append(path, e)
			used[e] = true
			dfs()
			path = path[:len(path)-1]
			used[e] = false
		}
	}

	dfs()
	return ans
}

//func permute(nums []int) [][]int {
//	var ans [][]int
//	var path []int
//	var n = len(nums)
//	var used = make(map[int]bool)
//
//	var dfs func(idx int)
//	dfs = func(idx int) {
//		if len(path) == n {
//			temp := make([]int, n)
//			copy(temp, path)
//			ans = append(ans, temp)
//			return
//		}
//
//		cur := nums[idx]
//		path = append(path, cur)
//		used[cur] = true
//
//		for idx, e := range nums {
//			if used[e] {
//				continue
//			}
//			dfs(idx)
//		}
//
//		path = path[:len(path)-1]
//		used[cur] = false
//	}
//
//	dfs(0)
//	return ans
//}

func Test_permute(t *testing.T) {
	ans := permute([]int{1, 2, 3})
	fmt.Println(ans)
}
