package leetcode

import "testing"

func pivotIndex(nums []int) int {
	if len(nums) <= 1 {
		return 0
	}

	sum := make([]int, len(nums))

	for i, v := range nums {
		if i == 0 {
			sum[i] = v
		} else {
			sum[i] = sum[i-1] + v
		}
	}

	for i := range sum {
		var leftPart, rightPart int
		if i == 0 {
			leftPart = 0
			rightPart = sum[len(sum)-1] - sum[0]
		} else {
			rightPart = sum[len(sum)-1] - sum[i]
			leftPart = sum[i-1]
		}

		if leftPart == rightPart {
			return i
		}
	}
	return -1
}

func TestPivotIndex(t *testing.T) {
	table := []struct {
		input  []int
		expect int
	}{
		{
			[]int{1, 7, 3, 6, 5, 6},
			3,
		},
		{
			[]int{2, 1, -1},
			0,
		},
	}

	for _, tt := range table {
		if actual := pivotIndex(tt.input); actual != tt.expect {
			t.Errorf("pivotIndex(%v) = %d; expected %d", tt.input, actual, tt.expect)
		}
	}
}
