package leetcode

import (
	"fmt"
	"testing"
)

func moveZeroes(nums []int) {
	left, right := 0, 0
	for left < len(nums) && nums[left] != 0 {
		left++
		right++
	}
	for right < len(nums) {
		if nums[right] != 0 {
			nums[left], nums[right] = nums[right], nums[left]
			left++
			right++
		} else {
			right++
		}
	}

	fmt.Printf("%v\n", nums)
}

func TestMoveZeroes(t *testing.T) {
	nums := []int{0, 1, 0, 3, 12}
	moveZeroes(nums)
}
