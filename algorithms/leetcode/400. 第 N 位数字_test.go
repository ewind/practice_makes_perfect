package leetcode

import (
	"fmt"
	"math"
	"strconv"
	"testing"
)

func findNthDigit(n int) int {
	width, reminder := convert(n)
	if reminder == 0 {
		return 9
	}

	count := (reminder + width) / width // 向上取整
	left := reminder % width
	targ := int(math.Pow(10, float64(width-1))) + count - 1
	if left == 0 {
		targ--
		s := strconv.Itoa(targ)
		fmt.Printf("target=%s, left=%v", s, left)
		return int(s[len(s)-1] - '0')
	}

	s := strconv.Itoa(targ)
	fmt.Printf("target=%s, left=%v", s, left)
	return int(s[left-1] - '0')
}

func convert(n int) (int, int) {
	i := 1
	pow := 1
	for n > digits(i, pow) {
		n -= digits(i, pow)
		pow *= 10
		i++
	}

	return i, n
}

func digits(i, pow int) int {
	return 9 * pow * i
}

func Test_findNthDigit(t *testing.T) {
	table := []struct {
		n      int
		wanted int
	}{
		{
			n:      1000,
			wanted: 3,
		},
		{
			n:      11,
			wanted: 0,
		},
		{
			n:      12,
			wanted: 1,
		},
	}

	for _, tt := range table {
		if got := findNthDigit(tt.n); tt.wanted != got {
			t.Errorf("n:%v, wanted:%v, got:%v",
				tt.n, tt.wanted, got)
		}
	}

}
