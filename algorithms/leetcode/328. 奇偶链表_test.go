package leetcode

func oddEvenList(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}

	var evenList *ListNode
	var evenCur *ListNode
	var oddList *ListNode
	var oddCur *ListNode

	cur := head
	i := 0
	for cur != nil {
		i++
		next := cur.Next
		if i%2 == 0 {
			if evenList == nil {
				evenList = cur
				evenCur = evenList
			} else {
				evenCur.Next = cur
				evenCur = evenCur.Next
				evenCur.Next = nil
			}
		} else {
			// odd list
			if oddList == nil {
				oddList = cur
				oddCur = oddList
			} else {
				oddCur.Next = cur
				oddCur = oddCur.Next
				oddCur.Next = nil
			}
		}
		cur = next
	}

	return concat(oddList, evenList)
}

func concat(odd, even *ListNode) *ListNode {
	if odd == nil {
		return even
	}
	if even == nil {
		return odd
	}

	p := odd
	for p.Next != nil {
		p = p.Next
	}
	p.Next = even
	return odd
}
