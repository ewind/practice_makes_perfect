package leetcode

import (
	"fmt"
	"testing"
)

func exist(board [][]byte, word string) bool {
	m, n := len(board), len(board[0])
	dxs := []int{-1, 1, 0, 0}
	dys := []int{0, 0, -1, 1}

	var visited = make([][]bool, m)
	for i := range visited {
		visited[i] = make([]bool, n)
	}

	var path []byte
	var dfs func(x, y int) bool
	dfs = func(x, y int) bool {
		visited[x][y] = true
		path = append(path, board[x][y])

		if len(path) == len(word) {
			return true
		}

		for i := 0; i < 4; i++ {
			dx := dxs[i]
			dy := dys[i]
			nx, ny := x+dx, y+dy
			if nx < 0 || nx >= m || ny < 0 || ny >= n {
				continue
			}
			if visited[nx][ny] {
				continue
			}
			if board[nx][ny] != word[len(path)] {
				continue
			}

			if dfs(nx, ny) {
				return true
			}
		}

		path = path[:len(path)-1]
		visited[x][y] = false
		return false
	}

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if board[i][j] != byte(word[0]) {
				continue
			}
			if dfs(i, j) {
				return true
			}
		}
	}
	return false
}

func Test_exist(t *testing.T) {
	boards := [][]byte{{'A', 'B'}, {'C', 'D'}}
	word := "ABCD"

	ans := exist(boards, word)
	fmt.Println(ans)
}
