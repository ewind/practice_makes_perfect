package leetcode

import "testing"

func minReorder(n int, connections [][]int) int {
	e := make([][][]int, n)

	var dfs func(x, parent int) int
	dfs = func(x, parent int) int {
		res := 0
		for _, edge := range e[x] {
			if edge[0] == parent {
				continue
			}
			// 从 0 点出发， dfs 搜索各个节点
			res += edge[1] + dfs(edge[0], x)
		}
		return res
	}

	for _, edge := range connections {
		e[edge[0]] = append(e[edge[0]], []int{edge[1], 1})
		e[edge[1]] = append(e[edge[1]], []int{edge[0], 0})
	}
	return dfs(0, -1)
}

func Test_minReorder(t *testing.T) {
	table := []struct {
		Connections [][]int
		N           int
		Wanted      int
	}{
		{
			Connections: [][]int{{0, 1}, {1, 3}, {2, 3}, {4, 0}, {4, 5}},
			N:           6,
			Wanted:      3,
		},
	}
	for _, tt := range table {
		if got := minReorder(tt.N, tt.Connections); got != tt.Wanted {
			t.Errorf("connections:%v, n:%v, wanted:%v, got:%v",
				tt.Connections, tt.N, tt.Wanted, got)
		}
	}
}
