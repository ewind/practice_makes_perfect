package leetcode

type MyQueue struct {
	dataStack []int
	luxStack  []int
}

func Constructor() MyQueue {
	return MyQueue{
		dataStack: make([]int, 0),
		luxStack:  make([]int, 0),
	}
}

func (q *MyQueue) Push(x int) {
	q.dataStack = append(q.dataStack, x)
}

func (q *MyQueue) Pop() int {
	for len(q.dataStack) > 0 {
		x := pop(&q.dataStack)
		q.luxStack = append(q.luxStack, x)
	}

	x := pop(&q.luxStack)

	for len(q.luxStack) > 0 {
		item := pop(&q.luxStack)
		q.dataStack = append(q.dataStack, item)
	}
	return x
}

func (q *MyQueue) Peek() int {
	for len(q.dataStack) > 0 {
		x := pop(&q.dataStack)
		q.luxStack = append(q.luxStack, x)
	}

	x := top(q.luxStack)
	for len(q.luxStack) > 0 {
		item := pop(&q.luxStack)
		q.dataStack = append(q.dataStack, item)
	}

	return x

}

func (q *MyQueue) Empty() bool {
	return len(q.dataStack) == 0
}

func top(stack []int) int {
	return stack[len(stack)-1]
}

func pop(stack *[]int) int {
	x := (*stack)[len(*stack)-1]
	*stack = (*stack)[:len(*stack)-1]
	return x
}

/**
 * Your MyQueue object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Push(x);
 * param_2 := obj.Pop();
 * param_3 := obj.Peek();
 * param_4 := obj.Empty();
 */
