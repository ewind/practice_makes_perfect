package leetcode

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func widthOfBinaryTree(root *TreeNode) int {
	if root == nil {
		return 0
	}

	q := []*TreeNode{root}
	indexes := []int{1}
	ans := 1
	for len(q) != 0 {
		n := len(q)
		for i := 0; i < n; i++ {
			cur := q[0]
			idx := indexes[0]
			q = q[1:]
			indexes = indexes[1:]

			if cur.Left != nil {
				q = append(q, cur.Left)
				indexes = append(indexes, idx*2)
			}
			if cur.Right != nil {
				q = append(q, cur.Right)
				indexes = append(indexes, idx*2+1)
			}
		}

		if len(indexes) > 0 {
			ans = max(ans, indexes[len(indexes)-1]-indexes[0]+1)
		}
	}

	return ans
}
