package leetcode

type Node struct {
	key  int
	val  int
	next *Node
	prev *Node
}

type LRUCache struct {
	cap  int
	data map[int]*Node
	head *Node
	tail *Node
}

func Constructor(capacity int) LRUCache {
	cache := LRUCache{
		cap:  capacity,
		data: make(map[int]*Node),
		head: &Node{},
		tail: &Node{},
	}

	cache.head.next = cache.tail
	cache.tail.prev = cache.head

	return cache
}

func (c *LRUCache) Get(key int) int {
	node, ok := c.data[key]
	if !ok {
		return -1
	}

	c.moveToHead(node)
	return node.val
}

func (c *LRUCache) addToHead(node *Node) {
	node.prev = c.head
	node.next = c.head.next
	c.head.next.prev = node
	c.head.next = node
}

func (c *LRUCache) removeTail() *Node {
	node := c.tail.prev
	c.removeNode(node)
	return node
}

func (c *LRUCache) removeNode(node *Node) {
	node.prev.next = node.next
	node.next.prev = node.prev
}

func (c *LRUCache) moveToHead(node *Node) {
	c.removeNode(node)
	c.addToHead(node)
}

func (c *LRUCache) Put(key int, value int) {
	// 更新
	node, ok := c.data[key]
	if ok {
		node.val = value
		c.data[key] = node
		c.moveToHead(node)
		return
	}

	node = &Node{key: key, val: value}
	c.data[key] = node
	c.addToHead(node)
	if len(c.data) > c.cap {
		tail := c.removeTail()
		delete(c.data, tail.key)
	}
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * obj := Constructor(capacity);
 * param_1 := obj.Get(key);
 * obj.Put(key,value);
 */
