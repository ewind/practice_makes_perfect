package leetcode

type ListNode struct {
	Val  int
	Next *ListNode
}

func reorderList(head *ListNode) {
	helper(head)
}

func helper(head *ListNode) *ListNode {
	// 少于两个元素
	if head == nil || head.Next == nil || head.Next.Next == nil {
		return head
	}

	lastSecond := head
	for lastSecond.Next.Next != nil {
		lastSecond = lastSecond.Next
	}

	second := head.Next
	head.Next = lastSecond.Next
	lastSecond.Next = nil

	head.Next.Next = helper(second)
	return head
}
