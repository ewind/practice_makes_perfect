package leetcode

import (
	"fmt"
	"reflect"
	"testing"
)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func leafSimilar(root1 *TreeNode, root2 *TreeNode) bool {
	var ls1 []int
	collectLeaf(root1, &ls1)

	var ls2 []int
	collectLeaf(root2, &ls2)

	return reflect.DeepEqual(ls1, ls2)
}

func collectLeaf(root *TreeNode, ls *[]int) {
	if root == nil {
		return
	}

	if root.Left == nil && root.Right == nil {
		*ls = append(*ls, root.Val)
		return
	}

	if root.Left != nil {
		collectLeaf(root.Left, ls)
	}

	if root.Right != nil {
		collectLeaf(root.Right, ls)
	}
}

func NewTreeNode(val int) *TreeNode {
	return &TreeNode{
		Val: val,
	}
}

func Test_leafSimilar(t *testing.T) {
	root1 := &TreeNode{
		Val:   1,
		Left:  NewTreeNode(2),
		Right: NewTreeNode(3),
	}

	root2 := &TreeNode{
		Val:   1,
		Left:  NewTreeNode(3),
		Right: NewTreeNode(2),
	}

	fmt.Printf("ans=%v", leafSimilar(root1, root2))
}
