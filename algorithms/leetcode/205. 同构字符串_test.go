package leetcode

func isIsomorphic(s string, t string) bool {
	if len(s) != len(t) {
		return false
	}

	var s2tMap = make(map[byte]byte)
	var t2sMap = make(map[byte]byte)

	for i := 0; i < len(s); i++ {
		sc := byte(s[i])
		tc := byte(t[i])

		// 相同字母必须映射到相同的字母
		if v, ok := s2tMap[sc]; ok && v != tc {
			return false
		}
		// 被映射字母的原字母必须相同
		if v, ok := t2sMap[tc]; ok && v != sc {
			return false
		}

		s2tMap[sc] = tc
		t2sMap[tc] = sc
	}

	return true
}
