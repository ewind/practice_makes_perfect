package leetcode

func canPermutePalindrome(s string) bool {
	c := count(s)

	oddC := 0
	for _, v := range c {
		if v%2 != 0 {
			oddC++
		}
	}

	return oddC <= 1
}

func count(s string) map[byte]int {
	var ret = make(map[byte]int)

	for _, c := range s {
		bc := byte(c)
		ret[bc]++
	}

	return ret
}
