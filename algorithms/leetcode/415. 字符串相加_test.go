package leetcode

import (
	"fmt"
	"strings"
	"testing"
)

func addStrings(num1 string, num2 string) string {
	if len(num1) < len(num2) {
		num1, num2 = num2, num1
	}

	var prefixZero []string
	for i := 0; i < len(num1)-len(num2); i++ {
		prefixZero = append(prefixZero, "0")
	}
	prefixZero = append(prefixZero, num2)
	num2 = strings.Join(prefixZero, "")

	var ans = make([]byte, len(num1)+1)
	var carry byte = '0'
	for i := len(num1) - 1; i >= 0; i-- {
		s, c := add(num1[i], num2[i], carry)
		ans[i+1] = s
		carry = c
	}
	if carry == '1' {
		ans[0] = '1'
	} else {
		ans[0] = '0'
	}

	if ans[0] == '0' {
		ans = ans[1:]
	}
	return string(ans)
}

func toInt(n byte) int {
	return int(n - '0')
}

func toByte(n int) byte {
	return byte(n + '0')
}

func add(n1, n2, carry byte) (byte, byte) {
	s := toInt(n1) + toInt(n2) + toInt(carry)

	if s >= 10 {
		s = s % 10
		carry = '1'
	} else {
		carry = '0'
	}

	return toByte(s), carry
}

func Test_addStrings(t *testing.T) {
	ans := addStrings("11", "123")
	fmt.Println(ans)
}
