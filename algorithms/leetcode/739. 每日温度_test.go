package leetcode

import (
	"reflect"
	"testing"
)

func dailyTemperatures(temperatures []int) []int {
	if len(temperatures) == 1 {
		return []int{0}
	}

	var stack []int
	var ans = make([]int, len(temperatures))
	for i := len(temperatures) - 1; i >= 0; i-- {
		for len(stack) > 0 && temperatures[stack[len(stack)-1]] <= temperatures[i] {
			stack = stack[:len(stack)-1]
		}

		if len(stack) == 0 {
			ans[i] = 0
		} else {
			top := stack[len(stack)-1]
			ans[i] = top - i
		}
		stack = append(stack, i)
	}
	return ans
}

func Test_dailyTemperatures(t *testing.T) {
	table := []struct {
		temp   []int
		wanted []int
	}{
		{
			temp:   []int{73, 74, 75, 71, 69, 72, 76, 73},
			wanted: []int{1, 1, 4, 2, 1, 1, 0, 0},
		},
	}

	for _, tt := range table {
		if got := dailyTemperatures(tt.temp); !reflect.DeepEqual(tt.wanted, got) {
			t.Errorf("wanted: %v, got: %v",
				tt.wanted, got)
		}
	}
}
