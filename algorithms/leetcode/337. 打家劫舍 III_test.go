package leetcode

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

var memo map[*TreeNode]int

func rob(root *TreeNode) int {
	memo = make(map[*TreeNode]int)

	var dp func(root *TreeNode) int
	dp = func(root *TreeNode) int {
		if root == nil {
			return 0
		}

		if _, ok := memo[root]; ok {
			return memo[root]
		}

		// 选择 root
		sum1 := root.Val
		if root.Left != nil {
			sum1 += dp(root.Left.Left)
			sum1 += dp(root.Left.Right)
		}
		if root.Right != nil {
			sum1 += dp(root.Right.Left)
			sum1 += dp(root.Right.Right)
		}

		// 不选择 root
		sum2 := dp(root.Left) + dp(root.Right)
		memo[root] = max(sum1, sum2)
		return memo[root]
	}

	return dp(root)
}
