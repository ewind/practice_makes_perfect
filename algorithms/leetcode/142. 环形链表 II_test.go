package leetcode

type ListNode struct {
	Val  int
	Next *ListNode
}

// 双指针的两次运用
func detectCycle(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}

	f, s := head, head
	for f != nil {
		s = s.Next
		if f.Next == nil {
			return nil
		}
		f = f.Next.Next
		if f == s {
			p := head
			for p != s {
				p = p.Next
				s = s.Next
			}

			return s
		}
	}

	return nil
}
