package leetcode

import "testing"

func minFlips(a int, b int, c int) int {
	var ans int
	for i := 0; i < 31; i++ {
		bitA := (a >> i) & 1
		bitB := (b >> i) & 1
		bitC := (c >> i) & 1

		if bitC == 0 {
			ans += bitA + bitB
		} else {
			// bitC == 1
			if bitA+bitB == 0 {
				ans++
			}
		}
	}
	return ans
}

func Test_minFlips(t *testing.T) {

}
