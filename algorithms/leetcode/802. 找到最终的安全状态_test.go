package leetcode

import (
	"fmt"
	"testing"
)

func eventualSafeNodes(graph [][]int) (ans []int) {
	n := len(graph)
	color := make([]int, n)
	var safe func(int) bool
	safe = func(x int) bool {
		if color[x] > 0 {
			return color[x] == 2
		}
		color[x] = 1
		for _, y := range graph[x] {
			if !safe(y) {
				return false
			}
		}
		color[x] = 2
		return true
	}
	for i := 0; i < n; i++ {
		if safe(i) {
			ans = append(ans, i)
		}
	}
	return
}

func Test_eventualSafeNodes(t *testing.T) {
	graph := [][]int{{}, {0, 3, 2, 4}, {3, 1}, {4}, {}}
	ans := eventualSafeNodes(graph)
	fmt.Println(ans)
}

// TODO(heshuhao): why so ineffective?
//func eventualSafeNodes(graph [][]int) []int {
//	n := len(graph)
//	var ans []int
//	var vis = make([]bool, n)
//
//	var terminalNode = make(map[int]bool)
//	for i := 0; i < n; i++ {
//		if len(graph[i]) == 0 {
//			terminalNode[i] = true
//		}
//	}
//	if len(terminalNode) == 0 {
//		return nil
//	}
//
//	var dfs func(i int) bool
//	dfs = func(i int) bool {
//		if terminalNode[i] {
//			return true
//		}
//
//		vis[i] = true
//		var safe = true
//		for _, e := range graph[i] {
//			if vis[e] {
//				return false
//			}
//			temp := dfs(e)
//			safe = safe && temp
//		}
//		vis[i] = false
//		return safe
//	}
//
//	for i := 0; i < n; i++ {
//		if dfs(i) {
//			ans = append(ans, i)
//		}
//	}
//
//	slices.Sort(ans)
//	return ans
//}
