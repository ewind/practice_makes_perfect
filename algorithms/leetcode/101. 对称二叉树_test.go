package leetcode

func isSymmetric(root *TreeNode) bool {
	if root == nil {
		return false
	}

	return helper(root.Left, root.Right)
}

func helper(l, r *TreeNode) bool {
	if l == nil && r == nil {
		return true
	}
	if l == nil && r != nil {
		return false
	}
	if l != nil && r == nil {
		return false
	}

	if l.Val != r.Val {
		return false
	}

	return helper(l.Right, r.Left) && helper(l.Left, r.Right)
}
