package leetcode

func maxProfit(prices []int) int {
	low := []int{prices[0]}

	lowest := low[0]
	for i := 1; i < len(prices); i++ {
		if prices[i] < lowest {
			lowest = prices[i]
		}
		low = append(low, lowest)
	}

	var maxProfile = 0
	for i := 0; i < len(prices); i++ {
		if prices[i]-low[i] > maxProfile {
			maxProfile = prices[i] - low[i]
		}
	}

	return maxProfile
}
