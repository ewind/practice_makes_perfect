package leetcode

import "testing"

func longestOnes(nums []int, k int) int {
	if len(nums) == 0 {
		return 0
	}

	var zeroPos []int
	var ret int

	l, r := 0, 0
	for r < len(nums) {
		ele := nums[r]
		if ele == 1 {
			r++
			continue
		}

		zeroPos = append(zeroPos, r)
		if len(zeroPos) > k {
			if r-l > ret {
				ret = r - l
			}
			l = zeroPos[0] + 1
			zeroPos = zeroPos[1:]
		}
		r++
	}

	if r-l > ret {
		ret = r - l
	}
	return ret
}

func TestLongestOnes(t *testing.T) {
	table := []struct {
		input  []int
		k      int
		output int
	}{
		{
			input:  []int{1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0},
			k:      2,
			output: 6,
		},
		{
			input:  []int{0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1},
			k:      3,
			output: 10,
		},
	}

	for _, tt := range table {
		if actual := longestOnes(tt.input, tt.k); actual != tt.output {
			t.Errorf("longestOnes(%v, %v) = %v, expected %v", tt.input, tt.k, actual, tt.output)
		}
	}
}
