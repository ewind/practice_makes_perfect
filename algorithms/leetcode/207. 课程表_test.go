package leetcode

// cycle detect
func canFinish(numCourses int, prerequisites [][]int) bool {
	g := make([][]int, numCourses)
	for _, p := range prerequisites {
		from, to := p[1], p[0]
		g[from] = append(g[from], to)
	}

	var onStack = make(map[int]bool)
	var visited = make([]bool, numCourses)
	var hasCycle bool

	var cycleDetect func(c int)
	cycleDetect = func(c int) {
		if hasCycle {
			return
		}

		visited[c] = true
		onStack[c] = true
		for _, adj := range g[c] {
			if onStack[adj] {
				hasCycle = true
				return
			}
			if !visited[adj] {
				cycleDetect(adj)
			}
		}
		onStack[c] = false
	}

	for i := 0; i < numCourses; i++ {
		if !visited[i] {
			cycleDetect(i)
		}
		if hasCycle {
			return false
		}
	}

	return true
}
