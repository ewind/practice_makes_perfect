package leetcode

import "math"

const Flag = math.MaxInt64

func setZeroes(matrix [][]int) {
	m, n := len(matrix), len(matrix[0])

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if matrix[i][j] == 0 {
				matrix[i][j] = Flag
			}
		}
	}

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if matrix[i][j] == Flag {
				doZero(matrix, i, j)
			}
		}
	}
}

func doZero(matrix [][]int, row, col int) {
	m, n := len(matrix), len(matrix[0])
	matrix[row][col] = 0

	for y := 0; y < n; y++ {
		if matrix[row][y] != Flag {
			matrix[row][y] = 0
		}

	}

	for x := 0; x < m; x++ {
		if matrix[x][col] != Flag {
			matrix[x][col] = 0
		}
	}
}
