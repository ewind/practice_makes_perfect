package leetcode

var directions = [][2]int{
	{-1, 0}, // 上
	{1, 0},  // 下
	{0, -1}, // 左
	{0, 1},  // 右
}

func numIslands(grid [][]byte) int {
	var cc = 0

	m, n := len(grid), len(grid[0])
	var visited = make([][]bool, m)
	for i := range visited {
		visited[i] = make([]bool, n)
	}

	var dfs func(i, j int)
	dfs = func(i, j int) {
		visited[i][j] = true

		for _, d := range directions {
			nx := i + d[0]
			ny := j + d[1]
			if nx >= 0 && nx < m && ny >= 0 && ny < n && !visited[nx][ny] && grid[nx][ny] == '1' {
				dfs(nx, ny)
			}
		}
	}

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if !visited[i][j] && grid[i][j] == '1' {
				cc++
				dfs(i, j)
			}
		}
	}

	return cc
}
