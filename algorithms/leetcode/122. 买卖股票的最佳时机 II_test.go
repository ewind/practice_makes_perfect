package leetcode

import "testing"

func maxProfit(prices []int) int {
	n := len(prices)
	var profits = make([][2]int, n)
	profits[0][0] = 0
	profits[0][1] = -prices[0]

	for i := 1; i < n; i++ {
		profits[i][0] = max(profits[i-1][0], profits[i-1][1]+prices[i])
		profits[i][1] = max(profits[i-1][1], profits[i-1][0]-prices[i])
	}

	return profits[n-1][0]
}

func Test_maxprofileII(t *testing.T) {
	table := []struct {
		prices []int
		wanted int
	}{
		{
			prices: []int{7, 1, 5, 3, 6, 4},
			wanted: 7,
		},
		{
			prices: []int{1, 2, 3, 4, 5},
			wanted: 4,
		},
	}

	for _, tt := range table {
		if got := maxProfit(tt.prices); got != tt.wanted {
			t.Errorf("wanted:%v, got:%v", tt.wanted, got)
		}
	}
}
