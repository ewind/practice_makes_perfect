package leetcode

func longestPalindrome(s string) int {
	sc := count(s)

	var ans = 0
	var flag = true
	for _, v := range sc {
		if v%2 == 0 {
			ans += v
			continue
		}

		if flag {
			ans += v
			flag = false
		} else {
			ans += v - 1
		}
	}
	return ans
}

func count(s string) map[byte]int {
	var ret = make(map[byte]int)

	for _, c := range s {
		bc := byte(c)
		ret[bc]++
	}

	return ret
}
