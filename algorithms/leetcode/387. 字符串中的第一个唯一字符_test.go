package leetcode

func firstUniqChar(s string) int {
	sMap := count(s)

	for idx, c := range s {
		if v := sMap[byte(c)]; v == 1 {
			return idx
		}
	}

	return -1
}

func count(s string) map[byte]int {
	var ret = make(map[byte]int)

	for _, c := range s {
		bc := byte(c)
		ret[bc]++
	}

	return ret
}
