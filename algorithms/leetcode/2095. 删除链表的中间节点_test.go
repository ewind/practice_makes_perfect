package leetcode

type ListNode struct {
	Val  int
	Next *ListNode
}

func deleteMiddle(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return nil
	}

	dummy := &ListNode{
		Next: head,
	}

	n := 0
	cur := dummy
	for cur.Next != nil {
		n++
		cur = cur.Next
	}

	mid := n / 2
	cur = dummy
	for i := 0; i < mid; i++ {
		cur = cur.Next
	}

	cur.Next = cur.Next.Next
	return dummy.Next
}
