package leetcode

import "slices"

func twoSum(nums []int, target int, start int) [][]int {
	lo, hi := start, len(nums)-1
	var res = make([][]int, 0)

	for lo < hi {
		left, right := nums[lo], nums[hi]
		s := left + right
		if s < target {
			for lo < hi && nums[lo] == left {
				lo++
			}
		} else if s > target {
			for lo < hi && nums[hi] == right {
				hi--
			}
		} else {
			res = append(res, []int{nums[lo], nums[hi]})
			for lo < hi && nums[lo] == left {
				lo++
			}
			for lo < hi && nums[hi] == right {
				hi--
			}
		}
	}

	return res
}

func threeSumTarget(nums []int, target int) [][]int {
	var res = make([][]int, 0)
	var n = len(nums)

	// 外层进行排序
	slices.Sort(nums)

	for i := 0; i < n; i++ {
		var tuples = twoSum(nums, target-nums[i], i+1)
		for _, tl := range tuples {
			tl = append(tl, nums[i])
			res = append(res, tl)
		}

		for i < n-1 && nums[i] == nums[i+1] {
			i++
		}
	}

	return res
}

func threeSum(nums []int) [][]int {
	return threeSumTarget(nums, 0)
}
