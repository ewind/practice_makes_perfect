package leetcode

func climbStairs(n int) int {
	t1, t2 := 1, 2

	if n <= 2 {
		return n
	}

	for i := 3; i <= n; i++ {
		t1, t2 = t2, t1+t2
	}

	return t2
}
