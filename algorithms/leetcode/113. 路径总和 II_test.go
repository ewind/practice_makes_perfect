package leetcode

import (
	"fmt"
	"testing"
)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func pathSum(root *TreeNode, targetSum int) [][]int {
	if root == nil {
		return nil
	}

	var ans [][]int
	var path []int
	var dfs func(root *TreeNode, targetSum int)
	dfs = func(root *TreeNode, sum int) {
		// 叶子节点
		if root.Left == nil && root.Right == nil {
			if sum == root.Val {
				temp := make([]int, len(path))
				copy(temp, path)
				temp = append(temp, root.Val)
				ans = append(ans, temp)
			}
			return
		}

		path = append(path, root.Val)
		if root.Left != nil {
			dfs(root.Left, sum-root.Val)
		}
		if root.Right != nil {
			dfs(root.Right, sum-root.Val)
		}
		path = path[:len(path)-1]
	}

	dfs(root, targetSum)
	return ans
}

func Test_pathSumII(t *testing.T) {
	root := &TreeNode{
		Val: 5,
		Left: &TreeNode{
			Val: 4,
			Left: &TreeNode{
				Val: 11,
				Left: &TreeNode{
					Val: 7,
				},
				Right: &TreeNode{
					Val: 2,
				},
			},
		},
		Right: &TreeNode{
			Val: 8,
			Left: &TreeNode{
				Val: 13,
			},
			Right: &TreeNode{
				Val: 4,
				Left: &TreeNode{
					Val: 5,
				},
				Right: &TreeNode{
					Val: 1,
				},
			},
		},
	}

	ans := pathSum(root, 22)
	fmt.Println(ans)
}
