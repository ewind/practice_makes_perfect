package leetcode

import "testing"

func lengthOfLongestSubstring(s string) int {
	if len(s) <= 1 {
		return len(s)
	}

	var ans = 1
	l, r := 0, 1
	tmp := map[uint8]int{
		s[l]: l,
	} // s[i] -> i
	for r = 1; r < len(s); r++ {
		prev, ok := tmp[s[r]]

		// r 进入滑动窗口
		tmp[s[r]] = r

		// s[r] 不存在
		if !ok || prev < l {
			ans = max(ans, r-l+1)
		} else {
			// r 存在，而且在滑动窗口内
			ans = max(ans, r-l)
			l = prev + 1
		}
	}

	ans = max(ans, r-l)
	return ans
}

func Test_lengthOfLongestSubstring(t *testing.T) {
	table := []struct {
		s      string
		wanted int
	}{
		{
			s:      "abcabcbb",
			wanted: 3,
		},
		{
			s:      "bbbbb",
			wanted: 1,
		},
		{
			s:      "dvdf",
			wanted: 3,
		},
		{
			s:      "aabaab!bb",
			wanted: 3,
		},
		{
			s:      "uv",
			wanted: 2,
		},
	}

	for _, tt := range table {
		if got := lengthOfLongestSubstring(tt.s); got != tt.wanted {
			t.Errorf("s: %v, wanted: %v, got: %v",
				tt.s, tt.wanted, got)
		}
	}
}
