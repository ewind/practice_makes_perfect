# coding: utf-8
class Solution(object):
    def decodeString(self, s):
        """
        :type s: str
        :rtype: str
        """
        stack = []

        res = ""

        for idx, c in enumerate(s):
            if c != "]":
                stack.append(c)
            else:
                temp = ""
                while True:
                    top = stack.pop(-1)
                    if top == "[":
                        break
                    temp = top + temp

                times = int(stack.pop(-1))
                temp = temp * times
                if len(stack) == 0:
                    res += temp
                else:
                    stack.append(temp)

        post = ""
        while stack:
            top = stack.pop(-1)
            post = top + post

        return res + post

if __name__ == '__main__':
    s = Solution().decodeString("100[leetcode]")
    print(s)

