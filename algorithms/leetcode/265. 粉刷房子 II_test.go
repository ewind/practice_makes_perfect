package leetcode

func minCostII(costs [][]int) int {
	m, n := len(costs), len(costs[0])
	dp := make([][]int, m)
	for i := range dp {
		dp[i] = make([]int, n)
	}
	dp[0] = costs[0]

	for i := 1; i < m; i++ {
		for j := 0; j < n; j++ {
			mc := 99999999
			for k := 0; k < n; k++ {
				if k != j {
					mc = min(mc, dp[i-1][k]+costs[i][j])
				}
			}

			dp[i][j] = mc
		}
	}

	var ans = 99999999
	for _, e := range dp[m-1] {
		ans = min(ans, e)
	}
	return ans
}
