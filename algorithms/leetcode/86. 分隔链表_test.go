package leetcode

import (
	"fmt"
	"testing"
)

type ListNode struct {
	Val  int
	Next *ListNode
}

func partition(head *ListNode, x int) *ListNode {
	if head == nil {
		return nil
	}

	var dummy = &ListNode{
		Next: head,
	}

	var nextBig *ListNode
	var cur *ListNode
	for cur = dummy; cur.Next != nil; cur = cur.Next {
		if cur.Next.Val > x {
			nextBig = cur
			break
		}
	}

	// 来到链表尾
	if cur.Next == nil {
		return dummy.Next
	}
	if nextBig == nil {
		return dummy.Next
	}

	for cur = nextBig.Next; cur != nil && cur.Next != nil; {
		if cur.Next.Val < x {
			targ := cur.Next
			cur.Next = cur.Next.Next
			targ.Next = nextBig.Next
			nextBig.Next = targ
			nextBig = nextBig.Next
		} else {
			cur = cur.Next
		}
	}

	return dummy.Next
}

func Test_partition(t *testing.T) {
	head := &ListNode{
		Val: 1,
		Next: &ListNode{
			Val: 4,
			Next: &ListNode{
				Val: 3,
				Next: &ListNode{
					Val: 0,
					Next: &ListNode{
						Val: 2,
						Next: &ListNode{
							Val: 5,
							Next: &ListNode{
								Val: 2,
							},
						},
					},
				},
			},
		},
	}

	ans := partition(head, 3)
	fmt.Println(ans)
}
