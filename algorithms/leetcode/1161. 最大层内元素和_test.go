package leetcode

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func maxLevelSum(root *TreeNode) int {
	if root == nil {
		return 0
	}

	var maxSum = -9999999999
	var minLevel = 1
	var curLevel = 0

	queue := []*TreeNode{root}
	for len(queue) > 0 {
		eleCnt := len(queue)
		curLevel++

		var sum = 0
		for i := 0; i < eleCnt; i++ {
			cur := queue[0]
			queue = queue[1:]
			sum += cur.Val

			if cur.Left != nil {
				queue = append(queue, cur.Left)
			}
			if cur.Right != nil {
				queue = append(queue, cur.Right)
			}
		}

		if sum > maxSum {
			maxSum = sum
			minLevel = curLevel
		}
	}

	return minLevel
}
