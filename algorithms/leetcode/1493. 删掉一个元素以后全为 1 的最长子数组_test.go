package leetcode

import "testing"

func longestSubarray(nums []int) int {
	if len(nums) <= 1 {
		return 0
	}

	var zeroPos []int
	var ret = 0
	l, r := 0, 0
	for ; r < len(nums); r++ {
		if r != len(nums)-1 && nums[r] == 1 {
			continue
		}

		if nums[r] == 0 {
			zeroPos = append(zeroPos, r)
		}

		// 两个0 或者来到最后一个元素
		if len(zeroPos) > 1 {
			if r-l-1 > ret {
				ret = r - l - 1
			}
			l = zeroPos[0] + 1
			zeroPos = zeroPos[1:]
			continue
		}

		if r == len(nums)-1 {
			if r-l > ret {
				ret = r - l
			}
		}
	}

	return ret
}

func TestLongestSubarray(t *testing.T) {
	table := []struct {
		input  []int
		output int
	}{
		{
			input:  []int{1, 1, 0, 1},
			output: 3,
		},
		{
			input:  []int{0, 1, 1, 1, 0, 1, 1, 0, 1},
			output: 5,
		},
		{
			input:  []int{1, 1, 1},
			output: 2,
		},
		{
			input:  []int{1, 1, 0, 0, 1, 1, 1, 0, 1},
			output: 4,
		},
		{
			input:  []int{0, 0, 0},
			output: 0,
		},
	}

	for _, table := range table {
		if actual := longestSubarray(table.input); actual != table.output {
			t.Errorf("longestSubarray(%v) = %v, expected %v", table.input, actual, table.output)
		}
	}
}
