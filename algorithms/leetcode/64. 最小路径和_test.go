package leetcode

func minPathSum(grid [][]int) int {
	m, n := len(grid), len(grid[0])

	cost := make([][]int, m)
	for i := range cost {
		cost[i] = make([]int, n)
	}

	cost[0][0] = grid[0][0]
	for i := 1; i < m; i++ {
		cost[i][0] = cost[i-1][0] + grid[i][0]
	}
	for j := 1; j < n; j++ {
		cost[0][j] = cost[0][j-1] + grid[0][j]
	}

	for i := 1; i < m; i++ {
		for j := 1; j < n; j++ {
			cost[i][j] = min(cost[i][j-1], cost[i-1][j]) + grid[i][j]
		}
	}
	return cost[m-1][n-1]
}
