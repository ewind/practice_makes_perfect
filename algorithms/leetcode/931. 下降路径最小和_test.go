package leetcode

func minFallingPathSum(matrix [][]int) int {
	m, n := len(matrix), len(matrix[0])
	dp := make([][]int, m)
	for i := range dp {
		dp[i] = make([]int, n)
	}

	for i := 0; i < n; i++ {
		dp[0][i] = matrix[0][i]
	}

	for i := 1; i < m; i++ {
		for j := 0; j < n; j++ {
			temp := dp[i-1][j]
			if j > 0 {
				temp = min(temp, dp[i-1][j-1])
			}
			if j < n-1 {
				temp = min(temp, dp[i-1][j+1])
			}

			dp[i][j] = temp + matrix[i][j]
		}
	}

	var ans = 9999999999
	for _, e := range dp[m-1] {
		ans = min(ans, e)
	}
	return ans
}
