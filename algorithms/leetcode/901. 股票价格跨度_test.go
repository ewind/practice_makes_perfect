package leetcode

import (
	"fmt"
	"testing"
)

type StockSpanner struct {
	priceList []int
	stack     []int
}

func Constructor() StockSpanner {
	return StockSpanner{
		stack:     make([]int, 0),
		priceList: make([]int, 0),
	}
}

func (s *StockSpanner) Next(price int) int {
	s.priceList = append(s.priceList, price)
	toPushIndex := len(s.priceList) - 1

	for len(s.stack) > 0 && s.priceList[top(s.stack)] <= price {
		s.stack = pop(s.stack)
	}

	if len(s.stack) == 0 {
		s.stack = append(s.stack, toPushIndex)
		return toPushIndex + 1
	}

	topEle := top(s.stack)
	s.stack = append(s.stack, toPushIndex)
	ans := len(s.priceList) - 1 - topEle
	return ans
}

func top(stack []int) int {
	return stack[len(stack)-1]
}

func pop(stack []int) []int {
	return stack[:len(stack)-1]
}

/**
 * Your StockSpanner object will be instantiated and called as such:
 * obj := Constructor();
 * param_1 := obj.Next(price);
 */

func Test_Stock(t *testing.T) {
	ss := Constructor()
	var ans []int
	ans = append(ans, ss.Next(100))
	ans = append(ans, ss.Next(80))
	ans = append(ans, ss.Next(60))
	ans = append(ans, ss.Next(70))
	ans = append(ans, ss.Next(60))
	ans = append(ans, ss.Next(75))
	ans = append(ans, ss.Next(85))
	fmt.Println(ans)
}
