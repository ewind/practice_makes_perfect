package leetcode

var co = []int{2, 3, 5}

func nthUglyNumber(n int) int {
	var ul = make([]int, n+1)
	ul[1] = 1
	ul[2] = 2
	ul[3] = 3
	ul[4] = 4
	ul[5] = 5
	if n <= 5 {
		return ul[n]
	}

	for i := 6; i <= n; i++ {
		minUgly := 1
		for j := 1; j < i; j++ {
			for _, c := range co {
				temp := ul[j] * c
				if temp > ul[i-1] {
					minUgly = min(minUgly, temp)
				}
			}
		}
	}

	return ul[n]
}
