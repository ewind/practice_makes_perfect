package leetcode

import "testing"

func removeStars(s string) string {
	var stack []byte

	for _, e := range s {
		if e == '*' {
			if len(stack) > 0 {
				stack = stack[:len(stack)-1]
			}
		} else {
			stack = append(stack, byte(e))
		}
	}

	return string(stack)
}

func TestRemoveStars(t *testing.T) {
	table := []struct {
		Input  string
		Wanted string
	}{
		{
			"leet**cod*e",
			"lecoe",
		},
		{
			"erase*****",
			"",
		},
	}

	for _, tt := range table {
		if got := removeStars(tt.Input); got != tt.Wanted {
			t.Errorf("input:%v, wanted: %v, got： %v", tt.Input, tt.Wanted, got)
		}
	}
}
