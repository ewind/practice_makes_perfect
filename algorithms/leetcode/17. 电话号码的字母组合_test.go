package leetcode

import (
	"reflect"
	"testing"
)

/*
给定一个仅包含数字 2-9 的字符串，返回所有它能表示的字母组合。答案可以按 任意顺序 返回。

给出数字到字母的映射如下（与电话按键相同）。注意 1 不对应任何字母。

示例 1：

输入：digits = "23"
输出：["ad","ae","af","bd","be","bf","cd","ce","cf"]
示例 2：

输入：digits = ""
输出：[]
示例 3：

输入：digits = "2"
输出：["a","b","c"]
*/
//func letterCombinations(digits string) []string {
//	if digits == "" {
//		return []string{}
//	}
//
//	table := map[byte][]string{
//		'2': []string{"a", "b", "c"},
//		'3': []string{"d", "e", "f"},
//		'4': []string{"g", "h", "i"},
//		'5': []string{"j", "k", "l"},
//		'6': []string{"m", "n", "o"},
//		'7': []string{"p", "q", "r", "s"},
//		'8': []string{"t", "u", "v"},
//		'9': []string{"w", "x", "y", "z"},
//	}
//
//	nums := []byte(digits)
//	var ans []string
//	var dfs func(nums []byte)
//	dfs = func(nums []byte){
//		if len(nums) == 0 {
//			return
//		}
//
//		letters := table[nums[0]]
//		for _, c := range letters {
//
//		}
//	}
//	return nil
//}

var phoneMap map[string]string = map[string]string{
	"2": "abc",
	"3": "def",
	"4": "ghi",
	"5": "jkl",
	"6": "mno",
	"7": "pqrs",
	"8": "tuv",
	"9": "wxyz",
}

var combinations []string

func letterCombinations(digits string) []string {
	if len(digits) == 0 {
		return []string{}
	}
	combinations = []string{}
	backtrack(digits, 0, "")
	return combinations
}

func backtrack(digits string, index int, combination string) {
	if index == len(digits) {
		combinations = append(combinations, combination)
	} else {
		digit := string(digits[index])
		letters := phoneMap[digit]
		lettersCount := len(letters)
		for i := 0; i < lettersCount; i++ {
			backtrack(digits, index+1, combination+string(letters[i]))
		}
	}
}

func Test_letterCombinations(t *testing.T) {
	table := []struct {
		digits string
		wanted []string
	}{
		{
			"23",
			[]string{"ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"},
		},
	}

	for _, tt := range table {
		if got := letterCombinations(tt.digits); !reflect.DeepEqual(tt.wanted, got) {
			t.Errorf("wanted: %v, got: %v",
				tt.wanted, got)
		}
	}
}
