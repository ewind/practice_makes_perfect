package leetcode

import (
	"fmt"
	"testing"
)

func spiralOrder(matrix [][]int) []int {
	m, n := len(matrix), len(matrix[0])
	lx, ly := 0, 0
	rx, ry := m-1, n-1

	var ans []int
	for lx <= rx && ly <= ry {
		helper(matrix, lx, ly, rx, ry, &ans)
		lx++
		ly++
		rx--
		ry--
	}

	return ans
}

func helper(matrix [][]int, lx, ly, rx, ry int, ans *[]int) {
	// 一行
	if lx == rx {
		for j := ly; j <= ry; j++ {
			*ans = append(*ans, matrix[lx][j])
		}
		return
	}

	// 一列
	if ly == ry {
		for i := lx; i <= rx; i++ {
			*ans = append(*ans, matrix[i][ly])
		}
		return
	}

	// 上
	for j := ly; j <= ry; j++ {
		*ans = append(*ans, matrix[lx][j])
	}
	// 右
	for i := lx + 1; i <= rx; i++ {
		*ans = append(*ans, matrix[i][ry])
	}
	// 下
	for j := ry - 1; j >= ly; j-- {
		*ans = append(*ans, matrix[rx][j])
	}
	// 左
	for i := rx - 1; i > lx; i-- {
		*ans = append(*ans, matrix[i][ly])
	}
}

func Test_spiralOrder(t *testing.T) {
	matrix := [][]int{
		{1, 2, 3, 4},
		{5, 6, 7, 8},
		{9, 10, 11, 12},
		{13, 14, 15, 16},
	}

	ans := spiralOrder(matrix)
	fmt.Println(ans)
}
