package leetcode

func validateStackSequences(pushed []int, popped []int) bool {
	var stack []int

	for _, e := range pushed {
		stack = append(stack, e)
		for len(stack) > 0 && top(stack) == popped[0] {
			stack = pop(stack)
			popped = popped[1:]
		}
	}

	return len(stack) == 0
}

func pop(stack []int) []int {
	return stack[:len(stack)-1]
}

func top(stack []int) int {
	return stack[len(stack)-1]
}
