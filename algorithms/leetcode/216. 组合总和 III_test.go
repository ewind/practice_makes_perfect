package leetcode

import (
	"reflect"
	"testing"
)

func combinationSum3(k int, target int) [][]int {
	var res [][]int

	var dfs func(start int, target int, path []int)
	dfs = func(start int, target int, path []int) {
		if len(path) == k {
			if target == 0 {
				res = append(res, append([]int{}, path...))
			}
			return
		}
		for i := start; i <= 9; i++ {
			if (target - i) < 0 {
				return
			}

			path = append(path, i)
			dfs(i+1, target-i, path)
			path = path[:len(path)-1]
		}
	}

	dfs(1, target, nil)
	return res
}

func Test_combinationSum3(t *testing.T) {
	table := []struct {
		k      int
		sum    int
		wanted [][]int
	}{
		{
			3,
			7,
			[][]int{{1, 2, 4}},
		},
	}

	for _, tt := range table {
		if got := combinationSum3(tt.k, tt.sum); !reflect.DeepEqual(got, tt.wanted) {
			t.Errorf("wanted: %v, got: %v", tt.wanted, got)
		}
	}
}
