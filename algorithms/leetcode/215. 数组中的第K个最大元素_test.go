package leetcode

import (
	"container/heap"
	"testing"
)

type IntHeap struct {
	data []int
}

func (h *IntHeap) Less(i, j int) bool {
	return h.data[i] > h.data[j]
}

func (h *IntHeap) Len() int {
	return len(h.data)
}

func (h *IntHeap) Swap(i, j int) {
	h.data[i], h.data[j] = h.data[j], h.data[i]
}

func (h *IntHeap) Push(x any) {
	e := x.(int)
	h.data = append(h.data, e)
}

func (h *IntHeap) Pop() any {
	old := h.data[h.Len()-1]
	h.data = h.data[:h.Len()-1]
	return old
}

func findKthLargest(nums []int, k int) int {
	intHeap := &IntHeap{
		data: nums,
	}

	heap.Init(intHeap)

	for i := 1; i < k; i++ {
		heap.Pop(intHeap)
	}

	ans := heap.Pop(intHeap).(int)
	return ans
}

func TestFindKthLargest(t *testing.T) {
	table := []struct {
		Input  []int
		K      int
		Wanted int
	}{
		{
			Input:  []int{3, 2, 1, 5, 6, 4},
			K:      2,
			Wanted: 5,
		},
	}

	for _, tt := range table {
		if got := findKthLargest(tt.Input, tt.K); got != tt.Wanted {
			t.Errorf("input: %v, wanted: %v, got: %v", tt.Input, tt.Wanted, got)
		}
	}
}
