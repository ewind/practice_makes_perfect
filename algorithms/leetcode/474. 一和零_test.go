package leetcode

type ZeroOnes struct {
	Zero int
	One  int
}

func calcZO(str string) ZeroOnes {
	var zero, one int
	for _, e := range str {
		if e == '0' {
			zero++
		} else {
			one++
		}
	}

	return ZeroOnes{
		Zero: zero,
		One:  one,
	}
}

func findMaxForm(strs []string, m int, n int) int {
	var ZOs []ZeroOnes
	for _, s := range strs {
		ZOs = append(ZOs, calcZO(s))
	}

	dp := make([][]int, m+1)
	for i := range dp {
		dp[i] = make([]int, n+1)
	}

	var ans int
	for i := 1; i <= len(ZOs); i++ {
		mi, ni := ZOs[i].Zero, ZOs[i].One
		for j := m; j >= mi; j-- {
			for k := n; k >= ni; k-- {
				dp[j][k] = max(dp[j][k], dp[j-mi][k-ni]+1)
				ans = max(ans, dp[j][k])
			}
		}
	}

	return ans
}
