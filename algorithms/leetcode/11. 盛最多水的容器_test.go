package leetcode

import "testing"

func maxArea(height []int) int {
	l, r := 0, len(height)-1

	maxArea := 0
	for l < r {
		area := min(height[l], height[r]) * (r - l)
		if area > maxArea {
			maxArea = area
		}

		if height[l] < height[r] {
			l++
		} else {
			r--
		}
	}
	return maxArea
}

func TestMaxAre(t *testing.T) {
	// 表格驱动
	tests := []struct {
		height []int
		want   int
	}{
		{[]int{1, 8, 6, 2, 5, 4, 8, 3, 7}, 49},
		{[]int{1, 1}, 1},
		{[]int{4, 3, 2, 1, 4}, 16},
	}

	for _, tt := range tests {
		if got := maxArea(tt.height); got != tt.want {
			t.Errorf("maxArea(%v) = %v, want %v", tt.height, got, tt.want)
		}
	}
}
