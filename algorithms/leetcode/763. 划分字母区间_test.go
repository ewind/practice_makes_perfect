package leetcode

func partitionLabels(s string) []int {
	idxMap := make(map[rune][2]int)

	for idx, r := range s {
		if v, ok := idxMap[r]; ok {
			v[1] = idx
		} else {
			idxMap[r] = [2]int{idx, idx}
		}
	}

	var res = make([]int, 0)
	curSegment := [2]int{0, 0}

	for idx, r := range s {
		if idx == 0 {
			curSegment = idxMap[r]
			continue
		}

		temp := idxMap[r]
		if temp[0] < curSegment[1] {
			curSegment[1] = max(curSegment[1], temp[1])
			continue
		}
		res = append(res, len(curSegment))
		curSegment = temp
	}

	return res
}
