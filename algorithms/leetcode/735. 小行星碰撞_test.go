package leetcode

import (
	"reflect"
	"testing"
)

func asteroidCollision(asteroids []int) (st []int) {
	for _, aster := range asteroids {
		alive := true
		for alive && aster < 0 && len(st) > 0 && st[len(st)-1] > 0 {
			alive = st[len(st)-1] < -aster // aster 是否存在
			if st[len(st)-1] <= -aster {   // 栈顶行星爆炸
				st = st[:len(st)-1]
			}
		}
		if alive {
			st = append(st, aster)
		}
	}
	return
}

//func asteroidCollision(asteroids []int) []int {
//	var stack []int
//	for _, as := range asteroids {
//		if len(stack) == 0 {
//			stack = append(stack, as)
//			continue
//		}
//
//		if as > 0 {
//			stack = append(stack, as)
//			continue
//		}
//
//		if stack[len(stack)-1] < 0 {
//			stack = append(stack, as)
//			continue
//		}
//
//		// as < 0
//		for len(stack) > 0 {
//			// 同向
//			if stack[len(stack)-1] < 0 {
//				stack = append(stack, as)
//				break
//			}
//
//			// 比栈顶质量更轻
//			if stack[len(stack)-1] > -as {
//				break
//			}
//
//			// 一起销毁
//			if stack[len(stack)-1] == -as {
//				stack = stack[:len(stack)-1]
//				break
//			}
//
//			// 销毁一个
//			stack = stack[:len(stack)-1]
//			if len(stack) == 0 {
//				stack = append(stack, as)
//				break
//			}
//		}
//	}
//	return stack
//}

func TestAsteroidCollision(t *testing.T) {
	table := []struct {
		Input  []int
		Wanted []int
	}{
		{
			[]int{5, 10, -5},
			[]int{5, 10},
		},
		{
			[]int{10, 2, -5},
			[]int{10},
		},
		{
			[]int{8, -8},
			[]int{},
		},
		{
			[]int{1, -2, -2, -2},
			[]int{-2, -2, -2},
		},
	}

	for _, tt := range table {
		if got := asteroidCollision(tt.Input); !reflect.DeepEqual(got, tt.Wanted) {
			t.Errorf("input: %v, wanted: %v, got: %v", tt.Input, tt.Wanted, got)
		}
	}
}
