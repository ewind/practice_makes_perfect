package leetcode

func gcdOfStrings(str1 string, str2 string) string {
	if str1+str2 != str2+str1 {
		return ""
	}

	return str1[0:gcd(len(str1), len(str2))]
}

func gcd(a, b int) int {
	reminder := a % b
	for reminder != 0 {
		a = b
		b = reminder
		reminder = a % b
	}

	return b
}
