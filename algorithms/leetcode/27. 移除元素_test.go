package leetcode

func removeElement(nums []int, val int) int {

	l, r := 0, len(nums)-1

	for {
		for l <= r && nums[l] != val {
			l++
		}

		for l <= r && nums[r] == val {
			r--
		}

		if l > r {
			break
		}

		nums[l], nums[r] = nums[r], nums[l]
		l++
		r--
	}

	return l
}
