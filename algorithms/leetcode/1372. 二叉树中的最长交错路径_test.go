package leetcode

// TODO(heshuhao): all test cases passed, but timeout
type Dir int

const (
	Left Dir = iota
	Right
)

func longestZigZag(root *TreeNode) int {
	var res = 0
	var visited = make(map[*TreeNode]bool)
	travel(root, visited, &res)
	return res
}

func doZipZag(root *TreeNode, lastDir Dir) int {
	var nodeCount = 0
	for root != nil {
		nodeCount++
		if lastDir == Left {
			root = root.Right
			lastDir = Right
		} else {
			root = root.Left
			lastDir = Left
		}
	}
	return nodeCount
}

func travel(root *TreeNode, visited map[*TreeNode]bool, res *int) {
	if root == nil {
		return
	}

	if visited[root] {
		return
	}

	visited[root] = true

	ls := doZipZag(root.Left, Left)
	rs := doZipZag(root.Right, Right)
	if *res < max(rs, ls) {
		*res = max(rs, ls)
	}

	travel(root.Left, visited, res)
	travel(root.Right, visited, res)
}
