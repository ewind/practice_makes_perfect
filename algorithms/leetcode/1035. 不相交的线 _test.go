package leetcode

func maxUncrossedLines(nums1 []int, nums2 []int) int {
	n1, n2 := len(nums1), len(nums2)

	dp := make([][]int, n1)
	for i := range dp {
		dp[i] = make([]int, n2)
	}

	// dp[i][j] =
	// 	相等：dp[i-1][j-1] + 1
	//  不相同： max(dp[i-1][j], dp[i][j-1])
	for i := 0; i < n2; i++ {
		if nums1[0] == nums2[i] {
			dp[0][i] = 1
		}
	}
	for i := 0; i < n1; i++ {
		if nums1[i] == nums2[0] {
			dp[i][0] = 1
		}
	}

	for i := 1; i < n1; i++ {
		for j := 1; j < n2; j++ {
			if nums1[i] == nums2[j] {
				dp[i][j] = dp[i-1][j-1] + 1
			} else {
				dp[i][j] = max(dp[i-1][j], dp[i][j-1])
			}
		}
	}

	return dp[n1-1][n2-1]
}
