class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        nums.sort()

        ans = []
        for i in range(len(nums)):
            if i != 0 and nums[i] == nums[i-1]:
                continue
            
            target = -nums[i]
            l, r = i+1, len(nums) - 1
            while l < r:
                if nums[l] + nums[r] == target:
                    l += 1
                    r -= 1
                    print(i, l , r)
                    ans.append([nums[i], nums[l], nums[r]])
                elif nums[l] + nums[r] > target:
                    r -= 1
                else:
                    l += 1

        return ans

                

if __name__ == "__main__":
    s = Solution()
    s.threeSum()