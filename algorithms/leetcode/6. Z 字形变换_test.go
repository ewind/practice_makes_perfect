package leetcode

import "testing"

func convert(s string, numRows int) string {

	if numRows == 1 {
		return s
	}

	if len(s) == 1 {
		return s
	}

	idx := 0
	n := len(s)
	var grip = make([][]uint8, numRows)
	for idx < n {
		for rows := 0; rows < numRows && idx < n; rows++ {
			grip[rows] = append(grip[rows], s[idx])
			idx++
		}

		for rows := numRows - 2; rows > 0 && idx < n; rows-- {
			grip[rows] = append(grip[rows], s[idx])
			idx++
		}
	}

	var ans []byte
	for _, row := range grip {
		ans = append(ans, row...)
	}
	return string(ans)
}

func Test_convert(t *testing.T) {
	table := []struct {
		input   string
		numRows int
		wanted  string
	}{
		{
			input:   "PAYPALISHIRING",
			numRows: 3,
			wanted:  "PAHNAPLSIIGYIR",
		},

		{
			input:   "PAYPALISHIRING",
			numRows: 4,
			wanted:  "PINALSIGYAHRPI",
		},
	}

	for _, tt := range table {
		if got := convert(tt.input, tt.numRows); tt.wanted != got {
			t.Errorf("wanted:%v, got:%v",
				tt.wanted, got)
		}
	}
}
