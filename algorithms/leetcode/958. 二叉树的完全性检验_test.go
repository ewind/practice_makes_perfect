package leetcode

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func isCompleteTree(root *TreeNode) bool {
	q := []*TreeNode{root}
	meetNil := false

	for len(q) > 0 {
		node := q[0]
		q = q[1:]

		if meetNil && node != nil {
			return false
		}

		if node == nil {
			meetNil = true
			continue
		}

		q = append(q, node.Left)
		q = append(q, node.Right)
	}

	return true
}
