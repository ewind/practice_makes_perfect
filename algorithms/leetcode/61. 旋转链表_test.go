package leetcode

func rotateRight(head *ListNode, k int) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}

	size := 0
	cur := head
	for cur != nil {
		size++
		cur = cur.Next
	}
	k = k % size

	l, r := head, head
	for i := 0; i < k; i++ {
		r = r.Next
	}

	for r.Next != nil {
		r = r.Next
		l = l.Next
	}

	r.Next = head
	head = l.Next
	l.Next = nil
	return head
}
