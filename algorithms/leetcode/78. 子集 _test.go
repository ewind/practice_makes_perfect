package leetcode

import (
	"fmt"
	"testing"
)

func subsets(nums []int) [][]int {
	var res = make([][]int, 0)
	var path = make([]int, 0)
	var dfs func(start int)
	dfs = func(start int) {
		res = append(res, append([]int{}, path...))

		for i := start; i < len(nums); i++ {
			path = append(path, nums[i])
			dfs(i + 1)
			path = path[:len(path)-1]
		}
	}

	dfs(0)
	return res
}

func Test_subsets(t *testing.T) {
	nums := []int{1, 2, 3}
	res := subsets(nums)
	fmt.Println(res)
}
