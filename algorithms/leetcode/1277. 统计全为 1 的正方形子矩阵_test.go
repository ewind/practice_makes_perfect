package leetcode

import "testing"

/*
	https://leetcode.cn/problems/count-square-submatrices-with-all-ones/

给你一个 m * n 的矩阵，矩阵中的元素不是 0 就是 1，请你统计并返回其中完全由 1 组成的 正方形 子矩阵的个数。
*/

func countSquares(matrix [][]int) int {
	m, n := len(matrix), len(matrix[0])
	dp := make([][]int, m)
	for i := range dp {
		dp[i] = make([]int, n)
	}

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if matrix[i][j] == 0 {
				dp[i][j] = 0
				continue
			}

			var neiMin int
			if i-1 >= 0 && j-1 >= 0 {
				neiMin = dp[i-1][j]
				neiMin = min(neiMin, dp[i][j-1])
				neiMin = min(neiMin, dp[i-1][j-1])
			}
			dp[i][j] = neiMin + 1
		}
	}

	var ans = 0
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			ans += dp[i][j]
		}
	}
	return ans
}

func Test_countSquares(t *testing.T) {
	table := []struct {
		matrix [][]int
		wanted int
	}{
		{
			matrix: [][]int{{0, 1, 1, 1}, {1, 1, 1, 1}, {0, 1, 1, 1}},
			wanted: 15,
		},
	}

	for _, tt := range table {
		if got := countSquares(tt.matrix); got != tt.wanted {
			t.Errorf("wanted:%v, got:%v", tt.wanted, got)
		}
	}
}
