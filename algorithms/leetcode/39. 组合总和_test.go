package leetcode

import (
	"fmt"
	"testing"
)

func combinationSum(candidates []int, target int) [][]int {
	var ans [][]int
	var path []int

	var dfs func(start int, t int)
	dfs = func(start, t int) {
		if t < 0 {
			return
		}

		if t == 0 {
			ans = append(ans, append([]int{}, path...))
			return
		}

		for i := start; i < len(candidates); i++ {
			e := candidates[i]
			path = append(path, e)
			dfs(i, t-e)
			path = path[:len(path)-1]
		}
	}

	dfs(0, target)
	return ans
}

func Test_combinationSum(t *testing.T) {
	ans := combinationSum([]int{2, 3, 6, 7}, 7)
	fmt.Println(ans)
}
