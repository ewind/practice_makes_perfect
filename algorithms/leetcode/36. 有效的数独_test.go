package leetcode

import "fmt"

func isValidSudoku(board [][]byte) bool {

	// row check
	for i := 0; i < 9; i++ {
		var dc = make(map[byte]int)
		for j := 0; j < 9; j++ {
			c := board[i][j]
			if c == '.' {
				continue
			}
			dc[c] = dc[c] + 1
			if dc[c] >= 2 {
				fmt.Printf("row check: (i, j)=%v, %v", i, j)
				return false
			}
		}
	}

	// col check
	for i := 0; i < 9; i++ {
		var dc = make(map[byte]int)
		for j := 0; j < 9; j++ {
			c := board[j][i]
			if c == '.' {
				continue
			}
			dc[c] = dc[c] + 1
			if dc[c] >= 2 {
				fmt.Printf("col check: (i, j)=%v, %v", i, j)
				return false
			}
		}
	}

	// 3*3 check
	for i := 0; i < 9; i += 3 {
		for j := 0; j < 9; j += 3 {
			var dc = make(map[byte]int)
			for row := i; row < i+3; row++ {
				for col := j; col < j+3; col++ {
					c := board[row][col]
					if c == '.' {
						continue
					}
					dc[c] = dc[c] + 1
					if dc[c] >= 2 {
						fmt.Printf("33 check: board[%v, %v] =%c, cnt=%v", row, col, c, dc[c])
						return false
					}
				}
			}
		}
	}

	return true
}
