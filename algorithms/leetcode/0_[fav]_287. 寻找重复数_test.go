package leetcode

// https://leetcode.cn/problems/find-the-duplicate-number/description/?envType=study-plan-v2&envId=selected-coding-interview

func findDuplicate(nums []int) int {
	slow, fast := 0, 0

	// 快慢指针，直到相遇
	for slow, fast = nums[slow], nums[nums[fast]]; slow != fast; slow, fast = nums[slow], nums[nums[fast]] {
	}

	// 重头走一次， 再次相遇，就是环形入口
	slow = 0
	for slow != fast {
		slow = nums[slow]
		fast = nums[fast]
	}
	return slow
}
