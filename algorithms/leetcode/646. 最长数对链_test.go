package leetcode

import "sort"

func findLongestChain(pairs [][]int) int {
	if len(pairs) == 0 {
		return 0
	}

	sort.Slice(pairs, func(i, j int) bool {
		if pairs[i][0] < pairs[j][0] {
			return true
		}
		if pairs[i][0] == pairs[j][0] && pairs[i][1] < pairs[j][1] {
			return true
		}
		return false
	})

	n := len(pairs)
	dp := make([]int, n)
	dp[0] = 1

	for i := 1; i < n; i++ {
		dp[i] = 1
		for j := 0; j < i; j++ {
			if pairs[j][1] < pairs[i][0] {
				dp[i] = max(dp[i], dp[j]+1)
			}
		}
	}

	var ans = 0
	for _, e := range dp {
		ans = max(ans, e)
	}
	return ans
}
