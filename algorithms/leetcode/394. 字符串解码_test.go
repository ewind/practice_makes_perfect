package leetcode

import (
	"strings"
	"testing"
)

func decodeString(s string) string {

	var cntStack []int32
	var letterStack []string

	for idx, c := range s {
		// 数字
		if c >= '0' && c <= '9' {
			if idx == 0 {
				cntStack = append(cntStack, c-'0')
			} else {
				if s[idx-1] >= '0' && s[idx-1] <= '9' {
					cntStack[len(cntStack)-1] = cntStack[len(cntStack)-1]*10 + c - '0'
				} else {
					cntStack = append(cntStack, c-'0')
				}
			}
			continue
		}

		if c != ']' {
			letterStack = append(letterStack, string(c))
			continue
		}

		var temp string
		for len(letterStack) > 0 {
			top := letterStack[len(letterStack)-1]
			letterStack = letterStack[:len(letterStack)-1]
			if top == "[" {
				break
			}
			temp = top + temp
		}

		cnt := cntStack[len(cntStack)-1]
		cntStack = cntStack[:len(cntStack)-1]
		temp = strings.Repeat(temp, int(cnt))
		letterStack = append(letterStack, temp)
	}

	var ret string
	for _, item := range letterStack {
		ret = ret + item
	}
	return ret
}

func TestDecodeString(t *testing.T) {
	table := []struct {
		Input  string
		Wanted string
	}{
		{
			Input:  "3[a]2[bc]",
			Wanted: "aaabcbc",
		},
		{
			Input:  "abc3[cd]xyz",
			Wanted: "abccdcdcdxyz",
		},
	}

	for _, tt := range table {
		if got := decodeString(tt.Input); got != tt.Wanted {
			t.Errorf("input: %v, wanted: %v, got: %v",
				tt.Input, tt.Wanted, got)
		}
	}
}
