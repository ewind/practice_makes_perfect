package leetcode

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func rightSideView(root *TreeNode) []int {
	if root == nil {
		return nil
	}

	var queue = []*TreeNode{root}
	var ans []int

	for len(queue) > 0 {
		levelN := len(queue)
		for i := 0; i < levelN; i++ {
			node := queue[0]
			queue = queue[1:]
			if node.Left != nil {
				queue = append(queue, node.Left)
			}
			if node.Right != nil {
				queue = append(queue, node.Right)
			}

			// rightmost
			if i == levelN-1 {
				ans = append(ans, node.Val)
			}
		}
	}

	return ans
}
