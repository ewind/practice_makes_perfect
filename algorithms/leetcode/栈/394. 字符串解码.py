class Solution:
    def decodeString(self, s: str) -> str:
        numberStack = []
        charStack = []

        idx = 0
        while idx < len(s):
            c = s[idx]
            if "0" <= c <= "9":
                number = int(c)
                tryIdx = idx
                while True:
                    tryIdx += 1
                    tryChar = s[tryIdx]
                    if not ("0" <= tryChar <= "9"):
                        break
                    number = number * 10 + int(tryChar)

                numberStack.append(number)
                idx = tryIdx
                continue

            elif c == "]":
                temp = ""
                while True:
                    sc = charStack.pop()
                    if sc == "[":
                        break
                    temp = sc + temp
                number = numberStack.pop()
                code = temp * number
                charStack.append(code)
                idx += 1
                continue
            else:
                charStack.append(c)
                idx += 1

        return "".join(charStack)

if __name__ == '__main__':
    s = Solution()
    res = s.decodeString("11[xy]3[cd]ef")
    print(res)