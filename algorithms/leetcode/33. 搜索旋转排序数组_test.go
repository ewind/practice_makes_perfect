package leetcode

import "testing"

func search(nums []int, target int) int {
	l, r := 0, len(nums)-1

	for l <= r {
		mid := (r-l)/2 + l
		if nums[mid] == target {
			return mid
		}

		if nums[l] <= nums[mid] && nums[mid] <= nums[r] {
			// 全局有序
			if target > nums[mid] {
				l = mid + 1
			} else {
				r = mid - 1
			}
		} else if nums[l] <= nums[mid] {
			// 左侧有序
			if target >= nums[l] && target < nums[mid] {
				r = mid - 1
			} else {
				l = mid + 1
			}
		} else {
			// 右侧有序
			if target > nums[mid] && target <= nums[r] {
				l = mid + 1
			} else {
				r = mid - 1
			}
		}
	}

	return -1
}

func Test_search(t *testing.T) {
	table := []struct {
		nums   []int
		target int
		wanted int
	}{
		{
			nums:   []int{4, 5, 6, 7, 0, 1, 2},
			target: 0,
			wanted: 4,
		},
		{
			nums:   []int{4, 5, 6, 7, 0, 1, 2},
			target: 3,
			wanted: -1,
		},
		{
			nums:   []int{1, 3},
			target: 3,
			wanted: 1,
		},
		{
			nums:   []int{3, 5, 1},
			target: 3,
			wanted: 0,
		},
	}

	for _, tt := range table {
		if got := search(tt.nums, tt.target); got != tt.wanted {
			t.Errorf("nums: %v, targer:%v, wanted: %v, but got: %v",
				tt.nums, tt.target, tt.wanted, got)
		}
	}
}
