package leetcode

import "strings"

func reverseWords(s string) string {
	s = strings.TrimSpace(s)

	temp := strings.Split(s, " ")
	var ret []string
	for i := len(temp) - 1; i >= 0; i-- {
		if temp[i] != "" {
			ret = append(ret, temp[i])
		}
	}

	return strings.Join(ret, " ")
}
