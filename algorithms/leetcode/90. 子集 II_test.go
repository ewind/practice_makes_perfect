package leetcode

import (
	"fmt"
	"slices"
	"testing"
)

func subsetsWithDup(nums []int) [][]int {
	slices.Sort(nums)

	var res = make([][]int, 0)
	var path = make([]int, 0)

	var dfs func(start int)
	dfs = func(start int) {
		res = append(res, append([]int{}, path...))

		for i := start; i < len(nums); i++ {
			if i > start && nums[i-1] == nums[i] {
				continue
			}
			path = append(path, nums[i])
			dfs(i + 1)
			path = path[:len(path)-1]
		}
	}

	dfs(0)
	return res
}

func Test_subsetWithDup(t *testing.T) {
	nums := []int{1, 2, 2}
	res := subsetsWithDup(nums)
	fmt.Println(res)
}
