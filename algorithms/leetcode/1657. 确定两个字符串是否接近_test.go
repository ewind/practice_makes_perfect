package leetcode

import (
	"slices"
	"testing"
)

func closeStrings(word1 string, word2 string) bool {
	if len(word1) != len(word2) {
		return false
	}

	var cnt1 = make(map[byte]int)
	for _, e := range word1 {
		cnt1[byte(e)]++
	}

	var cnt2 = make(map[byte]int)
	for _, e := range word2 {
		cnt2[byte(e)]++
	}

	for e := range cnt1 {
		if _, ok := cnt2[e]; !ok {
			return false
		}
	}

	for e := range cnt2 {
		if _, ok := cnt1[e]; !ok {
			return false
		}
	}

	cntArr1 := make([]int, len(cnt1))
	for _, v := range cnt1 {
		cntArr1 = append(cntArr1, v)
	}
	slices.Sort(cntArr1)

	cntArr2 := make([]int, len(cnt2))
	for _, v := range cnt2 {
		cntArr2 = append(cntArr2, v)
	}
	slices.Sort(cntArr2)

	for i := range cntArr1 {
		if cntArr1[i] != cntArr2[i] {
			return false
		}
	}

	return true
}

func TestCloseStrings(t *testing.T) {
	table := []struct {
		word1  string
		word2  string
		expect bool
	}{
		{
			"abc",
			"bca",
			true,
		},
		{
			"a",
			"aa",
			false,
		},
		{
			"cabbba",
			"abbccc",
			true,
		},
		{
			"cabbba",
			"abbccc",
			true,
		},
		{
			"cabbba",
			"abbccc",
			true,
		},
	}

	for _, tt := range table {
		if actual := closeStrings(tt.word1, tt.word2); actual != tt.expect {
			t.Errorf("closeStrings(%s, %s) = %t; expected %t", tt.word1, tt.word2, actual, tt.expect)
		}
	}
}
