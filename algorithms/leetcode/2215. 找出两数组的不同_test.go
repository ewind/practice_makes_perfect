package leetcode

func findDifference(nums1 []int, nums2 []int) [][]int {
	var d1 = make(map[int]bool)
	for _, e := range nums1 {
		d1[e] = true
	}
	var d2 = make(map[int]bool)
	for _, e := range nums2 {
		d2[e] = true
	}

	var diff1 = make(map[int]bool)
	for _, e := range nums1 {
		if _, ok := d2[e]; !ok {
			diff1[e] = true
		}
	}

	var diff2 = make(map[int]bool)
	for _, e := range nums2 {
		if _, ok := d1[e]; !ok {
			diff2[e] = true
		}
	}

	return [][]int{toSlice(diff1), toSlice(diff2)}
}

func toSlice(d map[int]bool) []int {
	var ret []int
	for k := range d {
		ret = append(ret, k)
	}
	return ret
}
