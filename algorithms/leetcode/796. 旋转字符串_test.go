package leetcode

func rotateString(s string, goal string) bool {
	n := len(s)
	for i := 0; i < n; i++ {
		if s == goal {
			return true
		}

		s = s[1:] + s[0:1]
	}
	return false
}
