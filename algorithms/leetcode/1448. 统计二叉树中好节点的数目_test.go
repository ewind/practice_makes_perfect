package leetcode

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func goodNodes(root *TreeNode) int {
	var pathMax []int
	var cnt = 0

	helper(root, pathMax, &cnt)
	return cnt
}

func helper(root *TreeNode, pathMax []int, cnt *int) {
	if root == nil {
		return
	}

	if len(pathMax) == 0 {
		pathMax = append(pathMax, root.Val)
		(*cnt)++
	} else {
		if root.Val >= Top(pathMax) {
			pathMax = append(pathMax, root.Val)
			(*cnt)++
		} else {
			pathMax = append(pathMax, Top(pathMax))
		}
	}

	helper(root.Left, pathMax, cnt)
	helper(root.Right, pathMax, cnt)
	Pop(pathMax)
}

func Top(stack []int) int {
	return stack[len(stack)-1]
}

func Pop(stack []int) {
	stack = stack[:len(stack)-1]
}
func Top(stack []int) int {
	return stack[len(stack)-1]
}

func Pop(stack []int) {
	stack = stack[:len(stack)-1]
}
