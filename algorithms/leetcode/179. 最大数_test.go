package leetcode

import (
	"sort"
	"strconv"
)

func largestNumber(nums []int) string {
	numStr := make([]string, len(nums))
	for i := range nums {
		numStr[i] = strconv.Itoa(nums[i])
	}

	sort.Slice(numStr, func(i, j int) bool {
		return numStr[i]+numStr[j] > numStr[j]+numStr[i]
	})

	if numStr[0] == "" {
		numStr[0] = "0"
	}

	var ans string
	for _, e := range numStr {
		ans += e
	}

	return ans
}
