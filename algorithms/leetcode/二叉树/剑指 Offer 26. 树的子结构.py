# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def isSubStructure(self, A: TreeNode, B: TreeNode) -> bool:

        def check(t1, t2):
            if t2 is None:
                return True
            
            if t1 is None and t2 is not None:
                return False

            if t1.val != t2.val:
                return False
            
            return check(t1.left, t2.left) and check(t1.right, t2.right)

        if A is None or B is None:
            return False
        
        if A.val == B.val and check(A, B):
            return True

        l = self.isSubStructure(A.left, B)
        r = self.isSubStructure(A.right, B)
        return l or r