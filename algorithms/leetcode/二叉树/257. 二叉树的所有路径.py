# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def binaryTreePaths(self, root: Optional[TreeNode]) -> List[str]:
        if root is None:
            return None

        self.res = []
        path = []
        self.traverse(root, path)

        return ["->".join(item) for item in self.res]

    def traverse(self, root, path):
        path.append(str(root.val))

        # is leaf node
        if not root.left and not root.right:
            self.res.append(list(path))
        
        if root.left:
            self.traverse(root.left, path)

        if root.right:
            self.traverse(root.right, path)

        path.pop()


if __name__ == '__main__':
    s= Solution()
    s.binaryTreePaths()