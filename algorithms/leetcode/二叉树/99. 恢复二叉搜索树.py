# Definition for a binary tree node.
from cmath import inf
from curses import nonl


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def recoverTree(self, root) -> None:
        """
        Do not return anything, modify root in-place instead.
        """
        first, second = None, None       
        prev = TreeNode(-float(inf))

        def inorder(root):
            if root is None:
                return 
            
            inorder(root.left)
            nonlocal first, second, prev

            if root.val < prev:
                if first is None:
                    first = prev
                second = root
            
            prev = root
            inorder(root.right)
        
        first.val, second.val = second.val, first.val
 


