# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def findFrequentTreeSum(self, root: TreeNode):
        self.d = dict()

        self.traverse(root)

        maxium = -999999999999999999
        for k, v in self.d.items():
            if v > maxium:
                maxium = v
        
        return [k for k, v in self.d.items() if v == maxium]
    
    def traverse(self, root) -> int:
        if root is None:
            return 0
        
        if not root.left and not root.right:
            self.d[root.val] = self.d.get(root.val, 0) + 1
            return root.val
        
        lsum = self.traverse(root.left)
        rsum = self.traverse(root.right)

        subtreeSum = lsum + rsum + root.val
        self.d[subtreeSum] = self.d.get(subtreeSum, 0) + 1



if __name__ == '__main__':
    s = Solution()
    root = TreeNode(5, TreeNode(2), TreeNode(-3))
    res = s.findFrequentTreeSum(root)
    print(res)