package leetcode

type Color int

const (
	Red Color = iota + 1
	Blue
)

func shortestAlternatingPaths(n int, redEdges [][]int, blueEdges [][]int) []int {
	edges := map[Color][][]int{
		Red:  redEdges,
		Blue: blueEdges,
	}

	var ans = make([]int, n)
	var bfs func(target int, c Color)
	bfs = func(target int, c Color) {

	}

	for i := 1; i < n; i++ {
		bfs(i, Red)
		bfs(i, Blue)
	}

	return ans
}
