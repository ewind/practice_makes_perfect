package leetcode

import "slices"

func suggestedProducts(products []string, searchWord string) [][]string {
	slices.Sort(products)

	addWord := func(root *Trie, word string) {
		cur := root
		for _, ch := range word {
			ch = ch - 'a'
			if _, ok := cur.children[ch]; !ok {
				cur.children[ch] = newTrie()
			}

			cur = cur.children[ch]
			cur.words = append(cur.words, word)
			slices.Sort(cur.words)
			if len(cur.words) > 3 {
				cur.words = cur.words[:len(cur.words)-1]
			}
		}
	}

	root := newTrie()
	for _, word := range products {
		addWord(root, word)
	}

	var ans [][]string
	cur := root
	for _, ch := range searchWord {
		ch = ch - 'a'
		if _, ok := cur.children[ch]; ok {
			cur = cur.children[ch]
			ans = append(ans, cur.words)
		} else {
			ans = append(ans, []string{})
		}
	}

	return ans
}

type Trie struct {
	children map[int32]*Trie
	words    []string
}

func newTrie() *Trie {
	return &Trie{
		children: make(map[int32]*Trie),
	}
}
