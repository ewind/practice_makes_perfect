package leetcode

func majorityElement(nums []int) int {
	var ans = nums[0]
	var c = 1
	for i := 1; i < len(nums); i++ {
		if nums[i] == ans {
			c++
			continue
		}
		if c == 0 {
			ans = nums[i]
			c = 1
		} else {
			c--
		}
	}

	return ans
}
