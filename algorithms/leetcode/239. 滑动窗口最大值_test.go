package leetcode

import (
	"reflect"
	"testing"
)

func maxSlidingWindow(nums []int, k int) []int {
	var q []int
	var ans []int

	// 第一个窗口
	for i := 0; i < k; i++ {
		for len(q) > 0 && nums[q[len(q)-1]] < nums[i] {
			q = q[:len(q)-1]
		}
		q = append(q, i)
	}
	ans = append(ans, nums[q[0]])

	n := len(nums)
	for i := k; i < n; i++ {
		// 超出窗口大小， 左侧元素出队
		if i-q[0] == k {
			q = q[1:]
		}

		// 队尾元素比当前元素更小， 出队
		for len(q) > 0 && nums[q[len(q)-1]] < nums[i] {
			q = q[:len(q)-1]
		}

		q = append(q, i)
		ans = append(ans, nums[q[0]])
	}

	return ans
}

func Test_maxSlidingWindow(t *testing.T) {
	table := []struct {
		nums   []int
		k      int
		wanted []int
	}{
		{
			nums:   []int{1, 3, -1, -3, 5, 3, 6, 7},
			k:      3,
			wanted: []int{3, 3, 5, 5, 6, 7},
		},
		{
			nums:   []int{1, 3, 1, 2, 0, 5},
			k:      3,
			wanted: []int{3, 3, 2, 5},
		},
	}

	for _, tt := range table {
		if got := maxSlidingWindow(tt.nums, tt.k); !reflect.DeepEqual(tt.wanted, got) {
			t.Errorf("wanted:%v, got:%v",
				tt.wanted, got)
		}
	}
}
