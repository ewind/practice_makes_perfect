package leetcode

import "testing"

func findMin(nums []int) int {
	return nums[0]
}

func Test_findMinII(t *testing.T) {
	table := []struct {
		nums   []int
		wanted int
	}{
		{
			[]int{3, 3, 1, 3},
			1,
		},
		{
			[]int{4, 5, 6, 7, 0, 1, 2},
			0,
		},
	}

	for _, tt := range table {
		if got := findMin(tt.nums); got != tt.wanted {
			t.Errorf("nums:%v, wanted:%v, got:%v",
				tt.nums, tt.wanted, got)
		}
	}
}
