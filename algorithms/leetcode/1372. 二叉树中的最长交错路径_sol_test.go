package leetcode

const (
	Right int = -1
	Left  int = 1
)

func longestZigZag(root *TreeNode) int {
	var dfs func(root *TreeNode, dir int, length int) int
	dfs = func(root *TreeNode, dir int, length int) int {
		if root == nil {
			return length
		}

		if dir == Left {
			return max(dfs(root.Right, Right, length+1),
				dfs(root.Left, Left, 0)) // 左左， 重置 length
		} else {
			return max(dfs(root.Left, Left, length+1),
				dfs(root.Right, Right, 0)) // 右右， 重置 length
		}
	}

	return max(dfs(root.Left, Left, 0),
		dfs(root.Right, Right, 0))
}
