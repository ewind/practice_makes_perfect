package leetcode

import "testing"

func findMin(nums []int) int {
	if len(nums) == 1 {
		return nums[0]
	}
	l, r := 0, len(nums)-1
	for l <= r {
		if l == r {
			return nums[l]
		}

		if r-l == 1 {
			return min(nums[l], nums[r])
		}

		mid := l + (r-l)/2
		if nums[l] > nums[mid] {
			l++
			r = mid
		} else if nums[mid] > nums[r] {
			l = mid + 1
		} else {
			return nums[l]
		}
	}

	return nums[l]
}

func Test_findMin(t *testing.T) {
	table := []struct {
		nums   []int
		wanted int
	}{
		{
			[]int{3, 4, 5, 1, 2},
			1,
		},
		{
			[]int{4, 5, 6, 7, 0, 1, 2},
			0,
		},
	}

	for _, tt := range table {
		if got := findMin(tt.nums); got != tt.wanted {
			t.Errorf("nums:%v, wanted:%v, got:%v",
				tt.nums, tt.wanted, got)
		}
	}
}
