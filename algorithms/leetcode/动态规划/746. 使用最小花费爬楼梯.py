class Solution(object):
    def minCostClimbingStairs(self, cost):
        """
        :type cost: List[int]
        :rtype: int
        """
		length = len(cost)-1

		dp = [0]*length
		for i in range(length):
			if i < 2:
				dp[i] = cost[length-i]
			else:
				dp[i] = min(dp[i-1], dp[i-2]) + cost[n-i]
			
			
