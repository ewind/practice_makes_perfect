/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

 func max(a, b int) int {
    if a > b {
        return a
    }
    return b
}

var memo = make(map[*TreeNode]int)

func rob(root *TreeNode) int {
    if root == nil {
        return 0
    }
    
    if v, ok := memo[root] {
        return v
    }
    
    temp1 := rob(root.Left) + rob(root.Right)  // not rob
    
              temp2 := root.Val
    left := root.Left
    right := root.Right
    
	if left!=nil {
		temp2 += rob(left.Left) + rob(left.Right)
	}
	if right!= nil {
		temp2 += rob(right.Left) + rob(right.Right)
	}
    
	memo[root] =  max(temp1, temp2)
    return 
}