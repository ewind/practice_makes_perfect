package leetcode

func maximalSquare(matrix [][]byte) int {
	m, n := len(matrix), len(matrix[0])
	dp := make([][]int, m)
	for i := range dp {
		dp[i] = make([]int, n)
	}

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if matrix[i][j] == 0 {
				dp[i][j] = 0
				continue
			}

			var neiMin int
			if i-1 >= 0 && j-1 >= 0 {
				neiMin = dp[i-1][j]
				neiMin = min(neiMin, dp[i][j-1])
				neiMin = min(neiMin, dp[i-1][j-1])
			}
			dp[i][j] = neiMin + 1
		}
	}

	var ans = 0
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			ans = max(ans, dp[i][j])
		}
	}
	return ans
}
