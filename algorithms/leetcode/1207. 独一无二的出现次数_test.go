package leetcode

func uniqueOccurrences(arr []int) bool {
	cnt := make(map[int]int)
	for _, v := range arr {
		cnt[v]++
	}

	uniq := make(map[int]bool)
	for _, v := range cnt {
		if uniq[v] {
			return false
		}
		uniq[v] = true
	}

	return true
}
