package leetcode

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func isBalanced(root *TreeNode) bool {
	if root == nil {
		return true
	}

	left := calcDepth(root.Left)
	right := calcDepth(root.Right)

	if abs(left-right) > 1 {
		return false
	}

	return isBalanced(root.Left) && isBalanced(root.Right)
}

func calcDepth(root *TreeNode) int {
	if root == nil {
		return 0
	}

	return 1 + max(calcDepth(root.Left), calcDepth(root.Right))
}

func abs(x int) int {
	if x < 0 {
		return -1 * x
	}
	return x
}
