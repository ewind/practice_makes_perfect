package leetcode

import (
	"fmt"
	"testing"
)

func increasingTriplet(nums []int) bool {
	if len(nums) < 3 {
		return false
	}

	leftMin := make([]int, len(nums))
	leftMin[0] = nums[0]
	for i := 1; i < len(nums); i++ {
		if nums[i] < leftMin[i-1] {
			leftMin[i] = nums[i]
		} else {
			leftMin[i] = leftMin[i-1]
		}
	}

	rightMax := make([]int, len(nums))
	rightMax[len(nums)-1] = nums[len(nums)-1]
	for i := len(nums) - 2; i >= 0; i-- {
		if nums[i] > rightMax[i+1] {
			rightMax[i] = nums[i]
		} else {
			rightMax[i] = rightMax[i+1]
		}
	}

	for i := 1; i < len(nums)-1; i++ {
		if nums[i] > leftMin[i-1] && nums[i] < rightMax[i+1] {
			fmt.Printf("leftMin: %v, cur: %v, rightMax: %v\n", leftMin[i-1], nums[i], rightMax[i+1])
			return true
		}
	}
	return false
}

func TestIncreasingTriplet(t *testing.T) {
	nums := []int{5, 6, 2, 3, 1, 2, 3, 5}
	ret := increasingTriplet(nums)
	if ret != true {
		t.Error("case 1 failed")
	}
}
