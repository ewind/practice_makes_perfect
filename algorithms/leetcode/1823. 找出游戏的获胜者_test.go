package leetcode

type Node struct {
	Val  int
	Next *Node
}

func findTheWinner(n int, k int) int {
	var head = &Node{Val: 1}
	var cur = head
	for i := 2; i <= n; i++ {
		node := &Node{Val: i}
		cur.Next = node
		cur = node
	}
	cur.Next = head

	dummy := &Node{
		Next: head,
	}
	cur = dummy
	prev := dummy
	cur = dummy.Next
	for {
		for i := 1; i < k; i++ {
			if cur.Next == cur {
				return cur.Val
			}
			prev = cur
			cur = cur.Next
		}

		// delete the kth node
		prev.Next = cur.Next
		cur = cur.Next
	}
}
