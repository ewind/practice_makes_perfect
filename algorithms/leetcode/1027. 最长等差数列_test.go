package leetcode

func longestArithSeqLength(nums []int) int {
	if len(nums) == 0 {
		return 0
	}
	n := len(nums)
	// 状态定义：dp[i][d]表示以nums[i]结尾且公差为d的数列长度。
	dp := make([]map[int]int, n)
	for i := -500; i <= 500; i++ {
		if dp[0] == nil {
			dp[0] = make(map[int]int)
		}
		dp[0][i] = 1
	}
	var ans = 1
	for i := 1; i < n; i++ {
		if dp[i] == nil {
			dp[i] = make(map[int]int)
		}

		for j := 0; j < i; j++ {
			d := nums[i] - nums[j]
			if _, ok := dp[j][d]; ok {
				dp[i][d] = dp[j][d] + 1
			} else {
				dp[i][d] = 2
			}

			ans = max(ans, dp[i][d])
		}
	}

	return ans
}
