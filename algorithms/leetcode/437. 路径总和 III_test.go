package leetcode

import "testing"

type *TreeNode struct {
	Val   int
	Left  TreeNode
	Right TreeNode
}

func pathSum(root *TreeNode, targetSum int) int {
	ans := travel(root, targetSum)
	return ans
}

func dfs(root *TreeNode, targetSum int) int {
	var ans = 0
	if root == nil {
		return 0
	}

	if root.Val == targetSum {
		ans++
	}

	ans += dfs(root.Left, targetSum-root.Val)
	ans += dfs(root.Right, targetSum-root.Val)
	return ans
}


func travel(root *TreeNode, targetSum int) int {
	if root == nil {
		return 0
	}

	var ans = 0
	ans += dfs(root, targetSum)
	ans += travel(root.Left, targetSum)
	ans += travel(root.Right, targetSum)
	return ans
}


func Test_pathSum(t *testing.T) {

}