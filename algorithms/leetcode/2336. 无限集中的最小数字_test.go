package leetcode

import (
	"container/heap"
	"sort"
)

type IntHeap struct {
	sort.IntSlice
}

func (h *IntHeap) Push(x any) {
	h.IntSlice = append(h.IntSlice, x.(int))
}

func (h *IntHeap) Pop() any {
	n := len(h.IntSlice)
	x := h.IntSlice[n-1]
	h.IntSlice = h.IntSlice[:n-1]
	return x
}

func initHeap() *IntHeap {
	return &IntHeap{
		IntSlice: make([]int, 0),
	}
}

type SmallestInfiniteSet struct {
	Offset      int
	PopList     map[int]struct{}
	AddBackList *IntHeap
}

func Constructor() SmallestInfiniteSet {
	return SmallestInfiniteSet{
		Offset:      0,
		PopList:     make(map[int]struct{}),
		AddBackList: initHeap(),
	}
}

func (s *SmallestInfiniteSet) PopSmallest() int {
	var popNum int
	if s.AddBackList.Len() == 0 {
		s.Offset++
		popNum = s.Offset
	} else {
		popNum = heap.Pop(s.AddBackList).(int)
	}

	s.PopList[popNum] = struct{}{}
	return popNum
}

func (s *SmallestInfiniteSet) AddBack(num int) {
	if _, ok := s.PopList[num]; ok {
		delete(s.PopList, num)
		heap.Push(s.AddBackList, num)
	}
}

/**
 * Your SmallestInfiniteSet object will be instantiated and called as such:
 * obj := Constructor();
 * param_1 := obj.PopSmallest();
 * obj.AddBack(num);
 */
