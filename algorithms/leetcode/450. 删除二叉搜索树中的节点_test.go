package leetcode

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func deleteNode(root *TreeNode, key int) *TreeNode {
	if root == nil {
		return nil
	}

	if root.Val > key {
		root.Left = deleteNode(root.Left, key)
	} else if root.Val < key {
		root.Right = deleteNode(root.Right, key)
	} else {
		// root.Val == key
		if root.Left == nil {
			return root.Right
		}
		if root.Right == nil {
			return root.Left
		}

		// 左右节点均存在

		// 后继结点
		succ := root.Right
		for succ.Left != nil {
			succ = succ.Left
		}
		succ.Right = deleteNode(root.Right, succ.Val)
		succ.Left = root.Left
		return succ
	}

	return root
}
