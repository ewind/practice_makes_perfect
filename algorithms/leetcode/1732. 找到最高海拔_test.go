package leetcode

import "testing"

func largestAltitude(gain []int) int {
	var ret int = 0
	var tmp int = 0
	for _, v := range gain {
		tmp += v
		if tmp > ret {
			ret = tmp
		}
	}
	return ret
}

func TestLargestAltitude(t *testing.T) {

}
