package leetcode

import (
	"fmt"
	"testing"
)

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */

type ListNode struct {
	Val  int
	Next *ListNode
}

func reverseList(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}

	nh := reverseList(head.Next)
	head.Next.Next = head
	head.Next = nil
	return nh
}

func makeList(nums []int) *ListNode {
	if len(nums) == 0 {
		return nil
	}

	head := &ListNode{
		Val: nums[0],
	}
	cur := head

	for i := 1; i < len(nums); i++ {
		node := ListNode{
			Val: nums[i],
		}
		cur.Next = &node
		cur = cur.Next
	}

	return head
}

func printList(h *ListNode) {
	for h != nil {
		fmt.Printf("%v ", h.Val)
		h = h.Next
	}

	fmt.Println()
}

func Test_reverseList(t *testing.T) {
	head := makeList([]int{1, 2, 3, 4, 5})
	printList(head)
	nh := reverseList(head)
	printList(nh)
}
