package 单调栈

import (
	"fmt"
	"testing"
)

func nextGreaterElement(nums1 []int, nums2 []int) []int {
	greaterArray := helper(nums2)
	tempMap := make(map[int]int)

	// x -> greater x
	for idx, item := range greaterArray {
		tempMap[nums2[idx]] = item
	}

	res := make([]int, len(nums1))
	for idx, item := range nums1 {
		res[idx] = tempMap[item]
	}

	return res
}

func helper(nums []int) []int {
	stack := make([]int, 0)

	res := make([]int, len(nums))

	for i := len(nums) - 1; i >= 0; i-- {
		for len(stack) > 0 && stack[len(stack)-1] <= nums[i] {
			stack = stack[:len(stack)-1]
		}

		if len(stack) == 0 {
			res[i] = -1
		} else {
			res[i] = stack[len(stack)-1]
		}

		stack = append(stack, nums[i])
	}

	return res
}

func TestDemo(t *testing.T) {
	nums1 := []int{4, 1, 2}
	nums2 := []int{1, 3, 4, 2}
	res := nextGreaterElement(nums1, nums2)
	fmt.Println(res)
}
