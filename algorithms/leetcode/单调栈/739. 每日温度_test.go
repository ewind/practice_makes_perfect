package 单调栈

import (
	"fmt"
	"testing"
)

func dailyTemperatures(temperatures []int) []int {

	res := make([]int, len(temperatures))

	stack := make([]int, 0)
	length := len(temperatures)
	for j := length - 1; j >= 0; j-- {
		for len(stack) != 0 && temperatures[stack[len(stack)-1]] <= temperatures[j] {
			stack = stack[:len(stack)-1]
		}

		if len(stack) == 0 {
			res[j] = 0
		} else {
			res[j] = stack[len(stack)-1] - j
		}

		stack = append(stack, j)
	}

	return res
}

func TestDemo3(t *testing.T) {
	nums := []int{89, 62, 70, 58, 47, 47, 46, 76, 100, 70}

	res := dailyTemperatures(nums)
	fmt.Println(res)
}
