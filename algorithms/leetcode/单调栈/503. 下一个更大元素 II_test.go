package 单调栈

import (
	"fmt"
	"testing"
)

func nextGreaterElements(nums []int) []int {
	stack := make([]int, 0)

	res := make([]int, len(nums))

	length := len(nums)

	for i := 2*len(nums) - 1; i >= 0; i-- {
		for len(stack) > 0 && stack[len(stack)-1] <= nums[i%length] {
			stack = stack[:len(stack)-1]
		}

		if len(stack) == 0 {
			res[i%length] = -1
		} else {
			res[i%length] = stack[len(stack)-1]
		}

		stack = append(stack, nums[i%length])
	}

	return res[:length]
}

func TestDemo2(t *testing.T) {
	nums := []int{1, 2, 3, 4, 3}

	res := nextGreaterElements(nums)
	fmt.Println(res)

}
