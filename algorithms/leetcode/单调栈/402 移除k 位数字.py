class Solution:
    def removeKdigits(self, num: str, k: int) -> str:
        stack = []
        for idx, digit in enumerate(num):
            while True:
                if k > 0 and len(stack) > 0 and stack[-1] > digit:
                    k -= 1
                    stack.pop()
                else:
                    break

            stack.append(digit)

        if k != 0:
            stack = stack[:len(stack)-k]

        if len(stack) == 0:
            return "0"

        return str(int("".join(stack)))


if __name__ == '__main__':
    num = "1432219"
    k = 3
    s = Solution()
    print(s.removeKdigits(num, k))
    num = "10"
    k = 2
    print(s.removeKdigits(num, k))

