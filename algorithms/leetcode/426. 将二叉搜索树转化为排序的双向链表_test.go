package leetcode

type Node struct {
	Val   float64
	Left  *Node
	Right *Node
}

func treeToDoublyList(root *Node) *Node {
	if root == nil {
		return nil
	}

	if root.Left == nil && root.Right == nil {
		root.Left = root
		root.Right = root
		return root
	}

	var left, right *Node
	if root.Left != nil {
		left = treeToDoublyList(root.Left)
	}
	if root.Right != nil {
		right = treeToDoublyList(root.Right)
	}

	if left == nil && right == nil {
		return root
	}

	if left == nil {
		root.Right = right
		root.Left = right.Left
		right.Left.Right = root
		right.Left = root
		return root
	}

	if right == nil {
		root.Left = left.Left
		root.Right = left
		left.Left.Right = root
		left.Left = root
		return left
	}

	root.Left = left.Left
	root.Right = right
	left.Left.Right = root
	left.Left = right.Left
	right.Left.Right = left
	right.Left = root
	return left
}
