package leetcode

import "testing"

func calculate(s string) int {
	stack := make([]int, 0)
	sign := 1
	result := 0
	for i := 0; i < len(s); i++ {
		if s[i] >= '0' && s[i] <= '9' {
			num := 0
			for i < len(s) && s[i] >= '0' && s[i] <= '9' {
				num = num*10 + int(s[i]-'0')
				i++
			}
			i--
			result += sign * num
		} else if s[i] == '+' {
			sign = 1
		} else if s[i] == '-' {
			sign = -1
		} else if s[i] == '(' {
			stack = append(stack, result)
			stack = append(stack, sign)
			result = 0
			sign = 1
		} else if s[i] == ')' {
			result *= stack[len(stack)-1]
			stack = stack[:len(stack)-1]
			result += stack[len(stack)-1]
			stack = stack[:len(stack)-1]
		}
	}
	return result
}

func Test_calc(t *testing.T) {
	s := "10-(12-(3+4))"
	t.Log(calculate(s))
}
