package leetcode

import "sort"

func maxEnvelopes(envelopes [][]int) int {

	sort.Slice(envelopes, func(i, j int) bool {
		if envelopes[i][0] < envelopes[j][0] {
			return true
		}

		if envelopes[i][0] == envelopes[j][0] && envelopes[i][1] < envelopes[j][1] {
			return true
		}

		return false
	})

	return 0
}
