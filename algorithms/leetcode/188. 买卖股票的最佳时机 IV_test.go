package leetcode

import "math"

func maxProfit(k int, prices []int) int {
	n := len(prices)
	if n == 0 {
		return 0
	}

	k = min(k, n/2)
	// buy[i][j] 第 i 天恰好完成第 j 笔交易
	buy := make([][]int, n)
	sell := make([][]int, n)

	// 初始化边界条件
	for i := range buy {
		buy[i] = make([]int, k+1)
		sell[i] = make([]int, k+1)
	}
	buy[0][0] = -prices[0]
	sell[0][0] = 0
	for i := 1; i <= k; i++ {
		buy[0][i] = -math.MinInt64 / 2
		sell[0][i] = -math.MinInt64 / 2
	}

	for i := 1; i < n; i++ {
		for j := 1; j <= k; j++ {
			buy[i][j] = max(buy[i-1][j], sell[i-1][j]-prices[i])
			sell[i][j] = max(sell[i-1][j], buy[i-1][j-1]+prices[i])
		}
	}

	var ans = 0
	for _, p := range sell[n-1] {
		if p > ans {
			ans = p
		}
	}
	return ans
}
