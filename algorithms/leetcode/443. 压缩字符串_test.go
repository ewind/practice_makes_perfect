package leetcode

import (
	"fmt"
	"strconv"
	"testing"
)

func compress(chars []byte) int {
	if len(chars) == 1 {
		return 1
	}

	ret := 0
	prev := 0
	cur := 1

	placeIndex := 0
	for cur = 1; cur < len(chars); cur++ {
		if chars[cur] == chars[cur-1] {
			continue
		}

		if cur-prev > 1 {
			numStr := strconv.Itoa(cur - prev)
			ret += 1 + len(numStr)

			// 将数字转换成字符串
			chars[placeIndex] = chars[prev]
			placeIndex++
			for _, e := range numStr {
				chars[placeIndex] = byte(e)
				placeIndex++
			}
		} else {
			ret += 1
			chars[placeIndex] = chars[prev]
			placeIndex++
		}
		prev = cur
	}

	// 最后一个字符如果和倒数第二个字符相同，需要再加上
	if cur-prev > 1 {
		numStr := strconv.Itoa(cur - prev)
		ret += 1 + len(numStr)

		// 将数字转换成字符串
		chars[placeIndex] = chars[len(chars)-1]
		placeIndex++
		for _, e := range numStr {
			chars[placeIndex] = byte(e)
			placeIndex++
		}
	} else {
		ret += 1
		chars[placeIndex] = chars[len(chars)-1]
		placeIndex++
	}

	fmt.Printf("chars: %v\n", string(chars[:ret]))
	return ret
}

func TestCompress(t *testing.T) {
	chars := []byte{'1', '2', '3', '3', '2'}
	ret := compress(chars)
	fmt.Printf("ret: %v\n", ret)
	//if ret != 6 {
	//	t.Error("case 1 failed")
	//}

	//chars = []byte{'a'}
	//ret = compress(chars)
	//if ret != 1 {
	//	t.Error("case 2 failed")
	//}
	//
	//chars = []byte{'a', 'a', 'b', 'b', 'b', 'c', 'c'}
	//ret = compress(chars)
	//if ret != 6 {
	//	t.Error("case 3 failed")
	//}
	//
	//chars = []byte{'a', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b'}
	//ret = compress(chars)
	//if ret != 4 {
	//	t.Error("case 4 failed")
	//}
}
