package leetcode

func minInsertions(s string) int {
	var memo = make(map[string]int)

	var helper func(s string) int
	helper = func(s string) int {
		if len(s) <= 1 {
			return 0
		}

		if _, ok := memo[s]; ok {
			return memo[s]
		}

		if s[0] == s[len(s)-1] {
			memo[s] = helper(s[1 : len(s)-1])
			return memo[s]
		}

		memo[s] = min(
			helper(s[1:]),
			helper(s[:len(s)-1]),
		) + 1
		return memo[s]
	}

	return helper(s)
}
