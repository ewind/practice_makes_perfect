package leetcode

import "testing"

func integerBreak(n int) int {
	if n == 2 {
		return 1
	}
	if n == 3 {
		return 2
	}
	prodList := make([]int, n+1)
	prodList[0] = 0
	prodList[1] = 1
	prodList[2] = 2
	prodList[3] = 3

	for i := 4; i <= n; i++ {
		var temp = 0
		for j := 1; j < i; j++ {
			temp = max(temp, prodList[j]*(i-j))
		}
		prodList[i] = temp
	}

	return prodList[n]
}

func Test_integerBreak(t *testing.T) {
	table := []struct {
		n      int
		wanted int
	}{
		{
			n:      2,
			wanted: 1,
		},
		{
			n:      3,
			wanted: 2,
		},
		{
			n:      4,
			wanted: 4,
		},
		{
			n:      5,
			wanted: 6,
		},
		{
			n:      6,
			wanted: 9,
		},
		{
			n:      10,
			wanted: 36,
		},
	}

	for _, tt := range table {
		if got := integerBreak(tt.n); got != tt.wanted {
			t.Errorf("n:%v, wanted:%v, got:%v",
				tt.n, tt.wanted, got)
		}
	}
}
