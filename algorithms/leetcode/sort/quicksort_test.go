package sort

import (
	"fmt"
	"testing"
)

func QuickSort(nums []int) []int {
	if len(nums) <= 1 {
		return nums
	}

	// partition op
	pivot := nums[0]
	var ltPivot []int
	var gtPivot []int
	for i := 1; i < len(nums); i++ {
		if nums[i] >= pivot {
			gtPivot = append(gtPivot, nums[i])
		} else {
			ltPivot = append(ltPivot, nums[i])
		}
	}

	sortedLtPivot := QuickSort(ltPivot)
	sortedGtPivot := QuickSort(gtPivot)

	res := append(sortedLtPivot, pivot)
	res = append(res, sortedGtPivot...)
	return res
}

func TestQuickSort(t *testing.T) {
	nums := []int{5, 3, 4, 2, 1}
	fmt.Println(QuickSort(nums))
}
