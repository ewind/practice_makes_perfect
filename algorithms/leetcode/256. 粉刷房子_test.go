package leetcode

func minCost(costs [][]int) int {
	m, n := len(costs), len(costs[0])

	dp := make([][]int, m)
	for i := range dp {
		dp[i] = make([]int, n)
	}
	// 初始化
	for i := 0; i < n; i++ {
		dp[0][i] = costs[0][i]
	}

	for i := 1; i < m; i++ {
		for j := 0; j < 3; j++ {
			dp[i][j] = min(dp[i-1][(j+3-1)%3], dp[i-1][(j+1)%3]) + costs[i][j]
		}
	}

	var ans = 99999999
	for _, e := range dp[m-1] {
		ans = min(ans, e)
	}
	return ans
}
