package leetcode

import (
	"container/heap"
	"sort"
	"testing"
)

func maxScore(nums1, nums2 []int, k int) int64 {
	type pair struct{ x, y int }
	a := make([]pair, len(nums1))
	sum := 0
	for i, x := range nums1 {
		a[i] = pair{x, nums2[i]}
	}
	sort.Slice(a, func(i, j int) bool { return a[i].y > a[j].y })

	h := hp{nums2[:k]} // 复用内存
	for i, p := range a[:k] {
		sum += p.x
		h.IntSlice[i] = p.x
	}
	ans := sum * a[k-1].y
	heap.Init(&h)
	for _, p := range a[k:] {
		if p.x > h.IntSlice[0] {
			sum += p.x - h.replace(p.x)
			ans = max(ans, sum*p.y)
		}
	}
	return int64(ans)
}

type hp struct{ sort.IntSlice }

func (hp) Pop() (_ interface{}) { return }
func (hp) Push(interface{})     {}
func (h hp) replace(v int) int {
	top := h.IntSlice[0]
	h.IntSlice[0] = v
	heap.Fix(&h, 0)
	return top
}

func Test_maxScore(t *testing.T) {
	table := []struct {
		nums1  []int
		nums2  []int
		k      int
		wanted int64
	}{
		{
			nums1:  []int{1, 3, 3, 2},
			nums2:  []int{2, 1, 3, 4},
			k:      3,
			wanted: 12,
		},
	}

	for _, tt := range table {
		if got := maxScore(tt.nums1, tt.nums2, tt.k); got != tt.wanted {
			t.Errorf("nums1: %v, nums2: %v, wanted: %v, got: %v",
				tt.nums1, tt.nums2, tt.wanted, got)
		}
	}
}
