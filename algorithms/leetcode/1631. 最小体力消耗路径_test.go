package leetcode

func abs(x int) int {
	if x < 0 {
		return -1 * x
	}
	return x
}

// TODO: WA
func minimumEffortPath_wrong(heights [][]int) int {

	m, n := len(heights), len(heights[0])
	dp := make([][]int, m)
	for i := range dp {
		dp[i] = make([]int, n)
	}

	dp[0][0] = 0
	for i := 1; i < n; i++ {
		dp[0][i] = max(dp[0][i-1], abs(heights[0][i]-heights[0][i-1]))
	}
	for i := 1; i < m; i++ {
		dp[i][0] = max(dp[i-1][0], abs(heights[i][0]-heights[i-1][0]))
	}

	for i := 1; i < m; i++ {
		for j := 1; j < n; j++ {
			dp[i][j] = min(
				max(
					dp[i-1][j],
					abs(heights[i][j]-heights[i-1][j]),
				),
				max(
					dp[i][j-1],
					abs(heights[i][j]-heights[i][j-1])),
			)
		}
	}

	return dp[m-1][n-1]
}
