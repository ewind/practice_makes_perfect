package leetcode

func findCircleNum(grip [][]int) int {

	n := len(grip)
	var visited = make([]bool, n)

	var cp = 0
	for i := 0; i < n; i++ {
		if !visited[i] {
			cp++
			dfs(grip, &visited, i)
		}
	}
	return cp
}

func dfs(grip [][]int, visited *[]bool, i int) {
	rows := grip[i]
	for to, connected := range rows {
		if to == i || (*visited)[to] || connected == 0 {
			continue
		}

		(*visited)[to] = true
		dfs(grip, visited, to)
	}
}
