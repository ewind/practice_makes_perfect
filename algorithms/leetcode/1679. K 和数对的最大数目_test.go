package leetcode

import (
	"sort"
	"testing"
)

func maxOperations(nums []int, k int) int {
	if len(nums) <= 1 {
		return 0
	}

	sort.Slice(nums, func(i, j int) bool {
		return nums[i] < nums[j]
	})

	var ret int
	l, r := 0, len(nums)-1
	for l < r {
		if nums[l]+nums[r] == k {
			l++
			r--
			ret++
		} else if nums[l]+nums[r] > k {
			r--
		} else {
			l++
		}
	}
	return ret
}

func TestMaxOperations(t *testing.T) {
	tables := []struct {
		Nums   []int
		K      int
		Wanted int
	}{
		{
			Nums:   []int{1, 2, 3, 4},
			K:      5,
			Wanted: 2,
		},
		{
			Nums:   []int{3, 1, 3, 4, 3},
			K:      6,
			Wanted: 1,
		},
	}

	for _, tt := range tables {
		if got := maxOperations(tt.Nums, tt.K); got != tt.Wanted {
			t.Errorf("maxOperations failed: nums: %v, k:%v, got:%v, wanted:%v\n", tt.Nums, tt.K, got, tt.Wanted)
		}
	}
}
