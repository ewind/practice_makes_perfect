package leetcode

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func buildTree(preorder []int, inorder []int) *TreeNode {
	if len(preorder) == 0 {
		return nil
	}

	var index = 0
	for i := 0; i < len(inorder); i++ {
		if inorder[i] == preorder[0] {
			index = i
			break
		}
	}

	leftNodes := inorder[:index]
	rightNodes := inorder[index+1:]

	preorder = preorder[1:]
	leftTree := buildTree(preorder[:len(leftNodes)], leftNodes)
	rightTree := buildTree(preorder[len(leftNodes):], rightNodes)
	root := &TreeNode{Val: preorder[0]}
	root.Left = leftTree
	root.Right = rightTree
	return root
}
