package leetcode

func minimumTotal(triangle [][]int) int {
	m := len(triangle)
	dp := make([][]int, m)
	dp[0] = append(dp[0], triangle[0][0])

	for level := 1; level < m; level++ {
		for j := 0; j < len(triangle[level]); j++ {
			var temp int
			if j >= 1 && j < len(triangle[level])-1 {
				temp = min(dp[level-1][j], dp[level-1][j-1]) + triangle[level][j]
			} else if j == 0 {
				temp = dp[level-1][0] + triangle[level][0]
			} else {
				// j == length-1
				temp = dp[level-1][j-1] + triangle[level][j]
			}
			dp[level] = append(dp[level], temp)
		}
	}

	var ans = 99999999999
	for _, item := range dp[len(dp)-1] {
		ans = min(ans, item)
	}

	return ans
}
