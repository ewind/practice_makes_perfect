package leetcode

func nextGreaterElements(nums []int) []int {
	newNums := append([]int{}, nums...)
	for i := 0; i < len(nums)-1; i++ {
		newNums = append(newNums, nums[i])
	}

	var res = make([]int, len(nums))
	monoStack := make([]int, 0)
	for i := len(newNums) - 1; i >= 0; i-- {
		for len(monoStack) > 0 && newNums[top(monoStack)] <= newNums[i] {
			monoStack = pop(monoStack)
		}

		if i <= len(nums)-1 {
			if len(monoStack) == 0 {
				res[i] = -1
			} else {
				res[i] = newNums[top(monoStack)]
			}
		}

		monoStack = append(monoStack, i)
	}

	return res
}

func pop(stack []int) []int {
	return stack[:len(stack)-1]
}

func top(stack []int) int {
	return stack[len(stack)-1]
}
