package leetcode

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func kthSmallest(root *TreeNode, k int) int {
	// 用栈模拟递归过程
	var stack []*TreeNode
	for {
		for root != nil {
			stack = append(stack, root)
			root = root.Left
		}

		if len(stack) == 0 {
			break
		}

		node := stack[len(stack)-1]
		k--
		if k == 0 {
			return node.Val
		}
		stack = stack[:len(stack)-1]
		root = node.Right
	}

	return 0
}
