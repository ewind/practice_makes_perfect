package leetcode

import (
	"container/heap"
	"sort"
	"testing"
)

func totalCost(costs []int, k, candidates int) int64 {
	ans := 0
	if n := len(costs); candidates*2 < n {
		pre := hp{costs[:candidates]}
		heap.Init(&pre) // 原地建堆
		suf := hp{costs[n-candidates:]}
		heap.Init(&suf)
		for i, j := candidates, n-1-candidates; k > 0 && i <= j; k-- {
			if pre.IntSlice[0] <= suf.IntSlice[0] {
				ans += pre.IntSlice[0]
				pre.IntSlice[0] = costs[i]
				heap.Fix(&pre, 0)
				i++
			} else {
				ans += suf.IntSlice[0]
				suf.IntSlice[0] = costs[j]
				heap.Fix(&suf, 0)
				j--
			}
		}
		costs = append(pre.IntSlice, suf.IntSlice...)
	}
	sort.Ints(costs)
	for _, c := range costs[:k] { // 也可以用快速选择算法求前 k 小
		ans += c
	}
	return int64(ans)
}

type hp struct{ sort.IntSlice }

func (hp) Push(interface{})     {} // 没有用到，留空即可
func (hp) Pop() (_ interface{}) { return }

func Test_totalCost(t *testing.T) {
	table := []struct {
		costs  []int
		k      int
		cand   int
		wanted int64
	}{
		{
			[]int{17, 12, 10, 2, 7, 2, 11, 20, 8},
			3,
			4,
			11,
		},
	}

	for _, tt := range table {
		if got := totalCost(tt.costs, tt.k, tt.cand); got != tt.wanted {
			t.Errorf("costs: %v, k: %v, cand: %v, wanted: %v, got: %v",
				tt.costs, tt.k, tt.cand, tt.wanted, got)
		}
	}
}
