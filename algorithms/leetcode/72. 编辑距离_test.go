package leetcode

import "testing"

func minDistance(word1 string, word2 string) int {
	m := len(word1)
	n := len(word2)

	// dp 初始化
	dp := make([][]int, m+1)
	for i := range dp {
		dp[i] = make([]int, n+1)
	}

	for i := 0; i <= m; i++ {
		dp[i][0] = i
	}

	for j := 0; j <= n; j++ {
		dp[0][j] = j
	}

	for i := 1; i <= m; i++ {
		for j := 1; j <= n; j++ {
			if word1[i-1] == word2[j-1] {
				dp[i][j] = dp[i-1][j-1]
			} else {
				replace := dp[i-1][j-1] + 1
				deleted := dp[i-1][j] + 1
				insert := dp[i][j-1]
				dp[i][j] = min(min(replace, deleted), insert)
			}
		}
	}

	return dp[m][n]
}

func Test_minDistance(t *testing.T) {
	table := []struct {
		word1  string
		word2  string
		wanted int
	}{
		{
			word1:  "horse",
			word2:  "ros",
			wanted: 3,
		},
	}

	for _, tt := range table {
		if got := minDistance(tt.word1, tt.word2); got != tt.wanted {
			t.Errorf("wanted:%v, got:%v",
				tt.wanted, got)
		}
	}
}
