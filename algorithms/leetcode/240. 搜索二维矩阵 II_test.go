package leetcode

func searchMatrix(matrix [][]int, target int) bool {
	m, n := len(matrix), len(matrix[0])

	x, y := 0, n-1
	for {
		if x < 0 || x >= m || y < 0 || y > n {
			break
		}
		if matrix[x][y] == target {
			return true
		}

		if matrix[x][y] > target {
			y--
		} else {
			x++
		}
	}

	return false
}
