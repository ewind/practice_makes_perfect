package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func deleteDuplicates(head *ListNode) *ListNode {
	if head == nil {
		return head
	}

	var cur = head
	var test = head.Next

	for {
		if test == nil {
			return head
		}

		if test.Val == cur.Val {
			cur.Next = test.Next
			test = test.Next
			continue
		}

		cur = test
		test = test.Next
	}
}

func main() {
	deleteDuplicates(nil)
}
