package leetcode

import "slices"

func successfulPairs(spells []int, potions []int, success int64) []int {
	// 排序
	slices.Sort(potions)

	var ans = make([]int, len(spells))
	for idx, e := range spells {
		ans[idx] = help(e, potions, int(success))
	}
	return nil
}

func help(spell int, potions []int, succ int) int {
	l, r := 0, len(potions)-1

	var ans = 0
	for l < r {
		mid := l + (r-l)/2
		if spell*potions[mid] >= succ {
			ans += r - mid + 1
			r = mid - 1
		} else {
			l = mid + 1
		}
	}

	return ans
}
