package leetcode

import "testing"

func singleNumber(nums []int) int {
	var ans int

	for _, e := range nums {
		ans ^= e
	}

	return ans
}

func Test_singleNumber(t *testing.T) {
	table := []struct {
		nums   []int
		wanted int
	}{
		{
			nums:   []int{2, 2, 1},
			wanted: 1,
		},
	}

	for _, tt := range table {
		if got := singleNumber(tt.nums); got != tt.wanted {
			t.Errorf("wanted:%v, got:%v", tt.wanted, got)
		}
	}
}
