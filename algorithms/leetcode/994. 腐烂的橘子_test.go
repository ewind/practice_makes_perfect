package leetcode

func orangesRotting(grid [][]int) int {
	var rottenQ = make([][]int, 0)

	for x, line := range grid {
		for y, e := range line {
			if e == 2 {
				rottenQ = append(rottenQ, []int{x, y, 0})
			}
		}
	}

	dx := []int{-1, 0, 1, 0}
	dy := []int{0, -1, 0, 1}

	m := len(grid)
	n := len(grid[0])

	var minute int

	for len(rottenQ) > 0 {
		var x, y int
		x, y, minute = rottenQ[0][0], rottenQ[0][1], rottenQ[0][2]
		grid[x][y] = 0
		// 出队
		rottenQ = rottenQ[1:]

		for i := 0; i < 4; i++ {
			nx, ny := x+dx[i], y+dy[i]
			if nx >= 0 && nx < m && ny >= 0 && ny < n && grid[nx][ny] == 1 {
				grid[nx][ny] = 0
				rottenQ = append(rottenQ, []int{nx, ny, minute + 1})
			}
		}
	}

	for _, line := range grid {
		for _, e := range line {
			if e == 1 {
				return -1
			}
		}
	}

	return minute
}
