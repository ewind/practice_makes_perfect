package leetcode

func longestSubsequence(arr []int, difference int) int {
	if len(arr) == 0 {
		return 0
	}

	n := len(arr)
	dp := make([]int, n)
	dp[0] = 1

	for i := 1; i < n; i++ {
		dp[i] = 1
		for j := 0; j < i; j++ {
			if arr[j]+difference == arr[i] {
				dp[i] = max(dp[i], dp[j]+1)
			}
		}
	}

	var ans = 0
	for _, e := range dp {
		ans = max(ans, e)
	}
	return ans
}
