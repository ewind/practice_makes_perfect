package leetcode

func isAnagram(s string, t string) bool {

	sMap := count(s)
	tMap := count(t)

	for k, v := range sMap {
		if tMap[k] != v {
			return false
		}
	}

	for k, v := range tMap {
		if sMap[k] != v {
			return false
		}
	}

	return true
}

func count(s string) map[byte]int {
	var ret = make(map[byte]int)

	for _, c := range s {
		bc := byte(c)
		ret[bc]++
	}

	return ret
}
