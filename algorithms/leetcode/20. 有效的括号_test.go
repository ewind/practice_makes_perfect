package leetcode

var (
	leftMap = map[byte]bool{
		'(': true,
		'[': true,
		'{': true,
	}

	rightMap = map[byte]byte{
		')': '(',
		']': '[',
		'}': '{',
	}
)

func isValid(s string) bool {
	var stack []byte

	for _, c := range s {
		bc := byte(c)
		if leftMap[bc] {
			stack = append(stack, bc)
			continue
		}

		if len(stack) == 0 {
			return false
		}
		top := stack[len(stack)-1]
		stack = stack[:len(stack)-1]
		if top != rightMap[bc] {
			return false
		}
	}

	if len(stack) == 0 {
		return true
	}
	return false
}
