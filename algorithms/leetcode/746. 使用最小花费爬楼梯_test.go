package leetcode

import (
	"fmt"
	"testing"
)

func minCostClimbingStairs(cost []int) int {
	var memo = map[int]int{}
	return helper(cost, 0, memo)
}

func helper(cost []int, start int, memo map[int]int) int {
	if len(cost)-start <= 1 {
		memo[start] = 0
		return 0
	}
	if len(cost)-start == 2 {
		r := min(cost[start], cost[start+1])
		memo[start] = r
		return r
	}

	if _, ok := memo[start]; ok {
		return memo[start]
	}

	s1 := cost[start] + helper(cost, start+1, memo)
	s2 := cost[start+1] + helper(cost, start+2, memo)
	memo[start] = min(s1, s2)
	return memo[start]
}

func Test_minCostClimbingStairs(t *testing.T) {
	cost := []int{1, 100, 1, 1, 1, 100, 1, 1, 100, 1}
	res := minCostClimbingStairs(cost)
	fmt.Println(res)
}
