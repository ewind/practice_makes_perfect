class Solution:
    def pivotIndex(self, nums: List[int]) -> int:
        pre = []
        for idx, ele in enumerate(nums):
            if idx == 0:
                pre.append(ele)
                continue

            pre.append(pre[idx-1] + ele)
        
        for idx, ele in enumerate(pre):
            if idx == 0 :
                if pre[-1] - pre[0] == 0:
                    return idx
                else:
                    continue
            
            if idx == len(pre) - 1:
                if pre[-2] == 0:
                    return idx
                else:
                    continue
                
            if pre[-1] - pre[idx] == pre[idx-1]:
                return idx
        
        return -1