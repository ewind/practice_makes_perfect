class Solution:
    def subarraySum(self, nums, k: int) -> int:
        count = 0

        pre = 0
        mp = dict()
        mp[0] = 1

        for ele in nums:
            pre += ele
            if (pre - k) in mp:
                count += mp[pre-k]

            mp[pre] = mp.get(pre, 0) + 1

        return count
            

if __name__ == "__main__":
    s = Solution()
    print(s.subarraySum([1,2,1,2,1],3))
