# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

class Solution:
    def pathSum(self, root, targetSum: int):
        mp = dict()
        count = 0

        def preorder(root, pre = 0):
            if root is None:
                return 
            
            pre += root.val
            nonlocal count, mp
            if (pre -  targetSum) in mp:
                count += mp[pre-targetSum]
            
            mp[pre] = mp.get(pre, 0) + 1
            preorder(root.left, pre)
            preorder(root.right, pre)
            del(mp[pre])
            pre -= root.val
        
        preorder(root, 0)
        return count

if __name__ == '__main__':

