package leetcode

import (
	"fmt"
	"testing"
)

func lengthOfLIS(nums []int) int {
	n := len(nums)
	if n == 1 {
		return 1
	}

	lis := make([]int, n)
	lis[0] = 1
	for i := 1; i < n; i++ {
		var temp = 1
		for k := i - 1; k >= 0; k-- {
			if nums[i] > nums[k] {
				temp = max(temp, lis[k]+1)
			}
		}
		lis[i] = temp
	}

	ans := 0
	for _, e := range lis {
		ans = max(ans, e)
	}

	return ans
}

func Test_lengthOfLIS(t *testing.T) {
	ans := lengthOfLIS([]int{4, 10, 4, 3, 8, 9})
	fmt.Print(ans)
}
