package leetcode

import "testing"

var (
	directions = [][]int{
		[]int{-1, 0}, // up
		[]int{0, -1}, // left
		[]int{1, 0},  // down
		[]int{0, 1},  // right
	}
)

func nearestExit(maze [][]byte, entrance []int) int {
	m, n := len(maze), len(maze[0])
	var q = [][]int{{entrance[0], entrance[1], 0}}
	maze[entrance[0]][entrance[1]] = '+'

	for len(q) > 0 {
		x, y, step := q[0][0], q[0][1], q[0][2]
		// 出队
		q = q[1:]

		for _, d := range directions {
			nx, ny := x+d[0], y+d[1]
			// 越界
			if nx >= 0 && nx < m && ny >= 0 && ny < n && maze[nx][ny] == '.' {
				if nx == 0 || nx == m-1 || ny == 0 || ny == n-1 {
					return step + 1
				}

				maze[nx][ny] = '+'
				q = append(q, []int{nx, ny, step + 1})
			}
		}
	}

	return -1
}

func Test_nearestExit(t *testing.T) {
	table := []struct {
		maze     [][]byte
		entrance []int
		wanted   int
	}{
		{
			maze: [][]byte{
				{'+', '+', '.', '+'},
				{'.', '.', '.', '+'},
				{'+', '+', '+', '.'},
			},
			entrance: []int{1, 2},
			wanted:   1,
		},
		{
			maze: [][]byte{
				{'+', '+', '+'},
				{'.', '.', '.'},
				{'+', '+', '+'},
			},
			entrance: []int{1, 0},
			wanted:   2,
		},
		{
			maze: [][]byte{
				{'+', '.', '+', '+', '+', '+', '+'},
				{'+', '.', '+', '.', '.', '.', '+'},
				{'+', '.', '+', '.', '+', '.', '+'},
				{'+', '.', '.', '.', '+', '.', '+'},
				{'+', '+', '+', '+', '+', '+', '.'},
			},
			entrance: []int{0, 1},
			wanted:   -1,
		},
	}

	for _, tt := range table {
		if got := nearestExit(tt.maze, tt.entrance); got != tt.wanted {
			t.Errorf("wanted: %v, got: %v",
				tt.wanted, got)
		}

	}
}
