package leetcode

import (
	"fmt"
	"slices"
	"testing"
)

func combinationSum2(candidates []int, target int) [][]int {
	var ans [][]int
	var path []int

	slices.Sort(candidates)

	var dfs func(start int, t int)
	dfs = func(start, t int) {
		if t < 0 {
			return
		}

		if t == 0 {
			ans = append(ans, append([]int{}, path...))
			return
		}

		for i := start; i < len(candidates); i++ {
			if i > start && candidates[i-1] == candidates[i] {
				continue
			}

			e := candidates[i]
			path = append(path, e)
			dfs(i+1, t-e)
			path = path[:len(path)-1]
		}
	}

	dfs(0, target)
	return ans
}

func Test_combinationSum2(t *testing.T) {
	ans := combinationSum2([]int{2, 5, 2, 1, 2}, 5)
	fmt.Println(ans)
}
