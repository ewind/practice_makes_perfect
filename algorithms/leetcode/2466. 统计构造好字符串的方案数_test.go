package leetcode

func countGoodStrings(low int, high int, zero int, one int) int {
	dp := make([]int, high+1)
	dp[0] = 1

	const mod = 1_000_000_007
	for i := 1; i <= high; i++ {
		if i >= one {
			dp[i] = (dp[i] + dp[i-one]) % mod
		}
		if i >= zero {
			dp[i] = (dp[i] + dp[i-zero]) % mod
		}
	}

	var ans = 0
	for i := range dp {
		if i >= low && i <= high {
			ans = (ans + dp[i]) % mod
		}
	}

	return ans
}
