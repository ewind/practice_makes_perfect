package leetcode

import (
	"reflect"
	"testing"
)

func productExceptSelf(nums []int) []int {
	if len(nums) == 1 {
		return nums
	}

	leftProd := make([]int, len(nums))
	leftProd[0] = nums[0]
	for i := 1; i < len(nums); i++ {
		leftProd[i] = leftProd[i-1] * nums[i]
	}

	rightProd := make([]int, len(nums))
	rightProd[len(nums)-1] = nums[len(nums)-1]
	for i := len(nums) - 2; i >= 0; i-- {
		rightProd[i] = rightProd[i+1] * nums[i]
	}

	var ret = make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		if i == 0 {
			ret[i] = rightProd[i+1]
		} else if i == len(nums)-1 {
			ret[i] = leftProd[i-1]
		} else {
			ret[i] = leftProd[i-1] * rightProd[i+1]
		}
	}

	return ret
}

func TestProductExceptSelf(t *testing.T) {
	nums := []int{1, 2, 3, 4}
	ret := productExceptSelf(nums)
	if !reflect.DeepEqual(ret, []int{24, 12, 8, 6}) {
		t.Error("case 1 failed")
	}
}
