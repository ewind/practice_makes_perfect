package leetcode

import "testing"

func uniquePaths(m int, n int) int {

	var memo = make([][]int, m)
	for i := 0; i < m; i++ {
		memo[i] = make([]int, n)
	}

	var dfs func(x, y int) int
	dfs = func(x, y int) int {
		if x >= m || y >= n {
			return 0
		}

		if memo[x][y] > 0 {
			return memo[x][y]
		}

		if x == m-1 && y == n-1 {
			return 1
		}

		n := dfs(x+1, y) + dfs(x, y+1)
		memo[x][y] = n
		return n
	}

	ans := dfs(0, 0)
	return ans
}

func Test_uniquePaths(t *testing.T) {
	table := []struct {
		m      int
		n      int
		wanted int
	}{
		{
			m:      3,
			n:      7,
			wanted: 28,
		},
	}

	for _, tt := range table {
		if got := uniquePaths(tt.m, tt.n); got != tt.wanted {
			t.Errorf("m=%v, n=%v, wanted:%v, got:%v",
				tt.m, tt.n, tt.wanted, got)
		}
	}
}
