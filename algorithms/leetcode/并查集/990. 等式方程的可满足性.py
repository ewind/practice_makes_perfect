class UF:
    def __init__(self, n):
        self.parents = [i for i in range(1,n+1)]
        self.count = n
    
    def connected(self, p, q):
        return self.find(p) == self.find(q)
    
    def find(self, x):
        if self.parents[x] != x:
            self.parents[x] = self.find(self.parents[x])
        return self.parents[x]
    
    def union(self, p, q):
        rootP = self.find(p)
        rootQ = self.find(q)
        if rootP == rootQ:
            return
        
        self.parents[rootP] = rootQ
        self.count -= 1

class Solution:
    def equationsPossible(self, equations) -> bool:
        uf = UF(26)

        for e in equations:
            op1 = ord(e[0]) - ord("a") + 1
            op2 = ord(e[3]) - ord("a") + 1
            print(op1, op2)
            if e[1] == "=":
                uf.union(op1, op2)
        
        for e in equations:
            op1 = ord(e[0]) - ord("a") + 1
            op2 = ord(e[3]) - ord("a") + 1
            print(op1, op2)
            if e[1] == "!":
                if uf.connected(op1, op2):
                    return False
                    
        return True


