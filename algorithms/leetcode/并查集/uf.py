class UF:
    def __init__(self, n):
        self.parents = [i for i in range(n)]
        self.count = n
    
    def connected(self, p, q):
        return self.find(p) == self.find(q)
    
    def find(self, x):
        print(self.parents)
        if self.parents[x] != x:
            self.parents[x] = self.find(self.parents[x])
        return self.parents[x]
    
    def union(self, p, q):
        rootP = self.find(p)
        rootQ = self.find(q)
        if rootP == rootQ:
            return
        
        self.parents[rootP] = rootQ
        self.count -= 1


if __name__ == '__main__':
    uf = UF(10)
    uf.union(0,1)
    uf.union(1,7)
    uf.union(7,9)

    uf.union(3,5)
    uf.union(5,8)

    uf.union(3,0)

    uf.union(2,4)
    uf.union(4,6)

    uf.find_with(7)


