package leetcode

import "sort"

func minEatingSpeed(piles []int, h int) int {
	maxP := 0
	for _, p := range piles {
		if p > maxP {
			maxP = p
		}
	}

	return 1 + sort.Search(maxP-1, func(speed int) bool {
		speed++
		time := 0
		for _, p := range piles {
			time += (p + speed - 1) / speed
		}
		return time <= h
	})
}
