package leetcode

func pairSum(head *ListNode) int {
	if head == nil {
		return 0
	}

	var vals []int
	cur := head
	for cur != nil {
		vals = append(vals, cur.Val)
		cur = cur.Next
	}

	var ret = -999999999999999999
	for l, r := 0, len(vals)-1; l < r; {
		if vals[l]+vals[r] > ret {
			ret = vals[r] + vals[l]
		}
		l++
		r--
	}
	return ret
}
