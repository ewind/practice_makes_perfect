package leetcode

const (
	A = iota
	E
	I
	O
	U
)

func countVowelPermutation(n int) int {
	var mod int = 1e9 + 7
	dp := make([][5]int, n+1)

	// 初始化
	dp[1][A] = 1
	dp[1][E] = 1
	dp[1][I] = 1
	dp[1][O] = 1
	dp[1][U] = 1

	for i := 2; i <= n; i++ {
		dp[i][A] = (dp[i-1][E] + dp[i-1][I] + +dp[i-1][U]) % mod
		dp[i][E] = (dp[i-1][A] + dp[i-1][I]) % mod
		dp[i][I] = (dp[i-1][E] + dp[i-1][O]) % mod
		dp[i][O] = (dp[i-1][I]) % mod
		dp[i][U] = (dp[i-1][I] + dp[i-1][O]) % mod
	}

	var ans int
	for _, e := range dp[n] {
		ans = (ans + e) % mod
	}

	return ans
}
