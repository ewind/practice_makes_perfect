package leetcode

//func longestPalindromeSubseq(s string) int {
//	m := len(s)
//	dp := make([][]int, m)
//	for i := range dp {
//		dp[i] = make([]int, m)
//	}
//
//	// 长度为 1 的回文串
//	for i := 0; i < m; i++ {
//		dp[i][i] = 1
//	}
//
//	// 长度为 2 的回文串
//	for i := 0; i < m-1; i++ {
//		if s[i] == s[i+1] {
//			dp[i][i+1] = 2
//		} else {
//			dp[i][i+1] = 0
//		}
//	}
//
//	for i := 0; i < m-1; i++ {
//		for j := 2; j < m; j++ {
//			if s[i] == s[j] {
//				dp[i][j] = dp[i+1][j-1] + 2
//			} else {
//				dp[i][j] = max(dp[i+1][j], dp[i][j-1])
//			}
//		}
//	}
//
//	var ans = 99999999
//	for i := 0; i < m; i++ {
//		for j := 0; j < m; j++ {
//			if dp[i][j] != 0 {
//				ans = min(ans, dp[i][j])
//			}
//		}
//	}
//	return ans
//}

func longestPalindromeSubseq(s string) int {
	n := len(s)
	dp := make([][]int, n)
	for i := range dp {
		dp[i] = make([]int, n)
	}
	for i := n - 1; i >= 0; i-- {
		dp[i][i] = 1
		for j := i + 1; j < n; j++ {
			if s[i] == s[j] {
				dp[i][j] = dp[i+1][j-1] + 2
			} else {
				dp[i][j] = max(dp[i+1][j], dp[i][j-1])
			}
		}
	}
	return dp[0][n-1]
}
