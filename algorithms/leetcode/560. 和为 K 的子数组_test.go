package leetcode

// O(n) + hash
func subarraySum(nums []int, k int) int {
	count, pre := 0, 0
	m := map[int]int{}
	m[0] = 1
	for i := 0; i < len(nums); i++ {
		pre += nums[i]
		if _, ok := m[pre-k]; ok {
			count += m[pre-k]
		}
		m[pre] += 1
	}
	return count
}

// O(n^2)
func subarraySum_v2(nums []int, k int) int {
	preSum := make([]int, len(nums))
	preSum[0] = nums[0]
	for i := 1; i < len(nums); i++ {
		preSum[i] = preSum[i-1] + nums[i]
	}

	var ans = 0
	for i := 0; i < len(nums); i++ {
		for j := i; j < len(nums); j++ {
			if i == 0 {
				if preSum[j] == k {
					ans++
				}
			} else {
				if preSum[j]-preSum[i-1] == k {
					ans++
				}
			}
		}
	}

	return ans
}
