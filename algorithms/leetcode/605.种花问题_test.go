package leetcode

import (
	"testing"
)

func canPlaceFlowers(flowerbed []int, n int) bool {
	if n == 0 {
		return true
	}

	flowerbed = append(flowerbed, 0)
	temp := append([]int{0}, flowerbed...)

	l := 1
	r := len(temp) - 2

	// 贪心算法
	var ret = 0
	for i, ele := range temp {
		if i >= l && i <= r {
			prev := temp[i-1]
			next := temp[i+1]
			if prev+ele+next == 0 {
				ret += 1
				temp[i] = 1
			}
		}
	}

	return ret >= n

}

func TestCanPlaceFlowers(t *testing.T) {
	flowerbed := []int{1, 0, 0, 0, 1}
	n := 2
	ret := canPlaceFlowers(flowerbed, n)
	if ret != false {
		t.Error("case 1 failed")
	}
}
