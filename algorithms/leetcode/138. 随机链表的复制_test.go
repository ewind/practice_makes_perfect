package leetcode

type Node struct {
	Val    int
	Next   *Node
	Random *Node
}

func copyRandomList(head *Node) *Node {
	if head == nil {
		return nil
	}

	var nodeIndexMap = makeNodeIndexMap(head)
	var newList = copyList(head)

	var newNodeIndex = make([]*Node, len(nodeIndexMap))
	var i = 0
	for cur := newList; cur != nil; cur = cur.Next {
		newNodeIndex[i] = cur
		i++
	}

	for old, cur := head, newList; old != nil; old, cur = old.Next, cur.Next {
		randNode := old.Random
		if randNode == nil {
			cur.Random = nil
			continue
		}

		newRandNode := newNodeIndex[nodeIndexMap[randNode]]
		cur.Random = newRandNode
	}

	return newList
}

func copyList(cur *Node) *Node {
	newHead := &Node{
		Val: cur.Val,
	}

	var newCur = newHead
	for cur.Next != nil {
		cur = cur.Next
		newNode := &Node{
			Val: cur.Val,
		}

		newCur.Next = newNode
		newCur = newCur.Next
	}

	return newHead
}

func makeNodeIndexMap(cur *Node) map[*Node]int {
	var ret = make(map[*Node]int)

	index := 0
	for cur != nil {
		ret[cur] = index
		cur = cur.Next
		index++
	}
	return ret
}
