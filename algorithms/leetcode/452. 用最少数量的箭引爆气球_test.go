package leetcode

import (
	"sort"
	"testing"
)

func findMinArrowShots(points [][]int) int {
	sort.Slice(points, func(i, j int) bool {
		return points[i][0] < points[j][0]
	})

	var ans = 1
	left := points[0]
	for _, p := range points[1:] {
		if p[0] > left[1] {
			ans++
			left = p
		}

		if p[0] > left[0] {
			left[0] = p[0]
		}
		if p[1] <= left[1] {
			left[1] = p[1]
		}
	}

	return ans
}

func Test_findMinArrowShots(t *testing.T) {
	table := []struct {
		points [][]int
		wanted int
	}{
		{
			points: [][]int{{10, 16}, {2, 8}, {1, 6}, {7, 12}},
			wanted: 2,
		},
	}

	for _, tt := range table {
		if got := findMinArrowShots(tt.points); got != tt.wanted {
			t.Errorf("wanted: %v, got:%v",
				tt.wanted, got)
		}
	}
}
