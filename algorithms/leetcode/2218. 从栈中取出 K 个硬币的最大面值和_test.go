package leetcode

// TODO(heshuhao): not done
func maxValueOfCoins(piles [][]int, k int) int {
	n := len(piles)
	dp := make([][]int, k)
	for i := range dp {
		dp[i] = make([]int, n)
	}

	topIndex := make([][]int, k)
	for i := range topIndex {
		topIndex[i] = make([]int, n)
	}

	for i := 0; i < n; i++ {
		top := len(piles[i]) - 1
		// dp[i][j] 第 i 次操作从第 j 个 stack 取数的最大值
		dp[0][i] = piles[i][top]
		// 记录当前 stack 的 top 指针
		topIndex[0][i] = top
	}

	for i := 1; i < k; i++ {
		for j := 0; j < n; j++ {
			maxOld := 0
			for k := 0; k < n; k++ {
				maxOld = max(dp[i-1][k], maxOld)
			}

			newTop := topIndex[i-1][j] - 1
			if newTop < 0 {
				dp[i][j] = 0
				continue
			}
			dp[i][j] = maxOld + piles[j][newTop]
			topIndex[i][j] = newTop
		}
	}

	var ans = 0
	for _, e := range dp[k-1] {
		ans = max(ans, e)
	}

	return ans
}
