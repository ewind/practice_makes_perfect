package leetcode

var directions = [][2]int{
	{-1, 0},
	{1, 0},
	{0, -1},
	{0, 1},
}

type UF struct {
	Parents []int
}

func NewUF(size int) *UF {
	uf := &UF{
		Parents: make([]int, size),
	}

	for i := range uf.Parents {
		uf.Parents[i] = i
	}

	return uf
}

func (u *UF) Union(x, y int) {
	rootX := u.Find(x)
	rootY := u.Find(y)

	if rootY == rootX {
		return
	}

	u.Parents[rootY] = rootX
}

func (u *UF) Connected(i, j int) bool {
	return u.Find(i) == u.Find(j)
}

func (u *UF) Find(x int) int {
	if u.Parents[x] != x {
		u.Parents[x] = u.Find(u.Parents[x])
	}

	return u.Parents[x]
}

func solve(board [][]byte) {
	if len(board) == 0 || len(board[0]) == 0 {
		return
	}
	n, m := len(board), len(board[0])
	size := m * n
	dummy := m * n

	uf := NewUF(size + 1)

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if board[i][j] != 'O' {
				continue
			}

			// 边界
			if i == 0 || i == m-1 || j == 0 || j == n-1 {
				uf.Union(i*n+j, dummy)
			} else {
				for _, d := range directions {
					nx, ny := i+d[0], j+d[1]
					if board[nx][ny] == 'O' {
						uf.Union(nx*n+ny, i*n+j)
					}
				}
			}
		}
	}

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if !uf.Connected(i*n+j, dummy) {
				board[i][j] = 'X'
			}
		}
	}
}
