package leetcode

import "testing"

func isSubsequence(s string, t string) bool {
	i, j := 0, 0
	for i < len(s) && j < len(t) {
		if s[i] == t[j] {
			if i == len(s)-1 {
				return true
			}
			i++
		}
		j++
	}

	return false
}

func TestIsSubsequence(t *testing.T) {
	s := "abc"
	tt := "ahbgdc"
	if !isSubsequence(s, tt) {
		t.Error("isSubsequence error")
	}
}
