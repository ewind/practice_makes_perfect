package leetcode

func maxSubArray(nums []int) int {
	t0, t1 := nums[0], nums[0]

	var ans = max(t0, t1)
	for i := 1; i < len(nums); i++ {
		nt0 := max(t0, t1)
		nt1 := max(t1+nums[i], nums[i])
		t0, t1 = nt0, nt1
		ans = max(max(ans, t0), t1)
	}

	return ans
}
