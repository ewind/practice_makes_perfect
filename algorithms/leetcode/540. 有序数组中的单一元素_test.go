package leetcode

// Time: O(lg n)
// Space: O(1)
func singleNonDuplicate(nums []int) int {
	if len(nums) == 1 {
		return nums[0]
	}

	l, r := 0, len(nums)-1

	for l <= r {
		if l == r {
			return nums[l]
		}

		mid := (r-l)/2 + l
		if nums[mid] != nums[mid+1] && nums[mid] != nums[mid-1] {
			return nums[mid]
		}

		if nums[mid] == nums[mid+1] {
			if (r-mid+1)%2 == 0 {
				r = mid - 1
			} else {
				l = mid
			}
		} else {
			if (mid-l+1)%2 == 0 {
				l = mid + 1
			} else {
				r = mid
			}
		}
	}

	return 0
}
