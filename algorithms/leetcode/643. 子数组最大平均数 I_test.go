package leetcode

import "testing"

func findMaxAverage(nums []int, k int) float64 {
	var sum float64 = 0
	for i := 0; i < k; i++ {
		sum += float64(nums[i])
	}

	var maxAvg = sum / float64(k)
	for i := k; i < len(nums); i++ {
		sum += float64(nums[i] - nums[i-k])
		if sum/float64(k) > maxAvg {
			maxAvg = sum / float64(k)
		}
	}
	return maxAvg
}

func TestFindMaxAverage(t *testing.T) {
	tables := []struct {
		input  []int
		k      int
		output float64
	}{
		{
			[]int{1, 12, -5, -6, 50, 3},
			4,
			12.75,
		},
	}

	for _, table := range tables {
		if actual := findMaxAverage(table.input, table.k); actual != table.output {
			t.Errorf("findMaxAverage(%v, %v) = %v, expected %v", table.input, table.k, actual, table.output)
		}
	}
}
