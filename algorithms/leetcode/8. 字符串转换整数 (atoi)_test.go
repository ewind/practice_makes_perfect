package leetcode

import (
	"math"
	"strings"
)

func myAtoi(s string) int {
	s = strings.TrimSpace(s)

	neg := false
	if s[0] == '-' {
		neg = true
		s = s[1:]
	} else if s[0] == '+' {
		s = s[1:]
	}

	var ans int
	for i := 0; i < len(s); i++ {
		if !(s[i] >= '0' && s[i] <= '9') {
			break
		}

		n := s[i] - '0'
		ans = ans*10 + int(n)
		if neg && ans >= -math.MinInt32 {
			ans = -math.MinInt32
			break
		}

		if !neg && ans >= math.MaxInt32 {
			ans = math.MaxInt32
			break
		}
	}

	if neg {
		ans *= -1
	}
	return ans
}
