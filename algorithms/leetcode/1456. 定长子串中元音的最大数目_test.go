package leetcode

import "testing"

func maxVowels(s string, k int) int {
	vo := map[byte]bool{
		'a': true,
		'e': true,
		'i': true,
		'o': true,
		'u': true,
	}

	bs := []byte(s)

	var wind []byte
	var cnt int
	for i := 0; i < k; i++ {
		wind = append(wind, bs[i])
		if vo[bs[i]] {
			cnt++
		}
	}

	if cnt == k {
		return cnt
	}

	var maxCnt = cnt

	for i := k; i < len(bs); i++ {
		pop := wind[0]
		wind = append(wind[1:], bs[i])
		if vo[pop] && !vo[bs[i]] {
			cnt--
		} else if vo[bs[i]] && !vo[pop] {
			cnt++
			if cnt == k {
				return cnt
			}

			if cnt > maxCnt {
				maxCnt = cnt
			}
		}
	}

	return maxCnt
}

func TestMaxVowels(t *testing.T) {
	table := []struct {
		input  string
		k      int
		output int
	}{
		{
			input:  "abciiidef",
			k:      3,
			output: 3,
		},
	}

	for _, tt := range table {
		if actual := maxVowels(tt.input, tt.k); actual != tt.output {
			t.Errorf("maxVowels(%v, %v) = %v, expected %v", tt.input, tt.k, actual, tt.output)
		}
	}
}
