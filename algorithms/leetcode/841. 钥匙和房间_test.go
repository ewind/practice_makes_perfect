package leetcode

import "testing"

func canVisitAllRooms(rooms [][]int) bool {
	n := len(rooms)
	var visitMemo = map[int]bool{}
	for i := 0; i < n; i++ {
		visitMemo[i] = false
	}

	dfs(rooms, visitMemo, 0)
	for i := range rooms {
		if !visitMemo[i] {
			return false
		}
	}

	return true
}

func dfs(rooms [][]int, visitMemo map[int]bool, roomNo int) {
	if visitMemo[roomNo] {
		return
	}

	visitMemo[roomNo] = true
	keys := rooms[roomNo]
	for _, key := range keys {
		dfs(rooms, visitMemo, key)
	}
}

func TestCanVisitAllRooms(t *testing.T) {
	table := []struct {
		Input  [][]int
		Wanted bool
	}{
		{
			Input:  [][]int{{1}, {2}, {3}, {}},
			Wanted: true,
		},
		{
			Input:  [][]int{{1, 3}, {3, 0, 1}, {2}, {0}},
			Wanted: false,
		},
	}

	for _, tt := range table {
		if got := canVisitAllRooms(tt.Input); got != tt.Wanted {
			t.Errorf("input: %v, wanted: %v, got: %v", tt.Input, tt.Wanted, got)
		}
	}
}
