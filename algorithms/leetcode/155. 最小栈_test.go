package leetcode

type MinStack struct {
	data    []int
	minList []int
}

func Constructor() MinStack {
	return MinStack{
		data:    make([]int, 0),
		minList: make([]int, 0),
	}
}

func (s *MinStack) Push(val int) {
	s.data = append(s.data, val)
	if len(s.minList) == 0 {
		s.minList = append(s.minList, val)
	} else if s.minList[len(s.minList)-1] > val {
		s.minList = append(s.minList, val)
	} else {
		s.minList = append(s.minList, s.minList[len(s.minList)-1])
	}
}

func (s *MinStack) Pop() {
	s.data = s.data[:len(s.data)-1]
	s.minList = s.minList[:len(s.minList)-1]
}

func (s *MinStack) Top() int {
	if len(s.data) == 0 {
		return -1
	}
	return s.data[len(s.data)-1]
}

func (s *MinStack) GetMin() int {
	return s.minList[len(s.minList)-1]
}

/**
 * Your MinStack object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Push(val);
 * obj.Pop();
 * param_3 := obj.Top();
 * param_4 := obj.GetMin();
 */
