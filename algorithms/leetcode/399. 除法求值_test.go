package leetcode

import (
	"reflect"
	"testing"
)

type Node struct {
	Name    string
	Val     float64
	Inverse bool // 是否反向
}

func calcEquation(equations [][]string, values []float64, queries [][]string) []float64 {
	var grip = make(map[string]map[string]*Node)

	for idx, pair := range equations {
		from, to, val := pair[0], pair[1], values[idx]
		if _, ok := grip[from]; !ok {
			grip[from] = make(map[string]*Node)
		}

		grip[from][to] = &Node{
			Name:    to,
			Val:     val,
			Inverse: false,
		}

		if _, ok := grip[to]; !ok {
			grip[to] = make(map[string]*Node)
		}
		grip[to][from] = &Node{
			Name:    from,
			Val:     val,
			Inverse: true,
		}
	}

	var ans []float64
	for _, pair := range queries {
		from, to := pair[0], pair[1]
		var path = make([]string, 0)
		if dfs(grip, from, "", to, &path) {
			if len(path) == 1 {
				ans = append(ans, 1.0)
			} else {
				ans = append(ans, calc(grip, path))
			}
		} else {
			ans = append(ans, -1.0)
		}
	}

	return ans
}

func calc(grp map[string]map[string]*Node, path []string) float64 {
	var ans = 1.0
	from := path[0]
	for i := 1; i < len(path); i++ {
		to := path[i]
		node := grp[from][to]
		if node.Inverse {
			ans *= 1 / node.Val
		} else {
			ans *= node.Val
		}

		from = to
	}

	return ans
}

func dfs(grip map[string]map[string]*Node,
	start, cur string, target string, path *[]string) bool {
	if _, ok := grip[cur]; !ok {
		return false
	}
	if _, ok := grip[target]; !ok {
		return false
	}

	if cur == target {
		*path = append(*path, target)
		return true
	}

	*path = append(*path, cur)

	var found bool
	for _, next := range grip[cur] {
		// // 避免回上一步
		if next.Name == cur {
			continue
		}
		// 避免回到 start （ 存在 cycle 的情况）
		if next.Name == start {
			continue
		}

		if dfs(grip, start, next.Name, target, path) {
			found = true
			break
		}
	}

	// backtrace
	if !found {
		*path = (*path)[:len(*path)-1]
	}
	return found
}

func Test_calcEquation(t *testing.T) {
	table := []struct {
		equations [][]string
		values    []float64
		queries   [][]string
		wanted    []float64
	}{
		{
			[][]string{{"a", "b"}, {"b", "c"}, {"a", "c"}, {"d", "e"}},
			[]float64{2.0, 3.0, 6.0, 1.0},
			[][]string{{"a", "e"}},
			[]float64{-1.0},
		},
		//{
		//	[][]string{{"x1", "x2"}, {"x2", "x3"}, {"x3", "x4"}, {"x4", "x5"}},
		//	[]float64{3.0, 4.0, 5.0, 6.0},
		//	[][]string{{"x2", "x4"}},
		//	[]float64{20.0},
		//},
	}

	for _, tt := range table {
		if got := calcEquation(tt.equations, tt.values, tt.queries); !reflect.DeepEqual(got, tt.wanted) {
			t.Errorf("wanted: %v, got: %v", tt.wanted, got)
		}
	}
}
