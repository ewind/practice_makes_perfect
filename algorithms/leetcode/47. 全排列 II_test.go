package leetcode

import (
	"fmt"
	"slices"
	"testing"
)

func permuteUnique(nums []int) [][]int {
	slices.Sort(nums)

	var ans [][]int
	var path []int
	var n = len(nums)
	var used = make(map[int]bool)

	var dfs func()
	dfs = func() {
		if len(path) == n {
			temp := make([]int, n)
			copy(temp, path)
			ans = append(ans, temp)
			return
		}

		for idx, e := range nums {
			if used[idx] {
				continue
			}

			// 剪枝逻辑： 如果前面的相邻相等元素没有用过，则跳过
			if idx > 0 && nums[idx-1] == e && !used[idx-1] {
				continue
			}

			path = append(path, e)
			used[idx] = true
			dfs()
			path = path[:len(path)-1]
			used[idx] = false
		}
	}

	dfs()
	return ans
}

func Test_permuteUnique(t *testing.T) {
	ans := permuteUnique([]int{1, 1, 2})
	fmt.Println(ans)
}
