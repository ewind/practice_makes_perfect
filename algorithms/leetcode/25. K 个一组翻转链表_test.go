package leetcode

import (
	"fmt"
	"testing"
)

type ListNode struct {
	Val  int
	Next *ListNode
}

func reverseKGroup(head *ListNode, k int) *ListNode {
	if head == nil {
		return head
	}

	l, r := head, head
	for i := 0; i < k; i++ {
		if r == nil {
			return head
		}
		r = r.Next
	}

	nh := doReverse(l, r)
	l.Next = reverseKGroup(r, k)
	return nh
}

func doReverse(l, r *ListNode) *ListNode {
	var prev *ListNode
	cur, next := l, l
	for cur != r {
		next = cur.Next
		cur.Next = prev
		prev = cur
		cur = next
	}

	return prev
}

func makeList(nums []int) *ListNode {
	if len(nums) == 0 {
		return nil
	}

	head := &ListNode{
		Val: nums[0],
	}
	cur := head

	for i := 1; i < len(nums); i++ {
		node := ListNode{
			Val: nums[i],
		}
		cur.Next = &node
		cur = cur.Next
	}

	return head
}

func printList(h *ListNode) {
	for h != nil {
		fmt.Printf("%v ", h.Val)
		h = h.Next
	}

	fmt.Println()
}

func Test_reverseKGroup(t *testing.T) {
	head := makeList([]int{1, 2, 3, 4, 5})
	printList(head)
	nh := reverseKGroup(head, 2)
	printList(nh)
}
