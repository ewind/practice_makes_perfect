package leetcode

import (
	"fmt"
	"math"
	"testing"
)

func findPeakElement(nums []int) int {
	left, right := 0, len(nums)-1

	for left < right {
		if right-left == 0 {
			return left
		}

		mid := left + (right-left+1)/2
		leftOne := math.MinInt64
		if mid-1 >= 0 {
			leftOne = nums[mid-1]
		}
		rightOne := math.MinInt64
		if mid+1 < len(nums) {
			rightOne = nums[mid+1]
		}

		if nums[mid] > leftOne && nums[mid] > rightOne {
			return mid
		}

		// 保留左半边
		if leftOne > rightOne {
			right = mid - 1
		} else {
			left = mid + 1
		}
	}

	return left
}

func Test_findPeakElement(t *testing.T) {
	ans := findPeakElement([]int{2, 1})
	fmt.Println(ans)
}
