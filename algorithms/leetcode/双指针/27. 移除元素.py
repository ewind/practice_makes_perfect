
class Solution:
    def removeElement(self, nums, val: int) -> int:
        i, j = 0, len(nums)-1

        while i<j:
            while nums[j] == val:
                j -= 1
            
            while nums[i] != val:
                i += 1
            
            nums[i], nums[j] = nums[j], nums[i]
            i += 1
            j -= 1
        
        return j
    

if __name__ == "__main__":
    s = Solution()
    nums = [3,2,2,3]
    j = s.removeElement(nums, 3)
    print(nums[:j+1])

