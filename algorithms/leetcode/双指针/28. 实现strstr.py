from re import S


class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        if needle == "" or haystack == "":
            return 0
        
        l, r = 0, 0
        while r < len(haystack):
            if r-l >= len(needle):
                return l

            if haystack[r] == needle[r-l]:
                r += 1
            else:
                l += 1
                r = l
        
        if r - l == len(needle):
            return l

        return -1
            

if __name__ == '__main__':
    s = Solution()
    s1 = "mississippi"
    s2 = "issip"
    print(s.strStr(s1, s2))
