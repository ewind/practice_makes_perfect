class Solution:
    def reverseStr(self, s: str, k: int) -> str:
        s = list(s)

        if len(s) <= k:
            return "".join(s[::-1])
        
        length = len(s)
        while k 