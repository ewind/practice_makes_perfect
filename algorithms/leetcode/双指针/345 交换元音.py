
class Solution:
    def reverseVowels(self, s: str) -> str:
        i, j = 0, len(s) - 1

        s = list(s)

        vowels = dict(
            a='a',
            e='e',
            i='i',
            o='o',
            u='u'
        )

        while True:
            while i < len(s) and s[i].lower() not in vowels:
                i += 1
            while j >= 0 and s[j].lower() not in vowels:
                j -= 1
            
            if i < j:
                s[i], s[j] = s[j], s[i]
                i += 1
                j -= 1
            else:
                break
        
        return "".join(s)


if __name__ == '__main__':
    s = Solution()
    s.reverseVowels("leetcode")