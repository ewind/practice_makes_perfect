class Solution:
    def longestPalindrome(self, s: str) -> int:
        count = dict()
        for c in s:
            count.setdefault(c, 0)
            count[c] += 1
        
        oddCount = 0

        res = 0
        for k, v in count.items():
            if v % 2 == 0:
                res += v
            else:
                res += v - 1
                oddCount += 1
        
        if oddCount >= 1:
            res += 1
        
        return res

if __name__ == '__main__':
    s = Solution()
    res = s.longestPalindrome("abccccdd")
    print(res)