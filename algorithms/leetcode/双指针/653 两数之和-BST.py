
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def findTarget(self, root, k: int) -> bool:

        self.s = []
        self.inorder(root)

        i, j =0, len(self.s)
        while i < j:
            temp = self.s[i] + self.s[j]
            if temp == k:
                return True
            elif temp > k:
                j -= 1
            else:
                i += 1

        return False
    
    def inorder(self, root):
        if self.root == None:
            return
        
        self.inorder(self.root.left)
        self.s.append(root.val)
        self.inorder(self.root.right)