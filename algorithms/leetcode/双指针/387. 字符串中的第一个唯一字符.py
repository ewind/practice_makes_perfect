class Solution:
    def firstUniqChar(self, s: str) -> int:
        count = dict()
        for c in s:
            count.setdefault(c, 0)
            count[c] += 1
        
        for i, c in enumerate(s):
            if count.get(c, 0) == 1:
                return i
        return -1


if __name__ == '__main__':
    s = Solution()
    print(s.firstUniqChar("leetcode"))