class Solution(object):
    def distanceBetweenBusStops(self, distance, start, destination):
        """
        :type distance: List[int]
        :type start: int
        :type destination: int
        :rtype: int
        """

        if start > destination:
            start, destination = destination, start

        return min(sum(distance[start:destination]), sum(distance[:start]) + sum(distance[destination:]))

