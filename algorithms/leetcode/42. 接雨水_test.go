package leetcode

func trap(height []int) int {
	ans := 0
	var stack []int

	for i := range height {
		for len(stack) > 0 && height[i] >= height[top(stack)] {
			popHeight := height[top(stack)]
			stack = pop(stack)

			if len(stack) == 0 {
				break
			}

			dis := i - top(stack) - 1
			area := dis * (min(height[i], height[top(stack)]) - popHeight)
			ans += area
		}

		stack = append(stack, i)
	}

	return ans
}

func pop(stack []int) []int {
	return stack[:len(stack)-1]
}

func top(stack []int) int {
	return stack[len(stack)-1]
}
