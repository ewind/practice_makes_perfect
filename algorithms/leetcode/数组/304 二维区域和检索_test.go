package 数组

import (
	"fmt"
	"testing"
)

type NumMatrix struct {
	preSum [][]int
}

func Constructor(matrix [][]int) NumMatrix {
	preSum := make([][]int, len(matrix)+1)

	for i := 0; i <= len(matrix); i++ {
		preSum[i] = make([]int, len(matrix[0])+1)
	}

	rows := len(matrix)
	cols := len(matrix[0])

	for i := 1; i <= rows; i++ {
		for j := 1; j <= cols; j++ {
			preSum[i][j] = preSum[i-1][j] + preSum[i][j-1] - preSum[i-1][j-1] + matrix[i-1][j-1]
		}
	}

	return NumMatrix{
		preSum: preSum,
	}
}

func (this *NumMatrix) SumRegion(row1 int, col1 int, row2 int, col2 int) int {
	return this.preSum[row2+1][col2+1] - this.preSum[row2+1][col1] - this.preSum[row1][col2+1] + this.preSum[row1][col1]
}

/**
 * Your NumMatrix object will be instantiated and called as such:
 * obj := Constructor(matrix);
 * param_1 := obj.SumRegion(row1,col1,row2,col2);
 */

func TestDemo(t *testing.T) {
	matrix := [][]int{
		{3, 0, 1, 4, 2},
		{5, 6, 3, 2, 1},
		{1, 2, 0, 1, 5},
		{4, 1, 0, 1, 7},
		{1, 0, 3, 0, 5},
	}

	nMatrix := Constructor(matrix)
	fmt.Println(nMatrix.preSum)

	fmt.Println(nMatrix.SumRegion(1, 2, 2, 4))
}
