package leetcode

func canConstruct(ransomNote string, magazine string) bool {

	d := make(map[rune]int)
	for _, v := range magazine {
		d[v]++
	}

	for _, v := range ransomNote {
		if c := d[v]; c == 0 {
			return false
		} else {
			d[v] = c - 1
		}
	}

	return true
}
