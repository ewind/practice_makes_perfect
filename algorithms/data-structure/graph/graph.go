package graph

type Graph struct {
}

// V returns the number of vertices in the graph
func (g *Graph) V() int {
	return 0
}

// E returns the number of edges in the graph
func (g *Graph) E() int {
	return 0
}

// AddEdge adds an edge between v and w
func (g *Graph) AddEdge(v, w int) {

}

// Adj returns the vertices adjacent to v
func (g *Graph) Adj(v int) []int {
	return nil
}

// String returns the string representation of the graph
func (g *Graph) String() string {
	return ""
}
